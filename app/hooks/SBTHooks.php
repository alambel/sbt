<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class SBTHooks
{

    function init()
    {
        $CI = &get_instance();

        $idiom = lang();

        switch ($idiom) {
            default:
            case 'fr':
                $language = 'french';
                break;
            case 'de':
                $language = 'german';
                break;
        }

        $CI->lang->load('sbt', $language);


    }

}
