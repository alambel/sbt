<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Taxation extends CI_Controller
{


	public function __construct()
	{
		parent::__construct();

		$this->output->enable_profiler(DEV);
		if (!SBTParticipant::isLoggedIn()) {
			redirect();
		}
	}

	public function index()
	{
		// chef de table
		$participant = SBTParticipant::getCurrent();
		if ($participant && $participant->taxateur == 10) {
			redirect('taxation/table');
		}

		$pid = $this->input->post('productid');
		if ($pid) {
			redirect('taxation/produit/' . $pid);
		}
		$this->load->view('taxation_attente');
	}

	public function table($no = null)
	{
		$pid = $this->input->post('productid');

		$participant = SBTParticipant::getCurrent();
		if ($pid) {
			SBTTable::set($participant->table, $pid);
		}

		$taxation = SBTTable::current($participant->table);
		if (!$taxation) {
			$no = $no ? intval($no) : $this->input->post('no');
			$produit = null;
			if ($no) {
				$produit = SBTProduit::getForNO($no);
				if ($produit) {
					if ($produit->comm_date && date('Y-m-d',$produit->comm_date) != date('Y-m-d')){
						SBTNotification::error(t('produit_deja_taxe'));
						redirect('taxation/table');
					}
					if ($produit->comm_date && $produit->comm_auteur != SBTParticipant::getCurrentUserId()) {
						SBTNotification::error(t('produit_deja_taxe'));
						redirect('taxation/table');
					} else if ($produit->arrivage) {
						SBTTable::set($participant->table, $produit->id);
						redirect('taxation/table');
					} else {
						SBTNotification::error(t('produit_pas_arrive'));
						redirect('taxation/table');
					}
				} else {
					SBTNotification::error(t('produit_introuvable'));
				}
			}
			$this->load->view('taxation_table_choixproduit', array('produit' => $produit, 'participant' => $participant));
		} else {

			if (SBTTaxation::allDone($taxation, $participant->table)) {
				SBTProduit::calculate($taxation);
				SBTTable::close($participant->table);
				$produit = SBTProduit::get($taxation);
				SBTNotification::success(sprintf(t('taxation_produit_x_terminee'), $produit->no));
			}
			$produit = $taxation ? SBTProduit::get($taxation) : null;
			$producteur = $produit->participant ? SBTParticipant::get($produit->participant) : null;

			$comment = $this->input->post('comment');

			if ($comment) {
				if ($produit->commenter($comment, $participant->id)) {
					SBTProduit::calculate($taxation);
					SBTTable::close($participant->table, true);
					SBTNotification::success(sprintf(t('taxation_produit_x_terminee'), $produit->no));
					redirect('taxation/table');
				}
				SBTNotification::error('Impossible de sauver le commentaire');
			}
			$this->load->view('taxation_table', array('participant' => $participant, 'taxation' => $taxation, 'table' => $participant->table, 'produit' => $produit, 'producteur' => $producteur));
		}
	}

	public function update()
	{

		$participant = SBTParticipant::getCurrent();


		$comment = $this->input->post('comment');
		if ($comment) {
			$pid = $this->input->post('pid');
			$produit = $pid ? SBTProduit::get($pid) : null;
			if ($produit->comm_date && $produit->comm_auteur != SBTParticipant::getCurrentUserId()) {
				SBTNotification::error(t('produit_deja_taxe'));
				redirect('taxation/table');
			}
			if ($produit->commentaire != $comment) {
				if ($produit->commenter($comment, $participant->id)) {
					SBTProduit::calculate($pid);
					SBTTable::close($participant->table, true);
					SBTNotification::success(sprintf(t('commentaire_mis_a_jour'), $produit->no));
					redirect('taxation/table');
				}
				SBTNotification::error('Impossible de sauver le commentaire');
			}
		}
		redirect('taxation/table');
	}

	public function canceltable()
	{
		$participant = SBTParticipant::getCurrent();

		$taxation = SBTTable::current($participant->table);
		if ($taxation) {
			$nonedone = true;
			for ($i = 1; $i <= 6; $i++) {
				$done = SBTTaxation::done($taxation, $i, $participant->table);
				$nonedone = $done ? false : $nonedone;
			}
			if ($nonedone) {
				SBTTable::close($participant->table);
				SBTNotification::success(t('taxation_produit_annulee'));
			} else {
				SBTNotification::error(t('erreur_taxateur_en_cours'));
			}

		}
		redirect('taxation/table');

	}

	public function produit($pId)
	{
		$produitId = $pId ?: $this->input->post('pid');
		if (!$produitId) {
			redirect();
		}

		$produit = SBTProduit::get($pId);
		if (!$produit) {
			SBTNotification::error('Produit inconnu');
			redirect();
		}

		$participant = SBTParticipant::getCurrent();
		if (!$participant->taxateur) {
			redirect();
		}
		// chef de table
		if ($participant->taxateur == 10) {
			redirect('taxation/table');
		}
		$t = new SBTTaxation();
		$t->produit = $produit->id;
		$t->table = $participant->table;
		$t->taxateur = $participant->taxateur;
		if ($t->done($produit->id, $participant->taxateur, $participant->table)) {
			SBTNotification::error('Vous avez déja taxé ce produit.');
		}
		if ($this->input->post('no')) {
			foreach (array('aspect', 'forme', 'surface', 'texture', 'bouche', 'odeur') as $key) {
				$data = $this->input->post($key);
				if ($data) {
					$t->{$key} = $data['note'];
					$t->{$key . '_commentaire'} = $data['commentaire'];
				}
			}
			$t->save();
			SBTNotification::success('Taxation enregistrée');
			redirect('taxation');
		}
		$this->load->view('taxation', array('produit' => $produit));
	}

	public function status()
	{

		$this->output->enable_profiler(false);
		$participant = SBTParticipant::getCurrent();
		if (!$participant->taxateur) {
			return;
		}
		$pid = SBTTable::current($participant->table);
		echo $pid && !SBTTaxation::done($pid, $participant->taxateur, $participant->table) ? 'reload' : 'wait';
	}

	public function post()
	{
		$produit = $this->input->post('p');
		$taxateur = $this->input->post('t');
		$critere = $this->input->post('c');
		$note = $this->input->post('n');
		$rs = SBTTaxation::edit($produit, $taxateur, $critere, $note);
		echo json_encode(['success' => $rs, 'note' => $note]);
	}

}
