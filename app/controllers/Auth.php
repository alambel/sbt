<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();

		$this->data = array('error' => null, 'success' => null);
		$this->output->enable_profiler(DEV);
		$this->load->model('SBTTaxateur');
	}

	public function login()
	{
		if (SBTParticipant::isLoggedIn()) {
			redirect('entreprise');
		}
		$username = $this->input->post('username');
		$pass = $this->input->post('password');
		if ($username && $pass) {
			if (SBTParticipant::login($username, $pass)) {
				redirect('entreprise');
			} else {
				SBTNotification::error('User ou mot de passe faux');
			}
		}
		$this->load->view('login', $this->data);
	}

	public function logout()
	{
		SBTParticipant::logout();
		redirect('');
	}

	public function inscription()
	{
		if (!INSCRIPTIONS_OUVERTES) {
			redirect('');
		}
		$this->load->helper(array('form', 'url'));
		$data = $this->input->post('participant');
		if ($data) {
			if ($id = $this->input->post('id')) {
				$participant = SBTParticipant::get($id);
				$participant->load($data);
				$rs = $participant->save();
				$participant->activate();
				$participant->setPassword($this->input->post('password'));
			} else {
				$rs = SBTParticipant::register($data, $this->input->post('password') ? sha1($this->input->post('password')) : sha1(substr(sha1(rand()), 0, 15)));
			}
			if ($rs) {
				if (SBTParticipant::isAdmin()) {
					SBTNotification::success('Entreprise Inscrite');
					redirect('admin/entreprises');
				}
				SBTParticipant::login($data['email'], $this->input->post('password'));
				redirect('produits');
				return;
			}
		}
		$data = array('participant' => null);

		$t = $this->input->get('t');
		if ($t) {
			$pre = explode('|', base64_decode($t));
			$participant = SBTParticipant::get($pre[0]);
			if (!$participant || substr($participant->entreprise, 0, 10) != $pre[1]) {
				redirect('inscription');
				return;
			}
			if ($participant->actif) {
				SBTNotification::error('Cette entreprise est déja inscrite. Merci de vous authentifier');
				redirect();
				return;
			}
			if (substr($participant->entreprise, 0, 10) == $pre[1]) {
				$data['participant'] = $participant;
			}

		}
		$this->load->view('inscription', $data);
	}

	public function inscription_taxateur()
	{
		if (!INSCRIPTIONS_TAXATEUR_OUVERTES) {
			redirect('');
		}

		$this->load->helper(array('form', 'url'));
		$data = $this->input->post('taxateur');
		if ($data) {

			$taxateur = new SBTTaxateur();
			$taxateur->load($data);
			$taxateur->actif = 1;
			$taxateur->jour1_am = intval($this->input->post('jour1_am'));
			$taxateur->jour1_pm = intval($this->input->post('jour1_pm'));
			$taxateur->jour2_am = intval($this->input->post('jour2_am'));
			$taxateur->jour2_pm = intval($this->input->post('jour2_pm'));
			$taxateur->jour3_am = intval($this->input->post('jour3_am'));
			$taxateur->jour3_pm = intval($this->input->post('jour3_pm'));
			$taxateur->jour4_am = intval($this->input->post('jour4_am'));
			$taxateur->jour4_pm = intval($this->input->post('jour4_pm'));
			$rs = $taxateur->save();

			if ($rs) {
				redirect('taxateur_succes');
				return;
			}
		}
		$data = array('taxateur' => null);

		$t = $this->input->get('t');
		if ($t) {
			$pre = explode('|', base64_decode($t));
			$taxateur = SBTTaxateur::getPrefill($pre[0]);
			if (!$taxateur || substr($taxateur->email, 0, 10) != $pre[1]) {
				redirect('inscription_taxateur');
				return;
			}
			$data['taxateur'] = $taxateur;
		}
		$this->load->view('inscription_taxateur', $data);
//        $this->load->view('inscription_taxateur_complet', $data);
	}

	public function taxateur_succes()
	{
		$this->load->view('inscription_taxateur_success');
	}

}
