<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Produits extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();

		$this->output->enable_profiler(DEV);
		if (!SBTParticipant::isLoggedIn()) {
			redirect();
		}
	}

	public function index($participantId = null)
	{
//    	Clôture des inscriptions
		if (!MODIFICATIONS_PRODUIT && !SBTParticipant::isAdmin()) {
			redirect('entreprise');
		}

		$participant = SBTParticipant::isAdmin() && $participantId ? SBTParticipant::get($participantId) : SBTParticipant::getCurrent();

		if (!$participant) {
			redirect();
		}
		$produits = $this->input->post('p');
		if ($produits) {
			$rs = true;
			foreach ($produits as $key => $p) {
				$produit = $p['id'] ? SBTProduit::get($p['id']) : new SBTProduit();
				$produit->nom = $p['nom'];
				$produit->categorie = $p['categorie'];
				$produit->composition = $p['composition'];
				$produit->arguments = $p['arguments'];
				$produit->participant = $participant->id;
				$produit->jubile = $key == 10;
				$rs = $produit->save() ? $rs : false;
			}
			if ($rs) {
				SBTNotification::success('Produits sauvegardés');
				redirect('entreprise/index/' . $participant->id);
			} else {
				SBTNotification::error('Erreur lors de la sauvegarde');
				redirect('produits/index/' . $participant->id);
			}
		}

		$this->load->view('produits', array('participant' => $participant));
	}

	public function delete($pId = null)
	{
		// Clôture des inscriptions

		if (!MODIFICATIONS_PRODUIT && !SBTParticipant::isAdmin()) {
			redirect('entreprise');
		}
		$produit = null;
		if ($pId) {
			$produit = SBTProduit::get($pId);

		}
		if (!$produit) {
			SBTNotification::error(t('produit_introuvable'));
		}
		if ($produit->participant != SBTParticipant::getCurrentUserId() && !SBTParticipant::isAdmin()) {
			SBTNotification::error('Vous ne pouvez pas supprimer ce produit');
		}

		if ($produit->delete()) {
			SBTNotification::success(t('produit_supprime'));
			redirect('produits/index/' . $produit->participant);
		} else {
			SBTNotification::error('Erreur lors de la suppression');
		}

		$this->load->view('modifier_produit', array('produit' => $produit));
	}

	public function photos($pid)
	{
		if (!$this->config->item('download_photos')) {
			redirect();
		}
		$this->load->library('zip');
		$this->load->model('SBTPhoto');
		$files = SBTPhoto::getForProduit($pid);
		$produit = SBTProduit::get($pid);
		if ($files) {
			foreach ($files as $f) {
				$this->zip->read_file(SBTPhoto::$productImagesFolder . $produit->no . '/' . $f);
			}
			$filename = $produit->no . '-Photos.zip';
			$this->zip->download($filename);
		} else {
			SBTNotification::error('Aucune image disponible');
			$produit = SBTProduit::get($pid);
			redirect('entreprise/view/' . $produit->participant);
		}
	}

}
