<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();

		$this->output->enable_profiler(DEV);
		if (!SBTParticipant::isManager()) {
			SBTNotification::error('Droits insuffisants');
			redirect('');
		}
		$this->data = array();

	}

	public function index()
	{
		if (SBTParticipant::isAdmin()) {
			redirect('admin/entreprises');
		} else {
			redirect('admin/arrivage');
		}
	}

	public function produits()
	{
		if (!SBTParticipant::isAdmin()) {
			SBTNotification::error('Droits insuffisants');
			redirect('');
		}
		$produits = SBTProduit::getAll();
		$doublons = null;//TODO SBTProduit::getDoublons();
		$this->load->view('produits_all', array('produits' => $produits, 'doublons' => $doublons));
	}

	public function entreprises($export = null)
	{
		if (!SBTParticipant::isAdmin()) {
			SBTNotification::error('Droits insuffisants');
			redirect('');
		}
		$participants = SBTParticipant::getAll(true, $export == 'csv');

		$this->load->view('participant_all', array('participants' => $participants));
	}

	public function taxateurs()
	{
		if (!SBTParticipant::isAdmin()) {
			SBTNotification::error('Droits insuffisants');
			redirect('');
		}
		$this->load->model('SBTTaxateur');
		$taxateurs = SBTTaxateur::getAll();

		$this->load->view('taxateurs_all', array('taxateurs' => $taxateurs));
	}

	public function taxateur_edit($tId = null)
	{
		if (!SBTParticipant::isAdmin()) {
			SBTNotification::error('Droits insuffisants');
			redirect('');
		}
		$this->load->model('SBTTaxateur');
		$taxateur = SBTTaxateur::get(intval($tId));

		if (!$taxateur) {
			SBTNotification::error('Taxateur introuvable');
			redirect('taxateurs');
		}
		$taxateurData = $this->input->post('taxateur');
		if ($taxateurData) {
			$taxateur->load($taxateurData);
			if ($taxateur->save()) {
				SBTNotification::success('Taxateur mis à jour');
			} else {
				SBTNotification::error('Erreur lors de la mise à jour du taxateur');
			}

		}


		$this->load->view('taxateur_edit', array('taxateur' => $taxateur));
	}

	public function preinscriptions($export = null)
	{
		if (!SBTParticipant::isAdmin()) {
			SBTNotification::error('Droits insuffisants');
			redirect('');
		}
		if ($this->input->post('action') == 'sendmail') {
			$sentMail = 0;
			$ids = $this->input->post('ids');
			foreach ($ids as $id) {
				$p = SBTParticipant::get($id);
				if ($p->sendPreinscriptionMail()) {
					$sentMail += 1;
				}
			}
			if ($sentMail == count($ids)) {
				SBTNotification::success($sentMail . ' emails envoyés avec succès');
			} else {
				SBTNotification::success($sentMail . '/' . count($ids) . ' emails envoyés avec succès');
			}
		}
		$participants = SBTParticipant::getPreinscriptions();

		$this->load->view('preinscriptions', array('participants' => $participants));
	}

	public function delete_taxateur($taxateurId = null)
	{
		if (SBTParticipant::isAdmin()) {
			$this->load->model('SBTTaxateur');
			SBTTaxateur::delete($taxateurId);
		}
		redirect('admin/taxateurs');
	}

	public function delete_participant($pId = null)
	{
		if (SBTParticipant::isAdmin()) {
			SBTParticipant::delete($pId);
		}
		redirect('admin/entreprises');
	}

	public function taxateurs_prefill($export = null)
	{
		if (!SBTParticipant::isAdmin()) {
			SBTNotification::error('Droits insuffisants');
			redirect('');
		}
		$this->load->model('SBTTaxateur');
		if ($this->input->post('action') == 'sendmail') {
			$sentMail = 0;
			$ids = $this->input->post('ids');
			foreach ($ids as $id) {
				$p = SBTTaxateur::getPrefill($id);
				if ($p && $p->sendPreinscriptionMail()) {
					$sentMail += 1;
				}
			}
			if ($sentMail == count($ids)) {
				SBTNotification::success($sentMail . ' emails envoyés avec succès');
			} else {
				SBTNotification::success($sentMail . '/' . count($ids) . ' emails envoyés avec succès');
			}
		}
		$taxateurs = SBTTaxateur::getAllPrefill();
		if ($export == 'csv') {
			// output headers so that the file is downloaded rather than displayed
			header('Content-Encoding: UTF-8');
			header('Content-type: text/csv; charset=UTF-8');
			echo "\xEF\xBB\xBF"; // UTF-8 BOM
			header('Content-Disposition: attachment; filename=SBT-Preinscriptions-' . date('YmdHis') . '.csv');
			$output = fopen('php://output', 'w');
			fputcsv($output, array('entreprise', 'nom', 'prenom', 'telephone', 'mobile', 'rue', 'npa', 'lieu', 'canton', 'email', 'url'));
			foreach ($taxateurs as $p) {
				fputcsv($output, array($p->entreprise,
						$p->nom,
						$p->prenom,
						$p->telephone,
						$p->mobile,
						$p->rue,
						$p->npa,
						$p->lieu,
						$p->canton,
						$p->email,
						$p->getPreinscriptionUrl())
				);
			}
			exit();
		}
		$this->load->view('taxateurs_prefill', array('taxateurs' => $taxateurs));
	}

	public function generateAllNO()
	{
		if (!SBTParticipant::isAdmin()) {
			SBTNotification::error('Droits insuffisants');
			redirect('');
		}
		SBTProduit::generateAllNO();
	}

	public function generateNO($pid = null)
	{
		if (!SBTParticipant::isAdmin()) {
			SBTNotification::error('Droits insuffisants');
			redirect('');
		}
		$produit = SBTProduit::get($pid);
		if ($produit->generateNO()) {
			SBTNotification::success('NO généré avec succès');
		} else {
			SBTNotification::error('Erreur lors de la génération du NO');
		}
		redirect('admin/produits');
	}

	public function arrivage()
	{
		$no = $this->input->post('no');
		$produit = null;
		if ($no) {
			$produit = SBTProduit::getForNO($no);
			if (!$produit) {
				SBTNotification::error('Produit introuvable');
				redirect('admin/arrivage');
			}
			if ($produit->arrivage) {
				SBTNotification::error('Produit déjà enregistré comme arrivé');
				redirect('admin/arrivage');
			}
			$data = $this->input->post('produit');
			if ($data) {
				$produit->categorie = $data['categorie'];
				$produit->composition = $data['composition'];
				$produit->arguments = $data['arguments'];
				$produit->save(); //TODO log changes?
				if ($produit->arrivage()) {
					SBTNotification::success('Produit marqué comme arrivé');
					redirect('admin/arrivage');
				} else {
					SBTNotification::error('Erreur lors de l\'arrivage');
				}
			}
		}
		$this->load->view('arrivage', array('produit' => $produit));
	}

	public function commentaires()
	{
		$this->data['produits'] = SBTProduit::getNonValides();
		$this->load->view('commentaires', $this->data);
	}

	public function commentaire_validate()
	{
		$p = SBTProduit::get(intval($this->input->post('id')));
		$p->commValidate();
	}

	public function commentaire_post()
	{
		$p = SBTProduit::get(intval($this->input->post('id')));
		$p->commenter($this->input->post('value'));
	}

	public function resultats()
	{
		if (!SBTParticipant::isAdmin()) {
			SBTNotification::error('Droits insuffisants');
			redirect('');
		}
		$this->data['categorie'] = $this->input->get('c');
		$this->data['produits'] = SBTProduit::getAll($this->data['categorie']);
		$this->load->view('resultats', $this->data);
	}

	public function recalcul_resultats()
	{
		if (!SBTParticipant::isAdmin()) {
			SBTNotification::error('Droits insuffisants');
			redirect('');
		}
		SBTProduit::recalculate();
		SBTNotification::getErrors();
		redirect('admin/produits');
	}

	public function medailles()
	{
		if (!SBTParticipant::isAdmin()) {
			SBTNotification::error('Droits insuffisants');
			redirect('');
		}
		SBTProduit::calculateMedailles();
		$this->data['produits'] = $medailles = SBTProduit::getMedailles();
		$this->load->view('medailles', $this->data);
	}


	public function prixCategorie()
	{
		if (!SBTParticipant::isAdmin()) {
			SBTNotification::error('Droits insuffisants');
			redirect('');
		}
		$this->data['prix'] = array(
			'confiserie' => SBTProduit::getPrixCategorie(array('confiserie', 'tartiner')),
			'boulangerie' => SBTProduit::getPrixCategorie('boulangerie'),
			'patisserie' => SBTProduit::getPrixCategorie('patisserie'),
			'snack' => SBTProduit::getPrixCategorie('snack'),
			'jubile' => SBTProduit::getPrixJubile()
		);
		$this->data['prixAcademy'] = SBTProduit::getPrixAcademy();
		$this->load->view('prix_categorie', $this->data);
	}

	public function prixAcademy()
	{
		if (!SBTParticipant::isAdmin()) {
			SBTNotification::error('Droits insuffisants');
			redirect('');
		}
		$this->data['prixAcademy'] = SBTProduit::getPrixAcademy();
		$this->load->view('prix_academy', $this->data);
	}

	public function champions()
	{
		if (!SBTParticipant::isAdmin()) {
			SBTNotification::error('Droits insuffisants');
			redirect('');
		}
		$this->data['champions'] = SBTProduit::getChampions();

		$this->load->view('champions', $this->data);
	}

	public function tables()
	{
		if (!SBTParticipant::isAdmin()) {
			SBTNotification::error('Droits insuffisants');
			redirect('');
		}
		$this->load->view('tables');
	}

	public function cancelTable($table)
	{
		if (!SBTParticipant::isAdmin()) {
			SBTNotification::error('Droits insuffisants');
			redirect('');
		}
		$pid = SBTTable::current($table);
		SBTProduit::cancelTaxation($pid);
	}

	public function sendAllFactures()
	{
		if (!SBTParticipant::isAdmin()) {
			SBTNotification::error('Droits insuffisants');
			redirect('');
		}
		SBTParticipant::sendAllFactures();
	}


}
