<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Func extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
	}


	public function lang($language, $redirect = false)
	{
		switch ($language) {
			case 'de':
				$idiom = 'de';
				break;
			default:
			case 'fr':
				$idiom = 'fr';
		}
		$this->session->language = $idiom;
		if ($redirect) {
			redirect();
		}
	}

}
