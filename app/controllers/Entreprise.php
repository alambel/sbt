<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Entreprise extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->output->enable_profiler(DEV);
        if (!SBTParticipant::isLoggedIn()) {
            redirect('login');
        }
    }

    public function index()
    {
        redirect('entreprise/view');
    }

    public function view($participantId = null, $action = null)
    {
        if (SBTParticipant::isTaxateur()) {
            redirect('taxation');
        }
        $participant = SBTParticipant::isAdmin() && $participantId ? SBTParticipant::get($participantId) : SBTParticipant::getCurrent();

        if (!$participant) {
			if (SBTParticipant::isAdmin()) {
				SBTNotification::error('Participant inconnu');
				redirect('admin/entreprises');
			} else {
				redirect('login');
			}
        }
        if ($action == 'activate') {
            if ($participant->activate()) {
                SBTNotification::success('Entreprise activée');
            } else {
                SBTNotification::success('Erreur lors de l\'activation de l\'entreprise');
            }
            redirect('entreprise/view/' . $participant->id);
        }
        if ($action == 'sendpreinscription') {
            if ($participant->sendPreinscriptionMail()) {
                SBTNotification::success('Email envoyé avec succès');
            } else {
                SBTNotification::success('Erreur lors de l\'envoi de l\'email');
            }
            redirect('entreprise/view/' . $participant->id);
        }
        if ($action == 'sendincomplets') {
            if ($participant->sendIncompletsMail()) {
                SBTNotification::success('Email envoyé avec succès');
            } else {
                SBTNotification::success('Erreur lors de l\'envoi de l\'email');
            }
            redirect('entreprise/view/' . $participant->id);
        }
        if ($action == 'sendfacture') {
            if ($participant->sendFactureMail()) {
                SBTNotification::success('Facture envoyée avec succès');
            } else {
                SBTNotification::success('Erreur lors de l\'envoi de la facture');
            }
            redirect('entreprise/view/' . $participant->id);
        }
        $paid = $this->input->post('paid');
        if ($paid) {
            $date = $this->input->post('paiddate');
            $amount = $this->input->post('amount');
            if ($participant->setPaid($date ? strtotime($date) : null,$amount)) {
                SBTNotification::success('Date de paiement enregistrée');
            } else {
                SBTNotification::success('Erreur');
            }
            redirect('entreprise/view/' . $participant->id);
        }
        $this->load->view('participant_view', array('participant' => $participant));
    }

    public function modifier($participantId = null)
    {
        if (!SBTParticipant::isAdmin()) {
            redirect('entreprise');
        }
        $participant = SBTParticipant::isAdmin() && $participantId ? SBTParticipant::get($participantId) : SBTParticipant::getCurrent();

        $pData = $this->input->post('participant');
        if ($pData) {
            $participant->load($pData);
            if ($participant->save()) {
                $password = $this->input->post('password');
                if ($password) {
                    $participant->setPassword($password);
                }
                SBTNotification::success('Modification sauvées avec succès');
                redirect('entreprise/view/' . $participant->id);
            } else {
                SBTNotification::error('Erreur lors de la sauvegarde');
            }
        }
        $this->load->view('participant_edit', array('participant' => $participant));
    }

    public function modifier_produit($produitId = null)
    {
        redirect('entreprise');
        $produit = new SBTProduit();
        $participant = null;
        if ($produitId) {
            $produit = SBTProduit::get($produitId);
            if (!$produit) {
                SBTNotification::error('Produit introuvable');
            } else {
                $participant = SBTParticipant::get($produit->participant);
            }
        }
        if ($produit && $produit->participant != SBTParticipant::getCurrentUserId() && !SBTParticipant::isAdmin()) {
            redirect('entreprise/view');
        }
        if (!$participant) {
            $participant = $this->input->post_get('e') ? SBTParticipant::get($this->input->post_get('e')) : SBTParticipant::getCurrent();
        }
        $pData = $this->input->post('produit');
        if ($pData) {
            $produit->load($pData);
            if (!$produit->participant) {
                $produit->participant = $participant->id;
            }
            if ($produit->save()) {
                SBTNotification::success('Produit sauvegardé');
                redirect('entreprise/view/' . $produit->participant);
            }
            SBTNotification::error('Erreur lors de l\'ajout');

        }
        $this->load->view('modifier_produit', array('produit' => $produit, 'participant' => $participant));
    }


}
