<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Guest extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();

        $this->output->enable_profiler(DEV);
    }

    public function index()
    {
        $pId = $this->input->post('pid');
        if ($pId !== null) {
            redirect('guest/produit/' . $pId);
        }
        $this->load->view('taxation_guest_choixproduit');

    }


    public function produit($no = null)
    {
        $produit = SBTProduit::getForNO($no);

        if ($this->input->post('done')) {
            SBTNotification::success('Merci!');
            redirect('guest');
        }

        $this->load->view('taxation_guest', array('produit' => $produit));
        return;
    }

}
