<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Photos extends CI_Controller
{

	var $data;


	public function __construct()
	{
		parent::__construct();

		$this->output->enable_profiler(DEV);
		if (!SBTParticipant::isManager()) {
			SBTNotification::error('Droits insuffisants');
			redirect('');
		}
		$this->load->model('SBTPhoto');
		$this->data = array();
	}

	public function index()
	{
		$this->load->helper('directory');

		$this->load->view('photos_upload', $this->data);
	}

	public function assign()
	{
		if ($this->input->post('delete')) {
			$images = $this->input->post('images');
			if ($images) {
				foreach ($images as $i) {
					unlink(SBTPhoto::$uploadfolder . $i);
				}
				redirect('photos/assign');
			}
		}
		if ($this->input->post('submit')) {
			$assignto = $this->input->post('assignto');
			$images = $this->input->post('images');
			if ($assignto && $images) {
				SBTPhoto::assign($assignto, $images);
				redirect('photos/assign');
			}
		}
		$this->data['uploaded'] = $this->getPicturesByExifDate(SBTPhoto::$uploadfolder);
		$this->load->view('photos_assign', $this->data);
	}


	function getPicturesByExifDate($dir)
	{
		if ($handle = opendir($dir)) {

			$images = array(); # empty data structure
			while (($file = readdir($handle)) !== false) {
				if ($file == '..' || $file == '.' || is_dir($file) || $this->getPictureType($file) == '')
					continue;
				# only for images
				$exif = exif_read_data("$dir/$file", 0, true);
				$date = $exif['IFD0']['DateTime']; # everything you like to be ordered
				$images[$file] = $date; # associate each file to its date
			}
			asort($images); # sort the structure by date
			$imagesSlice = array_slice($images, 0, 20);
			return $imagesSlice;
		}
	}

	function getPictureType($file)
	{
		$split = explode('.', $file);
		$ext = $split[count($split) - 1];
		if (preg_match('/jpg|jpeg/i', $ext)) {
			return 'jpg';
		} else if (preg_match('/png/i', $ext)) {
			return 'png';
		} else if (preg_match('/gif/i', $ext)) {
			return 'gif';
		} else {
			return '';
		}
	}


	public function post()
	{
		$config['upload_path'] = SBTPhoto::$uploadfolder;
		$config['allowed_types'] = 'jpg|jpeg|zip';
		$config['max_size'] = 0;

		$this->load->library('upload', $config);
		if (!$this->upload->do_upload('file')) {
			header("HTTP/1.0 500 Internal Server Error");
			print_r('Erreur lors de l\'envoi');
			exit;
		} else {
			$uploadData = $this->upload->data();

			if ($uploadData['file_type'] == 'application/zip') {
				## Extract the zip file ---- start
				$zip = new ZipArchive;
				$res = $zip->open($uploadData['full_path']);
				if ($res === TRUE) {
					// Extract file
					$zip->extractTo(SBTPhoto::$uploadfolder);
					$zip->close();
					unlink($uploadData['full_path']);
				}
			}
			print_r('Image Uploaded Successfully.');
			exit;
		}
	}

	public function dl()
	{
		SBTPhoto::generateAllPhotosZip();
		header("Content-type: application/zip");
		header("Content-Disposition: attachment; filename=SBT2024-Photos.zip");
		header("Content-length: " . filesize(SBTPhoto::$allPhotosZip));
		header("Pragma: no-cache");
		header("Expires: 0");
		readfile(SBTPhoto::$allPhotosZip);
	}
}
