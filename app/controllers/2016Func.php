<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Func extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }



    public function cron() {
        if (true) {
            $this->load->library('email');
            $this->email->from('lambelet@njs.ch');
            $this->email->to('lambelet@njs.ch');

            $this->email->subject('SBT Backup - ' . date('Y-m-d H:i:m'));
            $this->email->message('');
            $filename = 'SBT' . date('YmdHim') . '.csv';
            $result = $this->db->get('produit')->result_array();
            if (!$result)
                die('db error');
            $tmpfile = 'sbtbackup/' . $filename;

            $fp = fopen($tmpfile, "w");

            if ($fp && $result) {
                fwrite($fp, "\xEF\xBB\xBF"); // UTF-8 BOM
                fputcsv($fp, array('no',
                    'number',
                    'name',
                    'category',
                    'comment',
                    'entered_by',
                    'entered_on',
                    'entered_ip',
                    'lang',
                    'validated',
                    'canton',
                    'entreprise',
                    'npa',
                    'localite',
                    'medaille',
                    'mail',
                    'datescan',
                    'notes',
                    'moyenne',
                    'adresse',
                    'tel',
                    'fax',
                    'portable',
                    'datetaxation',
                    'responsable'
                ));
                foreach ($result as $r) {
                    fputcsv($fp, array_values($r));
                }
            }
//        $this->load->helper('directory');

            $metaDatas = stream_get_meta_data($fp);
            $tmpFilename = $metaDatas['uri'];
            $this->email->attach($tmpFilename);
            fclose($fp); // this removes the file
            $rs = $this->email->send();
            echo $rs ? 'done' : 'error';
        }
    }

}
