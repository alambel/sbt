<?php $this->load->view('base/header');
/** @var $uploaded [] */
SBTInclude::jsFile("assets/plugins/dropzone/dropzone.min.js");
SBTInclude::css("assets/plugins/dropzone/dropzone.min.css");
SBTInclude::jsFile("assets/plugins/image-picker/image-picker.min.js");
SBTInclude::css("assets/plugins/image-picker/image-picker.css");

?>

<div class="container sm-padding-10 p-t-20 p-l-0 p-r-0">

	<div class="row">
		<div class="col-lg-12 col-sm-12">
			<div class=" card widget-loader-circle todolist-widget pending-projects-widget">
				<div class="card-header ">
					<div class="card-title"><h2>Attribuer les images au produit</h2></div>
				</div>
				<div class="card-block">
					<?= form_open() ?>
					<div class="row m-b-20">
						<div class="col-sm-12">
							<div class="form-group">
								<label>Produit</label>
								<?= form_dropdown(array('name' => 'assignto', 'class' => '', 'data-init-plugin' => 'select2', 'style' => 'width:400px'), SBTProduit::dropdown(), null) ?>
								<?= form_submit(array('name' => 'submit', 'class' => 'btn btn-primary'), 'Assigner les images sélectionnées'); ?>
								<?= form_submit(array('name' => 'delete', 'class' => 'btn btn-danger pull-right'), 'Supprimer les images sélectionnées'); ?>
							</div>
						</div>
					</div>
					<div class="row">
						<select multiple="multiple" class="image-picker show-html" name="images[]" id="image-picker">
							<?php foreach ($uploaded as $file => $date): ?>
								<option data-img-src="<?= base_url('cache/uploads/' . $file); ?>" value="<?= $file ?>"><?= $file ?> (<?= $date ?>)</option>
							<?php endforeach; ?>
						</select>
					</div>
					<?= form_close() ?>

					<div class="row">
						<a href="<?= site_url('photos') ?>" class="btn btn-primary">Envoyer d'autres photos</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php ob_start(); ?>
<script type="application/javascript">
	$(document).ready(function () {
		$("#image-picker").imagepicker({
			'show_label': true
		});
	});
</script>
<?php
SBTInclude::js(ob_get_clean());
$this->load->view('base/footer'); ?>



