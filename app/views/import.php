<!DOCTYPE html>
<html>

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>SBT2016</title>

        <link rel="shortcut icon" type="image/png" href="/favicon.png"/>
        <link href="<?php echo base_url('css/bootstrap.min.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('font-awesome/css/font-awesome.css'); ?>" rel="stylesheet">

        <link href="<?php echo base_url('css/animate.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('css/style.css'); ?>" rel="stylesheet">
        <style>
            .ddd {
                color: #ddd;
            }</style>

    </head>

    <body class="gray-bg">

        <div class="middle-box text-center animated fadeInDown">
            <div>
                <div class="m-b-lg m-t-lg">
                    <img src="<?php echo base_url('img/sbt2016.jpg'); ?>" alt="Swiss Bakery Trophy" class="img-responsive center-block"/>
                </div>
                <h3><?php echo $title;?></h3>

                <?php
                if (isset($error) || isset($succes)):
                    if ($succes):
                        ?>
                        <div class="alert alert-success">
                            <?php echo $succes; ?>
                        </div>
                        <?php
                    endif;
                    if ($error):
                        ?>
                        <div class="alert alert-danger">
                            <?php echo $error; ?>
                        </div>

                    <?php endif; ?>
                <?php endif; ?>
                <?php echo form_open_multipart(''); ?>
                <div class="form-group">
                    <label class="btn btn-default btn-primary">
                        Choisir le fichier... <input type="file"  name="fizz" style="display: none;">
                    </label>
                </div>
                <br/>
                <button type="submit" name="" value="" class="btn btn-primary block full-width m-b">Envoyer</button>
                <a class="btn btn-sm btn-white btn-block" href="<?php echo base_url('admin'); ?>">Retour</a>

                </form><div class="pull-right">

                </div>

                <p class="m-t"> <small>&copy; <?php echo date('Y'); ?> - Swiss Bakery Trophy</small> </p>
            </div>
        </div>

        <!-- Mainly scripts -->
        <script src="<?php echo base_url('js/jquery-2.1.1.js'); ?>"></script>
        <script src="<?php echo base_url('js/bootstrap.min.js'); ?>"></script>
        <script>

            $(document).ready(function () {
                $("#comment").keydown(function (e) {
                    if (e.keyCode === 13 && !e.shiftKey)
                    {
                        // prevent default behavior
                        e.preventDefault();
//                        alert("ok");
                        return false;
                    }
                });
            });
        </script>
    </body>

</html>
