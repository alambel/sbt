<?php
/**
 * @var $table SBTTable
 * @var $taxation SBTTaxation
 * @var $producteur SBTParticipant
 * @var $participant SBTParticipant
 * @var $produit SBTProduit
 */

$this->load->view('base/header_taxation'); ?>
<div class="container">
	<div class="card">
		<div class="card-header ">
			<div class="card-title">
				<h1><?= t('taxation') ?></h1>
				<h4><?= t('table') ?> <?= $participant->table ?></h4>
			</div>
		</div>
		<div class="card-block">
			<p><b><?= t('categorie') ?></b>: <?= SBTProduit::$categories[$produit->categorie]; ?></p>
			<p><b><?= t('composition') ?></b>: <?= $produit->composition ?></p>
			<p><b><?= t('arguments_de_vente') ?></b>: <?= $produit->arguments ?></p>
			<p><b><?= t('langue') ?>:</b> <?= $producteur->lang ?></p>
			<hr>
			<?php if ($taxation):
				$results = SBTTaxation::get($produit->id);
				$taxations = array();
				if ($results)
					foreach ($results as $r) {
						$taxations[$r->taxateur] = $r;
					}
				?>
				<h2>NO <?= $produit->no ?> : <?= $produit->nom ?></h2>
				<table class="col-sm-12 table-bordered table-striped">
					<tr>
						<th><?= t('critere') ?></th>

						<?php for ($i = 1; $i <= 5; $i++): ?>
							<th><span style="color:<?= isset($taxations[$i]) ? 'green' : 'orange' ?>"><?= t('taxateur') ?> <?= $i ?></span></th>
						<?php endfor ?>
					</tr>
					<?php foreach (array('aspect', 'forme', 'surface', 'texture', 'bouche', 'odeur') as $critere): ?>
						<tr>
							<td><?= t($critere) ?></td>
							<?php for ($i = 1; $i <= 5; $i++): ?>
								<td><?php if (isset($taxations[$i])): ?>
										<span class="note"
											  onclick="startEdit(this)"
											  data-participant="<?= $i ?>"
											  data-critere="<?= $critere ?>"
											  style="color:green;"
											  id="label-<?= $i ?>-<?= $critere ?>"><?= $taxations[$i]->{$critere} ?></span>
										<div id="div-<?= $i ?>-<?= $critere ?>"
											 style="display: none;">
											<input
													style="70px"
													type="number"
													data-participant="<?= $i ?>"
													data-critere="<?= $critere ?>"
													name="n-<?= $i ?>-<?= $critere ?>"
													id="note-<?= $i ?>-<?= $critere ?>"
													value="<?= $taxations[$i]->{$critere} ?>"
													min="1" max="10"
											/>
											<button onclick="doEdit(<?= $i ?>,'<?= $critere ?>')"><i class="fa fa-check" aria-hidden="true"></i></button>
										</div>
										<br><?= $taxations[$i]->{$critere . '_commentaire'} ?>&nbsp;
									<?php else: ?>
										<span style="color:orange;"><?= t('en_attente') ?></span>
									<?php endif; ?>
								</td>
							<?php endfor ?>
						</tr>
					<?php endforeach; ?>
				</table>

				<?php
				$alldone = true;
				$nonedone = true;
				for ($i = 1; $i <= 5; $i++) {
					$done = SBTTaxation::done($produit->id, $i, $table);
					$alldone = $done ? $alldone : false;
					$nonedone = $done ? false : $nonedone;
				}
				?>
			<?php endif; ?>
			<hr>
			<?php if ($alldone): ?>
				<p><?php
					$lang = null;
					switch ($producteur->lang) {
						case 'D':
							$lang = t('allemand');
							break;
						case 'F':
							$lang = t('francais');
							break;
					}
					echo $lang ? sprintf(t('entrer_commentaire_en_X'), $lang) : null; ?></p>
				<form class="m-t" role="form" action="<?= SBTTable::current($table) ? current_url() : site_url('taxation/update'); ?>" method="POST">
					<input type="hidden" name="pid" value="<?=$produit->id?>"/>
					<div class="form-group">
                            <textarea name="comment" id="comment" class="form-control" placeholder="<?= t('commentaire') ?>" required="" autofocus
									  maxlength="255"><?= $produit->commentaire ?></textarea>
					</div>
					<br/>
					<button type="submit" name="" value="" class="btn btn-primary m-b"><?= t('sauver') ?></button>
				</form>
			<?php endif; ?>

			<?php if ($nonedone): ?>
				<a href="<?= site_url('taxation/canceltable') ?>" class="btn bt-white"><?= t('choisir_autre_produit') ?></a>
			<?php endif; ?>
		</div>

	</div>
</div>
<?php
ob_start() ?>
<script type="application/javascript">
	var timeout = null;
	<?php if (!$alldone):?>
	$(document).ready(function () {
		setReloadTimeout();
	});
	<?php endif;?>
	function startEdit(e) {
		clearTimeout(timeout)
		var p = $(e).data("participant");
		var c = $(e).data("critere");
		$('#div-' + p + '-' + c).show();
		$('#label-' + p + '-' + c).hide();

	}

	function doEdit(p, c) {
		clearReloadTimeout();
		$('#div-' + p + '-' + c).hide();
		$.post("<?= site_url('taxation/post')?>", {p: <?=$produit->id?>, t: p, c: c, n: $('#note-' + p + '-' + c).val()})
			.done(function (data) {
				var rs = jQuery.parseJSON(data)
				if (rs.success) {
					$('#label-' + p + '-' + c).text(rs.note).show();
					$('#div-' + p + '-' + c).hide();
					<?php if (!$alldone):?>
					setReloadTimeout();
					<?php else:?>
					window.location.replace("<?=site_url('taxation/table/' . $produit->no)?>");
					<?php endif;?>
				}

			});
	}

	function clearReloadTimeout() {
		if (timeout) {
			clearTimeout(timeout)
		}
	}

	function setReloadTimeout() {
		timeout = setTimeout(function () {
			window.location.reload(1);
		}, 6000); //set to 5000 for prod
	}

</script>
<?php
SBTInclude::js(ob_get_clean());

$this->load->view('base/footer_taxation'); ?>
