<?php $this->load->view('base/header');

$counts = SBTTaxateur::getCounts();
?>

    <div class="container sm-padding-10 p-t-20 p-l-0 p-r-0">
        <div class="row">
            <div class="col-lg-12 col-sm-12">
                <div class=" card widget-loader-circle todolist-widget pending-projects-widget">
                    <div class="card-header ">
                        <div class="card-title"><h2>Taxateurs</h2></div>
                    </div>
                    <div class="card-block">
                        <div class="col-md-12 p-b-30">
                            <h3>Par jour</h3>

                            <table class="table-sm table-striped" style="text-align: center; width:400px;">
                                <tr>
                                    <th style="text-align: center;">Date</th>
                                    <th style="text-align: center;">30.10</th>
                                    <th style="text-align: center;">31.10</th>
                                    <th style="text-align: center;">01.11</th>
                                    <th style="text-align: center;">02.11</th>


                                </tr>
                                <tr>
                                    <th style="text-align: center;">Matin<br>conso / pro</th>
                                    <td><b><?= $counts['conso']->tot1_am + $counts['pro']->tot1_am ?></b><br><?= $counts['conso']->tot1_am ?> / <?= $counts['pro']->tot1_am ?></td>
                                    <td><b><?= $counts['conso']->tot2_am + $counts['pro']->tot2_am ?></b><br><?= $counts['conso']->tot2_am ?> / <?= $counts['pro']->tot2_am ?></td>
                                    <td><b><?= $counts['conso']->tot3_am + $counts['pro']->tot3_am ?></b><br><?= $counts['conso']->tot3_am ?> / <?= $counts['pro']->tot3_am ?></td>
                                    <td><b><?= $counts['conso']->tot4_am + $counts['pro']->tot4_am ?></b><br><?= $counts['conso']->tot4_am ?> / <?= $counts['pro']->tot4_am ?></td>

                                </tr>
                                <tr>
                                    <th style="text-align: center;">Après-midi<br>conso / pro</th>
                                    <td><b><?= $counts['conso']->tot1_pm + $counts['pro']->tot1_pm ?></b><br><?= $counts['conso']->tot1_pm ?> / <?= $counts['pro']->tot1_pm ?></td>
                                    <td><b><?= $counts['conso']->tot2_pm + $counts['pro']->tot2_pm ?></b><br><?= $counts['conso']->tot2_pm ?> / <?= $counts['pro']->tot2_pm ?></td>
                                    <td><b><?= $counts['conso']->tot3_pm + $counts['pro']->tot3_pm ?></b><br><?= $counts['conso']->tot3_pm ?> / <?= $counts['pro']->tot3_pm ?></td>
                                    <td><b><?= $counts['conso']->tot4_pm + $counts['pro']->tot4_pm ?></b><br><?= $counts['conso']->tot4_pm ?> / <?= $counts['pro']->tot4_pm ?></td>
                                </tr>
                            </table>
                        </div>

                        <div class="col-md-12  p-b-30">
                            <h3>Par cantons</h3>
                            <table class="table-bordered table-sm" style="width:100%">
                                <tr>
                                    <th>Canton</th>
                                    <?php foreach (SBTParticipant::$cantons as $canton => $count): ?>
                                        <?php if ($canton): ?>
                                            <td><?= $canton; ?></td>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </tr>
                                <tr>
                                    <th>Inscrits</th>
                                    <?php $parCanton = SBTTaxateur::getParCanton();
                                    foreach (SBTParticipant::$cantons as $canton => $count): ?>
                                        <?php if ($canton): ?>
                                            <td><?= isset($parCanton[$canton]) ? $parCanton[$canton] : null; ?></td>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </tr>
                            </table>
                        </div>


                        <div class="col-md-12 ">
                            <table id="taxateurs" class="table table-hover table-responsive">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Nom</th>
                                    <th>Prenom</th>
                                    <th>Entreprise</th>
                                    <th>Tel</th>
                                    <th>Rue</th>
                                    <th>Npa</th>
                                    <th>Lieu</th>
                                    <th>Canton</th>
                                    <th>Email</th>
                                    <th>30.10 M</th>
                                    <th>30.10 A</th>
                                    <th>31.10 M</th>
                                    <th>31.10 A</th>
                                    <th>01.11 M</th>
                                    <th>01.11 A</th>
                                    <th>02.11 M</th>
                                    <th>02.11 A</th>
                                    <th>Dispo</th>
									<th>Remarque</th>
                                    <th>Type</th>
                                    <th>Frais jour</th>
                                    <th>Frais dépl.</th>
                                    <th>Frais non</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if ($taxateurs):
                                    foreach ($taxateurs as $t): ?>
                                        <tr>
                                            <td><?= $t->id; ?></td>
                                            <td><a href="<?= site_url('admin/taxateur_edit/' . $t->id) ?>"><?= $t->nom ?></a></td>
                                            <td><?= $t->prenom ?></td>
                                            <td><?= $t->entreprise ?></td>
                                            <td><?= $t->telephone; ?></td>
                                            <td><?= $t->rue ?></td>
                                            <td><?= $t->npa ?></td>
                                            <td><?= $t->lieu ?></td>
                                            <td><?= $t->canton; ?></td>
                                            <td><?= mailto($t->email, $t->email); ?></td>
                                            <td><?php if ($t->jour1_am): ?><span class="text-success">oui</span><? endif; ?></td>
                                            <td><?php if ($t->jour1_pm): ?><span class="text-success">oui</span><? endif; ?></td>
                                            <td><?php if ($t->jour2_am): ?><span class="text-success">oui</span><? endif; ?></td>
                                            <td><?php if ($t->jour2_pm): ?><span class="text-success">oui</span><? endif; ?></td>
                                            <td><?php if ($t->jour3_am): ?><span class="text-success">oui</span><? endif; ?></td>
                                            <td><?php if ($t->jour3_pm): ?><span class="text-success">oui</span><? endif; ?></td>
                                            <td><?php if ($t->jour4_am): ?><span class="text-success">oui</span><? endif; ?></td>
                                            <td><?php if ($t->jour4_pm): ?><span class="text-success">oui</span><? endif; ?></td>
                                            <td><?= $t->dispo == 2 ? 'tous':'un seul'; ?></td>
											<td><?= $t->note?></td>
                                            <td><?= $t->type; ?></td>
                                            <td><?php if ($t->frais_jour): ?><span class="text-success">oui</span><? endif; ?></td>
                                            <td><?php if ($t->frais_deplacement): ?><span class="text-success">oui</span><? endif; ?></td>
                                            <td><?php if ($t->frais_non): ?><span class="text-success">oui</span><? endif; ?></td>

                                            <td>
                                                <button onclick="delTaxateur(<?= $t->id ?>);">Supprimer</button>
                                            </td>
                                        </tr>
                                    <?php endforeach;
                                endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php ob_start(); ?>
    <script type="application/javascript">
        $(document).ready(function () {
            $('#taxateurs').DataTable({
                stateSave: true,
				scrollX: true,
                dom: '<"top"Blif>rt<"bottom"p><"clear">',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        text: 'Exporter Excel',
                    }
                ],
                render: {
                    _: 'display',
                    sort: 'timestamp'
                }
            });
        });

        function delTaxateur(taxateurId) {
            if (confirm('Voulez-vous vraiment supprimer ce taxateur?')) {
                window.location = '<?=site_url('admin/delete_taxateur')?>/' + taxateurId;
            }
        }
    </script>
<?php
SBTInclude::js(ob_get_clean());

$this->load->view('base/footer'); ?>
