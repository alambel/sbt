<?php
if (SBTParticipant::isTaxateur()) {
	redirect('taxation');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-121731481-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}

		gtag('js', new Date());

		gtag('config', 'UA-121731481-1');
	</script>

	<meta charset="utf-8">
	<title>Swiss Bakery Trophy</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="stylesheets/menu.css">
	<!-- CSS
	================================================== -->
	<link rel="stylesheet" href="stylesheets/menu.css">
	<link rel="stylesheet" href="stylesheets/flat-ui-slider.css">
	<link rel="stylesheet" href="stylesheets/base.css">
	<link rel="stylesheet" href="stylesheets/skeleton.css">
	<link rel="stylesheet" href="stylesheets/landings.css">
	<link rel="stylesheet" href="stylesheets/main.css?2">
	<link rel="stylesheet" href="stylesheets/landings_layouts.css">
	<link rel="stylesheet" href="stylesheets/box.css">
	<link rel="stylesheet" href="stylesheets/pixicon.css">
	<style>
		.parrains img {
			width: 220px;
		}

		.partners img {
			width: 220px;
		}

		ul {
			list-style-type: disc;
			padding: revert;
		}
	</style>
	<!--[if lt IE 9]>
	<script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<!-- Favicons
	================================================== -->
	<link rel="shortcut icon" href="favicon.ico">
</head>
<body>

<div class="header_nav_1 dark inter_3_bg pix_builder_bg" id="section_intro_3">
	<div class="header_style">
		<div class="container">
			<div class="sixteen columns firas2">
				<nav role="navigation" class="navbar navbar-white navbar-embossed navbar-lg pix_nav_1">
					<div class="containerss">
						<div class="navbar-header" style="background-color: white;">
							<button data-target="#navbar-collapse-02" data-toggle="collapse" class="navbar-toggle" type="button">
								<span class="sr-only">Toggle navigation</span>
							</button>
							<img src="img/logosbt2024.jpg" class="pix_nav_logo" alt="" height="100">
						</div>
						<div id="navbar-collapse-02" class="navbar-collapse">
							<ul class="nav navbar-nav navbar-right bg-sbt">
								<li class="propClone"><a href="<?= site_url('') ?>" style="color: white"><?= t('Accueil'); ?></a></li>
								<li class="active propClone"><a href="<?= site_url('assets/SBT2024-Reglement_' . lang() . '.pdf') ?>" target="_blank" style="color: white"><?= t('reglement') ?></a></li>
								<!--								<li class="propClone"><a href="--><? //= site_url('inscription') ?><!--" style="color: white">--><? //= t('inscription_entreprise') ?><!--</a></li>-->
								<!--								<li class="propClone"><a href="--><? //= site_url('inscription_taxateur') ?><!--" style="color: white">--><? //= t('inscription_taxateur') ?><!--</a></li>-->
								<?php $user = SBTParticipant::getCurrentUserId();
								if ($user):?>
									<li class="propClone"><a href="<?= site_url(SBTParticipant::isTaxateur() ? 'taxation' : 'entreprise') ?>" style="color: white"><?= t('mon_compte'); ?></a></li>
								<?php else: ?>
									<li class="propClone"><a href="<?= site_url('login') ?>" style="color: white"><?= t('login'); ?></a></li>
								<?php endif; ?>
								<?php $this->load->view('base/language_selector'); ?>
							</ul>
						</div><!-- /.navbar-collapse -->
					</div><!-- /.container -->
				</nav>
			</div>
		</div><!-- container -->
	</div>
</div>


<div class="" id="section_services">
	<div class="big_padding padding_bottom_0 pix_builder_bg">
		<div class="container">
			<div class="sixteen columns">
				<div class="sixteen columns alpha">
					<p class="big_title bold_text center_text editContent" style="font-size:42px;">Swiss Bakery Trophy Academy</p>
				</div>
				<div class="sixteen columns alpha">
					<p class="big_text center_text editContent">
					<div class="main-text event">
						<video width="100%" height="auto" controls autoplay>
							<source src="img/SBTA_v.2-DE_FR.mp4" type="video/mp4">
							Your browser does not support the video tag.
						</video>
						<?= t('SBTAcademy_body')?>
					</div>
				</div>
			</div>
		</div>
	</div>



	<div class="pix_builder_bg dark soft_dark_gray_bg" id="section_footer_5_dark">
		<div class="footer3">
			<div class="container">
				<div class="sixteen columns ">
					<div class="four column alpha mobile_center margin_vertical ">
						<div class="pix_div_fit">
							<img src="img/logosbt2024.jpg" class="pix_footer_logo pix_img_fit" alt="" width="200">
						</div>
					</div>
					<div class="four column alpha mobile_center margin_vertical ">
						<p class="small_text light_gray left_text mobile_center editContent"><?= t('welcome_2_title') ?></p>
						<p><a href="<?= site_url('assets/SBT2024-Reglement_fr.pdf') ?>" target="_blank" style="color: white">Règlement et conditions de participation</a><br>
							<a href="<?= site_url('assets/SBT2024-Reglement_de.pdf') ?>" target="_blank" style="color: white">Reglement und Teilnahmebedingungen</a></p>
						<ul class="bottom-icons center_text big_title">
							<li><a class="pi pixicon-instagram2 normal_gray" target="_parent" href="https://www.instagram.com/swissbakerytrophy/"></a></li>
							<li><a class="pi pixicon-linkedin2 normal_gray" target="_parent" href="https://www.linkedin.com/company/swiss-bakery-trophy/"></a></li>
							<li><a class="pi pixicon-facebook2 normal_gray" target="_parent" href="https://www.facebook.com/swissbakerytrophy/"></a></li>
							<li><a class="pi pixicon-twitter2 normal_gray" target="_blank" href="https://twitter.com/chbakerytrophy"></a></li>
							<li><a class="pi pixicon-flickr2 normal_gray" target="_blank" href="https://www.flickr.com/photos/127946990@N07/"></a></li>
						</ul>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>

<script src="js/jquery-2.1.1.js" type="text/javascript"></script>
<script src="js/jquery.easing.1.3.js" type="text/javascript"></script>
<script src="js/jquery.ui.touch-punch.min.js"></script>
<script src="js/ticker.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bootstrap-switch.js"></script>
<script src="js/appear.min.js" type="text/javascript"></script>
<script src="js/smoothscroll.min.js" type="text/javascript"></script>
<script src="js/custom.js?2" type="text/javascript"></script>
</body>
</html>
