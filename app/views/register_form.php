<div class="row ">
	<div class="col-md-12">
		<div class="form-group form-group-default required">
			<label><?= t('entreprise') ?></label>
			<input type="text" class="form-control" name="participant[entreprise]"
				   value="<?= $participant ? $participant->entreprise : set_value('participant[entreprise]'); ?>">
		</div>
	</div>
</div>
<div class="row ">
	<div class="col-md-6">
		<div class="form-group form-group-default required">
			<label><?= t('nom') ?></label>
			<input type="text" class="form-control" name="participant[nom]" required
				   value="<?= $participant ? $participant->nom : set_value('participant[nom]'); ?>">
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group form-group-default required">
			<label><?= t('prenom') ?></label>
			<input type="text" class="form-control" name="participant[prenom]" required
				   value="<?= $participant ? $participant->prenom : set_value('participant[prenom]'); ?>">
		</div>
	</div>
</div>
<div class="row ">
	<div class="col-md-12">
		<div class="form-group form-group-default required">
			<label><?= t('rue') ?></label>
			<input type="text" class="form-control" name="participant[rue]" required
				   value="<?= $participant ? $participant->rue : set_value('participant[rue]'); ?>">
		</div>
	</div>
</div>
<div class="row ">
	<div class="col-sm-4">
		<div class="form-group form-group-default required">
			<label><?= t('npa') ?></label>
			<input type="text" class="form-control" name="participant[npa]" required id="npa" maxlength="4"
				   value="<?= $participant ? $participant->npa : set_value('participant[npa]'); ?>">
		</div>
	</div>
	<div class="col-sm-8">
		<div class="form-group form-group-default required">
			<label><?= t('ville') ?></label>
			<input type="text" class="form-control" name="participant[lieu]" required value="<?= $participant ? $participant->lieu : set_value('participant[lieu]'); ?>">
		</div>
	</div>
</div>
<div class="row ">
	<div class="col-sm-12">
		<div class="form-group form-group-default required">
			<label><?= t('canton') ?></label>
			<?= form_dropdown('participant[canton]', SBTParticipant::$cantons, $participant ? $participant->canton : set_value('participant[canton]'), 'id="canton" class="full-width" data-init-plugin="select2" required'); ?>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="form-group form-group-default required">
			<label><?= t('email') ?></label>
			<input type="<?= SBTParticipant::isAdmin() ? 'text' : 'email' ?>" class="form-control" name="participant[email]" required
				   value="<?= $participant ? $participant->email : set_value('participant[email]'); ?>">
		</div>
	</div>
</div>
<div class="row ">
	<div class="col-md-6">
		<div class="form-group form-group-default">
			<label><?= t('telephone') ?></label>
			<input type="phone" class="form-control" name="participant[telephone]" id="telephone" value="<?= $participant ? $participant->telephone : set_value('participant[telephone]'); ?>">
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group form-group-default required">
			<label><?= t('mobile') ?></label>
			<input type="phone" class="form-control" id="mobile" name="participant[mobile]" required value="<?= $participant ? $participant->mobile : set_value('participant[mobile]'); ?>">
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="form-group  required">
			<label><?= t('langue') ?></label>
			<div class="radio radio-success">
				<input type="radio" value="F" id="fr" name="participant[langue]" required <?= $participant && $participant->lang == 'F' || lang() != 'de' ? 'checked' : '' ?>>
				<label for="fr"><?= t('francais') ?></label>
				<input type="radio" value="D" id="de" name="participant[langue]" required <?= $participant && $participant->lang == 'D' || lang() == 'de' ? 'checked' : '' ?>>
				<label for="de"><?= t('allemand') ?></label>
			</div>
		</div>
	</div>
</div>
<?php ob_start(); ?>
<script type="application/javascript">

	var npacantons = <?=json_encode(npaCantons())?>;

	$(document).ready(function () {
		$('form').bind('submit', function () {
			$('#canton').attr("disabled", false);
		});

		// Demande de Catherine (10.09.2024) ne pas rendre le téléphone obligatoire mais le mobile oui.
		// var inputs = $('#telephone,#mobile');
		// inputs.on('input', function () {
			// Set the required property of the other input to false if this input is not empty.
			// inputs.not(this).prop('required', !$(this).val().length);
		// });
		// $('#telephone').prop('required', !$('#mobile').val().length);
		// $('#mobile').prop('required', !$('#telephone').val().length);

		$('#npa').change(function () {
			var npa = $(this).val();
			var canton = npacantons[npa];
			if (canton) {
				$('#canton').val(canton).change();
				$('#canton').attr("disabled", true);
				console.log('canton changed to', canton);
			} else {
				$('#canton').attr("disabled", false);
			}
		});
	});


</script>
<?php SBTInclude::js(ob_get_clean()); ?>
