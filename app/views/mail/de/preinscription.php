<p>Sehr geehrte Damen und Herren<br>Liebe Mitglieder,</p>
<p>Sie haben per Post das Anmeldeformular für die 8. Swiss Bakery Trophy erhalten, die von Mittwoch, 31. Oktober bis Sonntag, 4. November 2021 in Bulle stattfindet.</p>
<p>Für diese Ausgabe haben wir eine Computergestützte Registrierungsplattform entwickelt, zu deren Nutzung wir Sie herzlich einladen.</p>
<p>Da Sie bereits an der Swiss Bakery Trophy 2016 teilgenommen haben, haben wir Ihre Basisdaten übernommen. Dann haben Sie die Möglichkeit, Ihre Daten zu übertragen oder zu ändern, indem Sie auf den untenstehenden Link klicken:</p>
<p><?= anchor($participant->getPreinscriptionUrl()) ?></p>
<p>Die Durchlauftage der Kantone richten sich nach der Anzahl der registrierten Produkte.</p>
<p>Wir freuen uns auf Ihre Anmeldung und verbleiben mit freundlichen Grüssen.</p>

<p>Catherine Oberson<br>
    Secrétaire<br>
    <br>
    Association Romande des Artisans<br>
    Boulangers-Pâtissiers-Confiseurs<br>
    Fédération Patronale et Economique<br>
	Rue de la Condémine 56<br>
	Case Postale<br>
	1630 Bulle<br>
    Tél. 026 919 87 51<br>
    <br>
    www.swissbakerytrophy.ch</p>
