<p>Sehr geehrte Damen und Herren</p>
<p>Mit grosser Freude teilen wir Ihnen mit, dass der Westschweizerische Bäcker-Konditor-Confiseurmeisterverband die 8. Swiss Bakery Trophy organisiert. Sie findet vom 28. Oktober bis 1. November 2021 im Espace Gruyère in Bulle statt und ist in die Schweizer Messe für heimische Genüsse "Goûts & Terroirs“ integriert.</p>
<p>Die Swiss Bakery Trophy ist ein wichtiger Anlass der Schweizerischen gewerblichen Bäckerei-Confiseriebranche. Sie ermöglicht einen Einblick in die Qualität und Sortimentsvielfalt unseres Berufes in der ganzen Schweiz. Der Wettbewerb – mit Fokus auf die Qualität der Produkte – vereint die Meinung von Konsumenten und Berufsleuten. Diese Zusammensetzung der Jury ermöglicht eine genaue Auswertung der Bedürfnisse und der Geschmacksvorlieben der Kunden.</p>
<p>Jedes Produkt wird durch eine Gruppe von fünf Personen bewertet: drei Berufsleuten und zwei Konsumenten. Jede Gruppe bewertet mehr als 30 Produkte pro Tag. Die Bewertungen finden vom 28. Oktober bis 1. November 2021, von 10.30 bis 20.00 Uhr inklusive Pausen statt.</p>
<p>Haben Sie bereits an den vergangenen Austragungen als Experte mitgewirkt oder interessieren Sie sich dafür, dieses Jahr erstmals als Mitglied der Jury mitzuarbeiten? Wir erlauben uns, Sie für einen Einsatz an der nächsten Swiss Bakery Trophy 2021 anzufragen. Gerne können Sie nach Verfügbarkeit und persönlichen Wünschen an einem oder mehreren der vier Tage diese Aufgabe übernehmen (Halbtagsmöglichkeiten). Sie können sich online über den folgenden Link anmelden: <a href="https://www.swissbakerytrophy.ch">www.swissbakerytrophy.ch</a>, den Sie auch jedem Interessierten mitteilen können.</p>
<p>Wir danken Ihnen für Ihr Interesse an der Swiss Bakery Trophy und freuen uns auf Ihre Antwort. Eine Teilnahme Ihrerseits als Experte würden wir sehr schätzen und übermitteln Ihnen hiermit unsere besten Grüsse.</p>
<p><strong>Westschweizer Bäcker-Konditorenmeisterverband<br>
        Swiss Bakery Trophy</strong></p>
<p>Nicolas Taillens, Chef Technik<br>Catherine Oberson, Sekretärin<br>
    <br>
    Association Romande des Artisans<br>
    Boulangers-Pâtissiers-Confiseurs<br>
    Fédération Patronale et Economique<br>
    Rue Condémine 56 – CP 2175<br>
    1630 Bulle 2<br>
    Tél. 026 919 87 51<br>
    <br>
    www.swissbakerytrophy.ch</p>
