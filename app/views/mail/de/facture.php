<p>Guten Tag <?= $participant->prenom; ?> <?= $participant->nom ?>,</p>
<p>In der Beilage erhalten Sie die Rechnung für die Teilnahme am Swiss Bakery Trophy 2021.</p>
<p>Im Voraus danken wir Ihnen für Ihre Zahlung und verbleiben mit freundlichen Grüssen.</p>

<p>Catherine Oberson<br>
    Secrétaire<br>
    <br>
    Association Romande des Artisans<br>
    Boulangers-Pâtissiers-Confiseurs<br>
    Fédération Patronale et Economique<br>
    Rue de la Condémine 56<br>
	Case Postale<br>
    1630 Bulle<br>
    Tél. 026 919 87 51<br>
    <br>
    www.swissbakerytrophy.ch</p>
