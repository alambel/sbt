<p>Bonjour,</p>
<p>Nous vous remettons ci-joint notre facture pour le règlement de votre participation au Swiss Bakery Trophy 2024.</p>
<p>D’avance, nous vous remercions pour votre paiement et vous adressons nos meilleures salutations<p>


<p>Catherine Oberson<br>
    Secrétaire<br>
    <br>
    Association Romande des Artisans<br>
    Boulangers-Pâtissiers-Confiseurs<br>
    Fédération Patronale et Economique<br>
    Rue Condémine 56 – CP 2175<br>
    1630 Bulle 2<br>
    Tél. 026 919 87 51<br>
    <br>
    www.swissbakerytrophy.ch</p>
