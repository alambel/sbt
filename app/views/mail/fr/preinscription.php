<p>Mesdames, Messieurs,<br>Chers Membres,</p>
<p>Vous avez reçu par courrier le dossier d’inscription au 8e Swiss Bakery Trophy qui se déroulera à Bulle du mercredi 31 octobre au dimanche 4 novembre 2018.</p>
<p>Pour cette édition, nous avons développé une plateforme informatique de gestion des inscriptions que nous vous invitons vivement à utiliser.</p>
<p>Comme vous avez déjà participé au Swiss Bakery Trophy 2016, nous avons repris vos données de base. Dès lors, en cliquant sur le lien ci-après, vous avez la possibilité de reprendre ou de modifier vos données :</p>
<p><?= anchor($participant->getPreinscriptionUrl()) ?></p>
<p>Les jours de passage des cantons seront définis en fonction du nombre de produits inscrits.</p>
<p>Nous nous réjouissons de votre inscription et vous adressons nos meilleures salutations.</p>

<p>Catherine Oberson<br>
    Secrétaire<br>
    <br>
    Association Romande des Artisans<br>
    Boulangers-Pâtissiers-Confiseurs<br>
    Fédération Patronale et Economique<br>
    Rue Condémine 56 – CP 2175<br>
    1630 Bulle 2<br>
    Tél. 026 919 87 51<br>
    <br>
    www.swissbakerytrophy.ch</p>
