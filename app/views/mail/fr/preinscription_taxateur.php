<p>Mesdames, Messieurs,</p>
<p>Nous avons le plaisir de vous annoncer que l'Association romande des artisans boulangers-pâtissiers-confiseurs organise une nouvelle édition du Swiss Bakery Trophy. Celle-ci se déroulera durant le Salon "Goûts & Terroirs", du 28 octobre au 1 novembre 2021, à Espace Gruyère, à Bulle.</p>
<p>Le Swiss Bakery Trophy est un événement important au sein de la boulangerie-confiserie artisanale suisse. Il ouvre une vitrine complète sur la qualité et la diversité de notre profession, et ce, à l'échelon helvétique. Les concours de qualité ne sont pas une innovation proprement dite, mais le Swiss Bakery Trophy a la particularité d'intégrer les avis des consommateurs et des professionnels. Ce panel ainsi réuni permet une évaluation plus proche des besoins et goûts de la clientèle.</p>
<p>Chaque produit est évalué par un groupe de cinq personnes : trois professionnels et deux consommateurs. Chaque groupe estimera plus d'une trentaine de produits par jour. Les taxations se dérouleront du 28 octobre au 1 novembre 2021, de 10h30 à 20h00, avec des pauses.</p>
<p>Vous avez déjà taxé des produits lors d'une ou des éditions précédentes ou êtes-vous intéressé à fonctionner comme taxateur cette année ? Aussi, nous permettons-nous de vous solliciter pour un mandat durant ce prochain Swiss Bakery Trophy 2021. A votre convenance, vous avez tout loisir d'assumer cette tâche durant une ou plusieurs des quatre journées (possibilité de demi-journée). Vous pouvez sans autre vous inscrire en ligne par le biais du lien suivant : <a href="https://www.swissbakerytrophy.ch">www.swissbakerytrophy.ch</a> que vous pouvez également communiquer aux personnes qui seraient intéressées.</p>
<p>Nous vous remercions de l'intérêt que vous portez au Swiss Bakery Trophy et, dans l'attente de votre prochaine inscription, nous vous présentons, Mesdames et Messieurs, nos meilleures salutations.</p>
<p><strong>Association romande des artisans boulangers-pâtissiers-confiseurs<br>
        Swiss Bakery Trophy</strong></p>
<p>Nicolas Taillens, chef technique<br>Catherine Oberson, secrétaire</p>

<p>Association Romande des Artisans<br>
    Boulangers-Pâtissiers-Confiseurs<br>
    Fédération Patronale et Economique<br>
    Rue Condémine 56 – CP 2175<br>
    1630 Bulle 2<br>
    Tél. 026 919 87 51<br>
    <br>
    www.swissbakerytrophy.ch</p>
