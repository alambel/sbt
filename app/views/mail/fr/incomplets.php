<p>Bonjour <?= $participant->nom ?> <?= $participant->prenom; ?>,<br>
    Certains produits de l'entreprise "<?= $participant->entreprise ?>" ne sont pas encore complets. Merci de bien vouloir compléter les produits suivants:<p>
<ul>
    <?php foreach($participant->getProduits(false) as $p):?>
    <li><?=$p->nom?></li>
<?php endforeach;?>
</ul>
<p><?= anchor('','Site web du Swiss Bakery Trophy') ?></p>

<p>Catherine Oberson<br>
    Secrétaire<br>
    <br>
    Association Romande des Artisans<br>
    Boulangers-Pâtissiers-Confiseurs<br>
    Fédération Patronale et Economique<br>
	Rue de la Condémine 56<br>
	Case Postale<br>
	1630 Bulle<br>
    Tél. 026 919 87 51<br>
    <br>
    www.swissbakerytrophy.ch</p>
