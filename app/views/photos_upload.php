<?php $this->load->view('base/header');

SBTInclude::jsFile("assets/plugins/dropzone/dropzone.min.js");
SBTInclude::css("assets/plugins/dropzone/dropzone.min.css");
SBTInclude::jsFile("assets/plugins/image-picker/image-picker.min.js");
SBTInclude::css("assets/plugins/image-picker/image-picker.css");

?>

<div class="container sm-padding-10 p-t-20 p-l-0 p-r-0">
	<div class="row">
		<div class="col-lg-12 col-sm-12">
			<div class=" card widget-loader-circle todolist-widget pending-projects-widget">
				<div class="card-header ">
					<div class="card-title"><h2>Envoi des photos sur le serveur</h2></div>
				</div>
				<div class="card-block">
					<div class="row m-b-20">
						<div class="col-md-12">
							<form action="<?= site_url('photos/post') ?>" class="dropzone needsclick dz-clickable" id="upload">
								<div class="dz-message needsclick">
									<h3>Glisser les image ici ou cliquez <u>ici</u> pour démarrer l'envoi</h3><br>
									<span class="note needsclick">Une fois envoyées, vous pourrez les attribuer à un produit</span>
								</div>
							</form>
						</div>
					</div>
					<div class="row m-b-20 pull-right">
						<a href="<?= site_url('photos/assign') ?>" class="btn btn-primary">Assigner les images</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container sm-padding-10 p-t-20 p-l-0 p-r-0">
	<div class="row">
		<div class="col-lg-12 col-sm-12">
			<div class=" card widget-loader-circle todolist-widget pending-projects-widget">
				<div class="card-header ">
					<div class="card-title"><h2>Télécharger les photos</h2></div>
				</div>
				<div class="card-block">
					<a href="<?= site_url('photos/dl'); ?>" class="btn">Télécharger toutes les photos</a>
				</div>
			</div>
		</div>
	</div>
</div>


<?php ob_start(); ?>
<script type="application/javascript">
	$(document).ready(function () {
		$("#image-picker").imagepicker()
		Dropzone.options.imageUpload = {
			acceptedFiles: ".jpeg,.jpg,.png,.gif,.zip"
		};
	});
</script>
<?php
SBTInclude::js(ob_get_clean());
$this->load->view('base/footer'); ?>



