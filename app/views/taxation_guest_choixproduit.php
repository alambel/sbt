<?php $this->load->view('base/header'); ?>
    <div class="container">
        <div class="card">
            <div class="card-header ">
                <div class="card-title">
                    <h1><?= t('demarrer_taxation_fictive')?></h1>
                </div>
            </div>
            <div class="card-block">
                    <p><?=t('entrer_num_produit')?></p>
                    <?php echo form_open();?>
                    <?= form_input(array('name' => 'pid', 'class' => 'form-control','placeholder'=>t('entrer_num_produit'))); ?>
                    <button class="btn btn-primary" type="submit"><?=t('selectionner')?></button>
                    <?= form_close(); ?>
            </div>
        </div>

    </div>

<?php $this->load->view('base/footer');