<?php $this->load->view('base/header');


?>
    <div class="container">
        <div class="card">
            <div class="card-header ">
                <div class="card-title">
                    <h1>Taxations en cours</h1>
                </div>
            </div>
            <div class="card-block">
                <div class="row">
                    <?php for ($table = 1;
                               $table <= 8;
                               $table++): ?>

                        <div class="col-sm-6">
                            <h2>Table <?= $table ?></h2>
                            <?php
                            $pid = SBTTable::current($table);
                            if ($pid):
                                $produit = SBTProduit::get($pid);

                                $producteur = SBTParticipant::get($produit->participant);
                                $taxation = SBTTaxation::get($pid);

                                $taxations = array();
                                if ($taxation)
                                    foreach ($taxation as $r) {
                                        $taxations[$r->taxateur] = $r;
                                    }
                                ?>
                                <h4>Produit NO <?= $produit->no ?>: <?= $produit->nom ?></h4>
                                <table class="col-sm-12 table-bordered table-striped">
                                    <tr>
                                        <th>Critère</th>
                                        <?php for ($i = 1; $i <= 5; $i++): ?>
                                            <th><span style="color:<?= isset($taxations[$i]) ? 'green' : 'orange' ?>"><?= $i ?></span></th>
                                        <?php endfor ?>
                                    </tr>
                                    <?php foreach (array('aspect', 'forme', 'surface', 'texture', 'bouche', 'odeur') as $critere): ?>
                                        <tr>
                                            <td><?= t($critere) ?></td>
                                            <?php for ($i = 1; $i <= 5; $i++): ?>
                                                <td><?php if (isset($taxations[$i])): ?>
                                                        <span style="color:green;"><?= $taxations[$i]->{$critere} ?><br><?= $taxations[$i]->{$critere . '_commentaire'} ?></span>
                                                    <?php else: ?>
                                                        <span style="color:orange;">...</span>
                                                    <?php endif; ?></td>
                                            <?php endfor ?>
                                        </tr>
                                    <?php endforeach; ?>
                                </table>
                                <button class="btn btn-danger" onclick="cancelTable(<?= $table ?>);">Annuler la table et les taxation</button>
                            <?php else: ?>
                                <p>Aucune taxation en cours</p>
                            <?php endif; ?>
                        </div>
                    <?php endfor; ?>
                </div>
            </div>
        </div>
    </div>
<?php
ob_start() ?>
    <script type="application/javascript">
        $(document).ready(function () {
            setTimeout(function () {
                window.location.reload(1);
            }, 15000);
        });

        function cancelTable(table) {
            if (confirm('Ceci va annuler la taxation de la table ' + table + ' et supprimer toutes les notes et commentaires déja effectués')) {
                window.location = '<?=site_url('admin/canceltable')?>/' + table;
            }

        }

    </script>
<?php
SBTInclude::js(ob_get_clean());

$this->load->view('base/footer'); ?>
