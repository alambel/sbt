<?php $this->load->view('base/header');
$participant = SBTParticipant::getCurrent();
?>
    <div class="container">
        <div class="card">
            <div class="card-header ">
                <p class="pull-right"><?= t('taxation_fictive') ?></p>
                <div class="card-title">
                    <?php if ($produit): ?>
                        <h1><?= $produit->nom ?> (NO: <?= $produit->no ?>)</h1>
                    <?php else: ?>
                        <h1><?= t('produit_example') ?></h1>
                    <?php endif; ?>
                </div>
            </div>
            <div class="card-block">
                <?php if ($produit): ?>
                    <p><b><?= t('categorie') ?></b>: <?= SBTProduit::$categories[$produit->categorie]; ?></p>
                <?php endif; ?>
                <?php echo form_open();
                echo form_hidden('done', 1); ?>
                <?php if ($produit && $produit->categorie == 'tartiner'): ?>
                    <?php scale('aspect', t('couleur')); ?>
                    <hr>
                    <?php scale('odeur', t('gout')); ?>
                    <hr>
                    <?php scale('texture'); ?>
                    <hr>
                    <?php scale('bouche'); ?>
                <?php else: ?>
                    <h3><?= t('caracteristiques_exterieures') ?></h3>
                    <?php scale('aspect'); ?>
                    <hr>
                    <h3><?= t('caracteristiques_interieures') ?></h3>
                    <?php scale('bouche'); ?>
                    <hr>
                    <?php scale('odeur'); ?>
                <?php endif; ?>
                <button class="btn btn-primary pull-right" type="submit"><?= t('sauver') ?></button>
                <?= form_close() ?>
            </div>
        </div>

    </div>

<?php


ob_start() ?>
    <script>

    </script>
<?php
SBTInclude::js(ob_get_clean());

$this->load->view('base/footer'); ?>