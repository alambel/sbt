<?php $this->load->view('base/header'); ?>
<div class="container container-fixed-lg bg-white">
	<!-- START card -->
	<div class="card card-transparent">
		<div class="card-header ">
			<div class="card-title"><h1><?= anchor('entreprise/view/' . $participant->id, t('produits_de') . ' ' . $participant->entreprise) ?></h1></div>
		</div>
		<div class="card-block">
			<p><?= t('infos_prix_champion') ?></p>

			<!-- END card -->
			<div class="table-responsive">
				<?= form_open() ?>
				<table class="table table-hover no-footer padding-0" role="grid">
					<thead>
					<tr role="row">
						<th></th>
						<th style="color:green"><?= t('nom_produit') ?> *</th>
						<th style="color:green"><?= t('categorie') ?> *</th>
						<th style="width: 30%"><?= t('composition_title') ?></th>
						<th style="width: 30%"><?= t('arguments_de_vente') ?></th>
						<th><?= t('contribution') ?></th>
					</tr>
					</thead>
					<tbody>
					<?php
					$produits = $participant->getProduits(null, true);

					for ($i = 0; $i < 10; $i++):
						$p = isset($produits[$i]) ? $produits[$i] : new SBTProduit();

						?>

						<tr role="row bg-success" class="<?= in_array($i, [4, 9]) ? 'border-bottom-dotted' : '' ?>">
							<td><?=$i+1?></td>
							<td class="padding-5"><?php
								echo form_hidden('p[' . $i . '][id]', $p->id);
								echo form_input('p[' . $i . '][nom]', $p->nom, array('placeholder' => t('nom_produit'), 'class' => 'form-control ' . ($p->id && !$p->nom ? 'bg-warning' : null))); ?>
							</td>
							<td class="padding-5"><?= form_dropdown('p[' . $i . '][categorie]', SBTProduit::$categories, $p->categorie, array('placeholder' => t('categorie'), 'class' => 'form-control ' . ($p->id && !$p->categorie ? 'bg-warning' : null))); ?></td>
							<td class="padding-5"><textarea class="form-control <?= $p->id && strlen($p->composition) < 10 ? 'bg-warning' : null ?>" id="composition"
															name="p[<?= $i ?>][composition]"
															placeholder="<?= t('composition') ?>"
															style="height:120px"><?= $p->composition ?></textarea></td>
							<td class="padding-5"><textarea class="form-control <?= $p->id && strlen($p->arguments) < 10 ? 'bg-warning' : null ?>" id="arguments" name="p[<?= $i ?>][arguments]"
															placeholder="<?= t('arguments_de_vente') ?>"
															style="height:120px"><?= $p->arguments ?></textarea></td>
							</td>
							<td><?php if ($i == 0) {
									echo 'Fr. 200 .-';
								} else if ($i < 3) {
									echo '+ Fr. 150.-';
								} else if ($i == 10) {
									echo t('Gratuit');
								} else {
									echo '+ Fr. 50.-';
								} ?><br><?php if ($p->id):
									if (!$p->isComplete()):?>
										<?php if ($p->nom && $p->categorie): ?>
											<span class="text-info"><?= t('completer_veille_concours') ?></span>
										<?php else: ?>
											<span class="text-danger"><?= t('produit_incomplet') ?></span>
										<?php endif; ?>
									<?php endif; ?>
									<br><br>
									<button type="button" class="btn btn-default btn-xs text-danger" onclick="delProduit(<?= $p->id; ?>);"><?= t('supprimer') ?></button>
								<?php endif; ?>
							</td>

						</tr>


					<?php endfor; ?>
					<?php $jubile = $participant->getJubile(); ?>

					<tr>
						<td colspan="5" class="color-sbt"><?= t('prix_du_jubilé') ?></td>
					</tr>

					<tr role="row" class="<?= in_array($i, [4, 9]) ? 'border-bottom-dotted' : '' ?>">
						<td></td>
						<td class="padding-5"><?php
							echo form_hidden('p[10][id]', $jubile?->id);
							echo form_input('p[10][nom]', $jubile?->nom, array('placeholder' => t('nom_produit'), 'class' => 'form-control ' . ($jubile?->id && !$jubile->nom ? 'bg-warning' : null))); ?>
						</td>
						<td class="padding-5"><?= form_dropdown('p[10][categorie]', SBTProduit::$categories, $jubile?->categorie, array('placeholder' => t('categorie'), 'class' => 'form-control ' . ($jubile?->id && !$jubile->categorie ? 'bg-warning' : null))); ?></td>
						<td class="padding-5"><textarea class="form-control <?= $jubile?->id && strlen($jubile->composition) < 10 ? 'bg-warning' : null ?>" id="composition"
														name="p[10][composition]"
														placeholder="<?= t('composition') ?>"
														style="height:120px"><?= $jubile?->composition ?></textarea></td>
						<td class="padding-5"><textarea class="form-control <?= $jubile?->id && strlen($jubile->arguments) < 10 ? 'bg-warning' : null ?>" id="arguments" name="p[10][arguments]"
														placeholder="<?= t('arguments_de_vente') ?>"
														style="height:120px"><?= $jubile?->arguments ?></textarea></td>
						</td>
						<td><?= t('Gratuit'); ?><br><?php if ($jubile?->id):
								if ($jubile && !$jubile->isComplete()):?>
									<?php if ($jubile->nom && $jubile->categorie): ?>
										<span class="text-info"><?= t('completer_veille_concours') ?></span>
									<?php else: ?>
										<span class="text-danger"><?= t('produit_incomplet') ?></span>
									<?php endif; ?>
								<?php endif; ?>
								<br><br>
								<button type="button" class="btn btn-default btn-xs text-danger" onclick="delProduit(<?= $jubile->id; ?>);"><?= t('supprimer') ?></button>
							<?php endif; ?>
						</td>

					</tr>

					</tbody>
				</table>
				<br>
				<a href="<?= site_url('entreprise/view/' . $participant->id) ?>" class="btn btn-white"><?= t('retour') ?></a>
				<button class="btn btn-primary pull-right" type="submit"><?= t('sauver') ?></button>
				<?= form_close() ?>
			</div>
		</div>
	</div>
</div>
<?php


ob_start() ?>
<script>
	function delProduit(pid) {
		if (confirm('<?=t('voulez_vous_supprimer_produit')?>')) {
			window.location = '<?= site_url('produits/delete/');?>/' + pid;
		}
	}
</script>
<?php
SBTInclude::js(ob_get_clean());

$this->load->view('base/footer'); ?>
