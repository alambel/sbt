<?php $this->load->view('base/header');
/**
 * @var $produits SBTProduit[]
 */
?>


<div class="container sm-padding-10 p-t-20 p-l-0 p-r-0">
	<div class="row"><h1><?= $participant->entreprise; ?></h1></div>
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header ">
					<div class="card-title"><h2><?= t('infos_entreprise') ?></h2></div>
				</div>
				<div class="card-block">
					<div class="form-group row">
						<label class="col-md-3 control-label"><?= t('entreprise') ?></label>
						<div class="col-md-9"><?= $participant->entreprise; ?></div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 control-label"><?= t('nom') ?> <?= t('prenom') ?></label>
						<div class="col-md-9"><?= $participant->nom . ' ' . $participant->prenom; ?></div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 control-label"><?= t('rue') ?></label>
						<div class="col-md-9"><?= $participant->rue; ?></div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 control-label"><?= t('npa') ?> <?= t('ville') ?></label>
						<div class="col-md-9"><?= $participant->npa . ' ' . $participant->lieu; ?></div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 control-label"><?= t('canton') ?></label>
						<div class="col-md-9"><?= SBTParticipant::$cantons[$participant->canton]; ?></div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 control-label"><?= t('email') ?></label>
						<div class="col-md-9"><?= $participant->email; ?></div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 control-label"><?= t('telephone') ?></label>
						<div class="col-md-9"><?= $participant->telephone; ?></div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 control-label"><?= t('mobile') ?></label>
						<div class="col-md-9"><?= $participant->mobile; ?></div>
					</div>
					<div class="form-group row">
						<label class="col-md-3 control-label"><?= t('langue') ?></label>
						<div class="col-md-9"><?= $participant->lang=='F' ? 'Français' : 'Deutsch'; ?></div>
					</div>
				</div>
				<?php if (SBTParticipant::isAdmin()) : ?>
					<div class="card-footer">
						<div class="pull-left"><a href="<?= site_url('entreprise/modifier/' . $participant->id); ?>"><?= t('modifier') ?></a></div>
					</div>
				<?php endif; ?>
			</div>
		</div>

		<?php if ($participant->actif == 1): ?>
			<div class="col-md-12">
				<div class="card">
					<div class="card-header ">
						<div class="card-title"><h2><?= t('mes_produits') ?></h2></div>
					</div>
					<div class="card-block">

						<?php $produits = $participant->getProduits(); ?>
						<?= sprintf(t('x_produits_inscrits'), count($produits)) ?><br><br>
						<?php if ($produits): ?>
							<table class="sm-table table-hover " id="produits">
								<thead>
								<tr role="row m-b-20">
									<?php if (SBTParticipant::isAdmin()): ?>
										<th>NO</th>
									<?php endif; ?>
									<th><?= t('nom_produit') ?></th>
									<th><?= t('categorie') ?></th>
									<th><?= t('composition_title') ?></th>
									<th><?= t('arguments_de_vente') ?></th>
									<th></th>
								</tr>
								</thead>
								<tbody>
								<?php
								$i = 1;
								foreach ($produits as $p):
									; ?>
									<tr class="<?= !$p->isComplete() ? 'text-danger' : 'text-success' ?>">
										<?php if (SBTParticipant::isAdmin()): ?>
											<td><?= $p->no ?></td>
										<?php endif; ?>
										<td><?= $p->nom ?><?= $p->jubile ? ' (' . t('jubile') . ')' : '' ?> </td>
										<td><?= SBTProduit::$categories[$p->categorie]; ?></td>
										<td><?= $p->composition; ?></td>
										<td class="v-align-middle"><?= $p->arguments; ?></td>
										<td>
											<?php
											$nb = $p->nbPhotos();
											if ($nb && $this->config->item('download_photos')): ?>
												<a href="<?= site_url('produits/photos/' . $p->id); ?>" class="btn"><?= sprintf(t('telecharger_les_x_photos'), $nb) ?></a>
											<?php endif; ?>
										</td>
									</tr>

								<?php
								endforeach; ?>
								</tbody>
							</table>
						<?php else: ?>
							<a class="btn btn-primary" href="<?= site_url('produits/index/' . $participant->id); ?>"><?= t('ajouter_produit') ?></a>
						<?php endif; ?>

						<?php if ($participant->billed): ?>
							<div class="row m-t-20">
								<a href="<?= base_url($participant->getFacturePath()); ?>" class="btn btn-default btn-cons m-b-10" target="_blank"><i class="fa fa-download"></i>
									Télécharger la facture</a>
							</div>
						<?php endif; ?>
					</div>
					<?php if (MODIFICATIONS_PRODUIT || SBTParticipant::isAdmin()): // clôture des inscriptions ?>
						<div class="card-footer">
							<div class="pull-left"><a href="<?= site_url('produits/index/' . $participant->id); ?>"><?= t(count($produits) > 0 ? 'modifier' : 'ajouter_produit') ?></a></div>
						</div>
					<?php endif ?>
				</div>
			</div>
		<?php endif; ?>
		<?php if (SBTParticipant::isAdmin()): ?>
		<div class="col-md-12">
			<div class="card">
				<div class="card-header ">
					<div class="card-title"><h4>Actions</h4></div>
				</div>
				<div class="card-block">
					<?php if (!$participant->actif): ?>
						<a href="<?= site_url('entreprise/view/' . $participant->id . '/activate'); ?>" class="btn btn-default btn-cons m-b-10"><i class="fa fa-check"></i> Activer</a>
						<a href="<?= site_url('entreprise/view/' . $participant->id . '/sendpreinscription'); ?>" class="btn btn-default btn-cons m-b-10"><i class="fa fa-envelope"></i> Envoyer
							email pré-inscription</a>
					<?php else: ?>
						<?php if ($participant->billed): ?>
							<a href="<?= site_url('entreprise/view/' . $participant->id . '/sendfacture'); ?>" class="btn btn-default btn-cons m-b-10"><i class="fa fa-envelope"></i>
								Ré-envoyer la facture</a>
							<a href="<?= base_url($participant->getFacturePath()); ?>" class="btn btn-default btn-cons m-b-10" target="_blank"><i class="fa fa-download"></i>
								Télécharger la facture</a>
						<?php else: ?>
							<a href="<?= site_url('entreprise/view/' . $participant->id . '/sendfacture'); ?>" class="btn btn-default btn-cons m-b-10">
								<i class="fa fa-envelope"></i> Envoyer la facture par email</a>
						<?php endif; ?>


						<div class="row">
							Montant facturé: <?= $participant->totalfacture ?> - Montant actuel: <?= $participant->getTotal() ?>
						</div>
						<div class="row">
							<?= form_open('', array('class' => 'form-inline')); ?>
							<?= form_hidden('paid', 1); ?>
							<div class="col-md-4">
								Payé <input name="amount" type='float' class="form-control inline   " value="<?= $participant->totalpaiement ?: null ?>"/>
							</div>
							<div class="col-md-4">
								le
								<div class="form-group">
									<div class='input-group date' id='datetimepicker'>
										<input name="paiddate" type='text' class="form-control" value="<?= $participant->paid ? date('d.m.Y', $participant->paid) : null ?>"/>
										<span class="input-group-addon">
                                    <span class="fa fa-calendar"></span>
                                </span>
									</div>
								</div>
							</div>

							<div class="col-md-4    ">
								<input type="submit" class="btn" value="Sauver"/>
							</div>
							<?= form_close() ?>
						</div>

						<?php if (!MODIFICATIONS_PRODUIT): ?>
							<a href="<?= site_url('entreprise/view/' . $participant->id . '/sendincomplets'); ?>" class="btn btn-default btn-cons m-b-10"><i class="fa fa-envelope"></i> Rappel
								produits incomplets</a>
						<?php endif; ?>
					<?php endif; ?>
				</div>
				<div class="card-header ">
					<div class="card-title"><h4>Historique</h4></div>
				</div>
				<div class="card-block">
					<?php foreach ($participant->getHistory() as $title => $text): ?>
						<div class="form-group row">
							<label class="col-md-3 control-label"><?= $title ?></label>
							<div class="col-md-9"><?= $text; ?></div>
						</div>
					<?php endforeach; ?>
				</div>

			</div>
			<?php endif; ?>
		</div>
	</div>
</div>


<?php ob_start(); ?>
<script type="application/javascript">
	$(document).ready(function () {
		$('#produits').DataTable({searching: false, paging: false, info: false});
		$('#datetimepicker').datepicker({
			format: 'dd.mm.yyyy',
		});
	});
</script>
<?php
SBTInclude::js(ob_get_clean());
SBTInclude::jsFile('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
SBTInclude::css('assets/plugins/bootstrap-datepicker/css/datepicker3.css');
$this->load->view('base/footer'); ?>
