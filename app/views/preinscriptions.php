<?php $this->load->view('base/header'); ?>


    <div class="container sm-padding-10 p-t-20 p-l-0 p-r-0">

        <div class="row">
            <div class="col-lg-12 col-sm-12">
                <div class=" card widget-loader-circle todolist-widget pending-projects-widget">
                    <div class="card-header ">
                        <div class="card-title"><h2>Pre-Inscriptions</h2></div>
                    </div>
                    <div class="card-block">
                        <div class="row m-b-10">
                            <button id="sendEmail" class="btn btn-success btn-cons " type="button"><i class="fa fa-envelope"></i> <span id="sendMailTitle" class="bold">Envoyer 0 email</span></button>
                        </div>
                        <?= form_open(); ?>
                        <?= form_hidden('action', ''); ?>
                        <table id="participants" class="table table-hover" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th><?= form_checkbox('', null, false, 'id="checkall"'); ?></th>
                                <th>Entreprise</th>
                                <th>Contact</th>
                                <th>Adresse</th>
                                <th>Canton</th>
                                <th>Email</th>
                                <th>Lien d'activation</th>
                                <th>Email envoyé</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($participants as $p):
                                $nbComplets = $p->getNbProduitsComplets();
                                $nbProduits = $p->getNbProduits();
                                ?>
                                <tr>
                                    <td><?= form_checkbox('ids[]', $p->id, false, 'class="rowcheckbox"'); ?></td>
                                    <td><?= anchor('entreprise/view/' . $p->id, $p->entreprise); ?></td>
                                    <td><?= $p->nom . ' ' . $p->prenom; ?><br><?= $p->telephone ? '<i class="fa fa-phone"></i> ' . $p->telephone : null; ?>
                                        <br><?= $p->mobile ? '<i class="fa fa-mobile"></i> ' . $p->mobile : null ?></td>
                                    <td><?= $p->rue . '<br>' . $p->npa . ' ' . $p->lieu ?></td>
                                    <td><?= $p->canton; ?></td>
                                    <td><?= mailto($p->email, $p->email); ?></td>
                                    <td><?= anchor($p->getPreinscriptionUrl()) ?></td>
                                    <td><?= $p->mailpreinscription ? 'oui' : 'non' ?></td>
                                    <td>
                                        <a onclick="delPreinscription(<?= $p->id ?>);" class="btn">Supprimer</>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php ob_start(); ?>
    <script type="application/javascript">
        var table;
        $(document).ready(function () {
            table = $('#participants').DataTable({

                // responsive: true,
                stateSave: true,
                dom: '<"top"Blif>rt<"bottom"p><"clear">',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        text: 'Exporter Excel',
                    }
                ],
                select: "multi",
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
            });
            $('.rowcheckbox').change(function () {
                console.log(table.rows('.selected').data().length);
                $('#sendMailTitle').html('Envoyer ' + $('input[name="ids[]"]:checked').length + ' emails');
            });
            $('#checkall').change(function () {
                console.log(this.checked);
                $('input[name="ids[]"]').prop('checked', this.checked);
                $('#sendMailTitle').html('Envoyer ' + $('input[name="ids[]"]:checked').length + ' emails');
            });
            $('#sendEmail').click(function () {
                $('input[name="action"]').val('sendmail');
                $('form').submit();
            });
        });

        function delPreinscription(participantId) {
            if (confirm('Voulez-vous vraiment supprimer cette entreprise?')) {
                window.location = '<?=site_url('admin/delete_participant')?>/' + participantId;
            }
        }
    </script>
<?php
SBTInclude::js(ob_get_clean());

$this->load->view('base/footer'); ?>