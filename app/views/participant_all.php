<?php $this->load->view('base/header'); ?>


    <div class="container sm-padding-10 p-t-20 p-l-0 p-r-0">

        <div class="row">
            <div class="col-lg-12 col-sm-12">
                <div class=" card widget-loader-circle todolist-widget pending-projects-widget">
                    <div class="card-header ">
                        <div class="card-title"><h2>Participants</h2></div>
                    </div>
                    <div class="card-block">
                        <h3>Participants par canton</h3>
                        <table class="table-bordered table-sm" style="width:100%">
                            <tr>
                                <th>Canton</th>
                                <?php foreach (SBTParticipant::$cantons as $canton => $count): ?>
                                    <?php if ($canton): ?>
                                        <td><?= $canton; ?></td>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </tr>
                            <tr>
                                <th>Inscrits</th>
                                <?php $parCanton = SBTParticipant::getParCanton();
                                foreach (SBTParticipant::$cantons as $canton => $count): ?>
                                    <?php if ($canton): ?>
                                        <td><?= isset($parCanton[$canton]) ? $parCanton[$canton] : null; ?></td>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </tr>
                        </table>
                        <h3>Inscriptions</h3>
                                                <a href="<?= site_url('admin/sendAllFactures') ?>" class="btn btn-white pull-right">Envoyer les factures (max 10)</a><br><br>
                        <table id="participants" class="table table-hover table-responsive" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>id</th>
                                <th>Entreprise</th>
                                <th>Nom</th>
                                <th>Prenom</th>
                                <th>Telephone</th>
                                <th>Mobile</th>
                                <th>Rue</th>
                                <th>NPA</th>
                                <th>Lieu</th>
                                <th>Canton</th>
                                <th>Email</th>
                                <th>Langue</th>
                                <th>Produits complets</th>
                                <th>Produits total</th>
                                <th>Montant actuel</th>
                                <th>Montant facturé</th>
                                <th>Date inscription</th>
                                <th>Facturé</th>
                                <th>Payé</th>
                                <th>Montant payé</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if ($participants):
                                foreach ($participants as $p):
                                    $nbComplets = $p->getNbProduitsComplets();
                                    $nbProduits = $p->getNbProduits();
                                    $total = $p->getTotal();
                                    ?>
                                    <tr>
                                        <td><?= $p->id; ?></td>
                                        <td><?= anchor('entreprise/view/' . $p->id, $p->entreprise); ?></td>
                                        <td><?= $p->nom ?></td>
                                        <td><?= $p->prenom; ?></td>
                                        <td><?= $p->telephone ?></td>
                                        <td><?= $p->mobile ?></td>
                                        <td><?= $p->rue ?></td>
                                        <td><?= $p->npa ?></td>
                                        <td><?= $p->lieu ?></td>
                                        <td><?= $p->canton; ?></td>
                                        <td><?= mailto($p->email, $p->email); ?></td>
										<td><?= $p->lang; ?></td>
                                        <td><span class="<?= $nbComplets < $nbProduits ? 'text-danger' : 'text-success' ?>"><?= $nbComplets ?></span></td>
                                        <td><?= $nbProduits ?></td>
                                        <td><?= $total ?></td>
                                        <td><?= $p->totalfacture ?></td>
                                        <td><?= $p->dateinscription ?></td>
                                        <td><?= $p->billed ? '<i class="fa fa-check text-success"></i> ' . date('Y-m-d',$p->billed) : '<i class="fa fa-times text-danger"></i>' ?></td>
                                        <td><?= $p->paid ? '<i class="fa fa-check text-success"></i> ' . date('Y-m-d', $p->paid) : '<i class="fa fa-times text-danger"></i>' ?></td>
                                        <td><?= $p->totalpaiement?></td>
                                        <td>
                                            <button onclick="delParticipant(<?= $p->id ?>);">Supprimer</button>
                                        </td>
                                    </tr>
                                <?php endforeach;
                            endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>

<?php ob_start(); ?>
    <script type="application/javascript">
        $(document).ready(function () {
            $('#participants').dataTable({
                // responsive: true,
				scrollX: true,
                stateSave: true,
                dom: '<"top"Blif>rt<"bottom"p><"clear">',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        text: 'Exporter Excel',
                    }
                ]
            });

        });

        function delParticipant(participantId) {
            if (confirm('Voulez-vous vraiment supprimer cette entreprise?')) {
                window.location = '<?=site_url('admin/delete_participant')?>/' + participantId;
            }
        }
    </script>
<?php
SBTInclude::js(ob_get_clean());

$this->load->view('base/footer');
