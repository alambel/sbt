<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
	<meta charset="utf-8"/>
	<title>Swiss Bakery Trophy</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no"/>
	<link rel="apple-touch-icon" href="<?= base_url('pages/ico/60.png?v3') ?>">
	<link rel="apple-touch-icon" sizes="76x76" href="<?= base_url('pages/ico/76.png?v3') ?>">
	<link rel="apple-touch-icon" sizes="120x120" href="<?= base_url('pages/ico/120.png?v3') ?>">
	<link rel="apple-touch-icon" sizes="152x152" href="<?= base_url('pages/ico/152.png?v3') ?>">
	<link rel="icon" type="image/x-icon" href="<?= base_url('favicon.ico') ?>"/>
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-touch-fullscreen" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="default">
	<link href="<?= base_url('assets/plugins/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet" type="text/css"/>
	<!--	<link href="--><?php //= base_url('assets/plugins/font-awesome/css/font-awesome.css') ?><!--" rel="stylesheet" type="text/css"/>-->
	<!--	<link href="--><?php //= base_url('assets/plugins/select2/css/select2.min.css') ?><!--" rel="stylesheet" type="text/css" media="screen"/>-->
	<!--	<link href="--><?php //= base_url('assets/plugins/switchery/css/switchery.min.css') ?><!--" rel="stylesheet" type="text/css" media="screen"/>-->
	<link href="<?= base_url('pages/css/pages-icons.css') ?>" rel="stylesheet" type="text/css">
	<link class="main-stylesheet" href="<?= base_url('pages/css/sbt.css?v2') ?>" rel="stylesheet" type="text/css"/>
	<?php

	SBTInclude::jsFile('assets/plugins/jquery/jquery-1.11.1.min.js');


	SBTInclude::printJs();
	?>
	<!--	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>-->
	<!--	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>-->
	<!--	<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.18/b-1.5.4/b-colvis-1.5.4/b-html5-1.5.4/b-print-1.5.4/r-2.2.2/datatables.min.js"></script>-->

</head>
<body class="fixed-header horizontal-menu horizontal-app-menu ">
<!-- START PAGE-CONTAINER -->
<div class="header p-r-0 p-l-0 bg-sbt">
	<div class="container header-inner header-md-height bg-sbt">
		<div class="">
			<div class="brand inline no-border hidden-xs-down">

				<img src="<?= base_url('img/logosbt2024.jpg'); ?>" alt="logo" width="56" height="40"> <span class="semi-bold text-white">Swiss Bakery Trophy 2024</span>

			</div>
		</div>
		<div class="d-flex align-items-center">
			<?php if (SBTParticipant::getCurrentUserId()):
				$participant = SBTParticipant::getCurrent(); ?>
				<div class="pull-left p-r-10 fs-14 font-heading hidden-md-down text-white">
					<a href="<?= site_url('entreprise') ?>"><span class="semi-bold text-white"><?= $participant->entreprise ?> (<?= $participant->prenom . ' ' . $participant->nom; ?>)</span></a>
				</div>
				<div class="dropdown pull-right">
					<a href="<?= site_url('logout'); ?>" style="color:white"><i class="pg-power"></i></a>
				</div>
			<?php endif; ?>
			<!-- END User Info-->
			<ul class="hidden-md-down notification-list no-margin hidden-sm-down b-grey b-r no-style p-l-30 p-r-20">

				<?php if (lang() == 'de'): ?>
					<li class="p-r-10 inline"><a href="javascript:void(0)" onclick="changeLang('fr');"> <img height="15" width="15" src="<?php echo base_url('img/flag/fr.png'); ?>"/></a></li>
				<?php else: ?>
					<li class="p-r-10 inline"><a href="javascript:void(0)" onclick="changeLang('de');"> <img height="15" width="15" src="<?php echo base_url('img/flag/de.png'); ?>"/></a></li>
				<?php endif; ?>
				<script>
					function changeLang(lang) {
						$.get('<?=site_url('func/lang')?>/' + lang, function (data) {
							window.location.reload();
						});
					}
				</script>
			</ul>
		</div>
	</div>
</div>
<div class="page-container ">
	<div class="page-content-wrapper ">
		<div class="content">
			<div class="content sm-gutter">
				<div class="container">
					<?php if (SBTNotification::hasMessages()): ?>
						<div class="wrapper wrapper-content">
							<?php
							$successes = SBTNotification::getSuccesses();
							if (count($successes) > 0):
								foreach ($successes as $m):
									?>
									<div class="alert alert-success"><?php echo $m; ?></div>
								<?php
								endforeach;
							endif;
							?>
							<?php
							$warnings = SBTNotification::getWarnings();
							if (count($warnings) > 0):
								foreach ($warnings as $m):
									?>
									<div class="alert alert-warning"><?php echo $m; ?></div>
								<?php
								endforeach;
							endif;
							?>
							<?php
							$errors = SBTNotification::getErrors();
							if (count($errors) > 0):
								foreach ($errors as $m):
									?>
									<div class="alert alert-danger"><?php echo $m; ?></div>
								<?php
								endforeach;
							endif;
							?>
						</div>
					<?php endif; ?>
				</div>

