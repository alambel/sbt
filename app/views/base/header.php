<!DOCTYPE html>
<html>
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-121731481-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}

		gtag('js', new Date());

		gtag('config', 'UA-121731481-1');
	</script>

	<meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
	<meta charset="utf-8"/>
	<title>Swiss Bakery Trophy</title>
	<meta name="viewport"
		  content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no"/>
	<link rel="apple-touch-icon" href="<?= base_url('pages/ico/60.png?v=3') ?>">
	<link rel="apple-touch-icon" sizes="76x76" href="<?= base_url('pages/ico/76.png?v=3') ?>">
	<link rel="apple-touch-icon" sizes="120x120" href="<?= base_url('pages/ico/120.png?v=3') ?>">
	<link rel="apple-touch-icon" sizes="152x152" href="<?= base_url('pages/ico/152.png?v=3') ?>">
	<link rel="icon" type="image/x-icon" href="<?= base_url('favicon.ico') ?>"/>
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-touch-fullscreen" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="default">
	<meta content="" name="description"/>
	<meta content="" name="author"/>
	<link href="<?= base_url('assets/plugins/pace/pace-theme-flash.css') ?>" rel="stylesheet" type="text/css"/>
	<link href="<?= base_url('assets/plugins/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet" type="text/css"/>
	<link href="<?= base_url('assets/plugins/font-awesome/css/font-awesome.css') ?>" rel="stylesheet" type="text/css"/>
	<link href="<?= base_url('assets/plugins/jquery-scrollbar/jquery.scrollbar.css') ?>" rel="stylesheet" type="text/css" media="screen"/>
	<link href="<?= base_url('assets/plugins/select2/css/select2.min.css') ?>" rel="stylesheet" type="text/css" media="screen"/>
	<link href="<?= base_url('assets/plugins/switchery/css/switchery.min.css') ?>" rel="stylesheet" type="text/css" media="screen"/>
	<link href="<?= base_url('pages/css/pages-icons.css') ?>" rel="stylesheet" type="text/css">
	<link class="main-stylesheet" href="<?= base_url('pages/css/sbt.css?v3') ?>" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.18/b-1.5.4/b-colvis-1.5.4/b-html5-1.5.4/b-print-1.5.4/r-2.2.2/datatables.min.css"/>

	<?php

	SBTInclude::jsFile('assets/plugins/jquery/jquery-1.11.1.min.js');


	SBTInclude::printJs();
	?>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.18/b-1.5.4/b-colvis-1.5.4/b-html5-1.5.4/b-print-1.5.4/r-2.2.2/datatables.min.js"></script>

</head>
<body class="fixed-header horizontal-menu horizontal-app-menu ">
<!-- START PAGE-CONTAINER -->
<div class="header p-r-0 p-l-0 bg-sbt">
	<div class="container header-inner header-md-height bg-sbt">
		<div class="">
			<div class="brand inline no-border hidden-xs-down">
				<a href="<?= SBTParticipant::isTaxateur() ? '#' : site_url(); ?>">
					<img src="<?= base_url('img/logosbt2024.jpg'); ?>" alt="logo" width="56" height="40"> <span class="semi-bold text-white">Swiss Bakery Trophy 2024</span>
				</a>
			</div>
		</div>
		<div class="d-flex align-items-center">
			<?php if (SBTParticipant::getCurrentUserId()):
				$participant = SBTParticipant::getCurrent(); ?>
				<div class="pull-left p-r-10 fs-14 font-heading hidden-md-down text-white">
					<a href="<?= site_url('entreprise') ?>"><span class="semi-bold text-white"><?= $participant->entreprise ?> (<?= $participant->prenom . ' ' . $participant->nom; ?>)</span></a>
				</div>
				<div class="dropdown pull-right">
					<!--					<button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">-->
					<a href="<?= site_url('logout'); ?>" style="color:white"><i class="pg-power"></i></a>
					<!--					</button>-->
				</div>
			<?php else: ?>
				<ul class="hidden-md-down notification-list no-margin hidden-sm-down b-grey b-r no-style p-l-30 p-r-20">
					<li class="p-r-10 inline">
						<a href="<?= site_url('inscription') ?>" class="header-icon"> <?= t('inscription_entreprise'); ?></a>
					</li>
					<li class="p-r-10 inline">
						<a href="<?= site_url('inscription_taxateur') ?>" class="header-icon"> <?= t('inscription_taxateur'); ?></a>
					</li>
					<li class="p-r-10 inline">
						<a href="<?= site_url('login') ?>" class="header-icon"> <?= t('login'); ?></a>
					</li>
				</ul>
			<?php endif; ?>
			<!-- END User Info-->
			<ul class="hidden-md-down notification-list no-margin hidden-sm-down b-grey b-r no-style p-l-30 p-r-20">

				<?php if (lang() == 'de'): ?>
					<li class="p-r-10 inline"><a href="javascript:void(0)" onclick="changeLang('fr');"> <img height="15" width="15" src="<?php echo base_url('img/flag/fr.png'); ?>"/></a></li>
				<?php else: ?>
					<li class="p-r-10 inline"><a href="javascript:void(0)" onclick="changeLang('de');"> <img height="15" width="15" src="<?php echo base_url('img/flag/de.png'); ?>"/></a></li>
				<?php endif; ?>
				<script>
					function changeLang(lang) {
						$.get('<?=site_url('func/lang')?>/' + lang, function (data) {
							window.location.reload();
						});
					}
				</script>
			</ul>
		</div>
	</div>
	<?php if (SBTParticipant::isLoggedIn() && !SBTParticipant::isTaxateur()): ?>
		<div class="bg-sbt2">
			<div class="container">
				<div class="menu-bar header-sm-height  color-sbt2" data-pages-init='horizontal-menu'>
					<ul>
						<?php if (SBTParticipant::isAdmin()): ?>

							<?php if (INSCRIPTIONS_OUVERTES):?>
							<li><a href="<?= site_url('inscription') ?>"><span class="title color-sbt2">Nouvelle Inscription</span></a></li>
							<?php endif;?>
								<?php /*
							<li>
								<a href="javascript:;"><span class="title">Inscriptions</span>
									<span class=" arrow"></span></a>
								<ul class="">
									<li><a href="<?= site_url('inscription') ?>"><span class="title">Nouvelle Inscription</span></a></li>
									<li><a href="<?= site_url('admin/preinscriptions') ?>"><span class="title">Pré-inscriptions Participants</span></a></li>
									<li><a href="<?= site_url('admin/taxateurs_prefill') ?>"><span class="title">Pré-inscriptions Taxateurs</span></a></li>
								</ul>
							</li>
 							*/ ?>
							<li><a href="<?= site_url('admin/entreprises') ?>"><span class="title color-sbt2">Participants</span></a></li>
							<li><a href="<?= site_url('admin/produits') ?>"><span class="title color-sbt2">Produits</span></a></li>
							<li><a href="<?= site_url('admin/taxateurs') ?>"><span class="title color-sbt2">Taxateurs</span></a></li>

						<?php endif; ?>
						<?php if (SBTParticipant::isManager()): ?>
							<li><a href="<?= site_url('admin/arrivage') ?>"><span class="title color-sbt2">Arrivage</span></a></li>
							<li><a href="<?= site_url('photos') ?>"><span class="title color-sbt2">Photos</span></a></li>
							<li><a href="<?= site_url('admin/commentaires') ?>"><span class="title color-sbt2">Commentaires</span></a></li>

						<?php endif; ?>
						<?php if (SBTParticipant::isAdmin()): ?>
							<li><a href="<?= site_url('admin/tables') ?>"><span class="title color-sbt2">Tables</span></a></li>
							<li class="bg-sbt2">
								<a href="javascript:;"><span class="title color-sbt2">Résultats</span>
									<span class=" arrow color-sbt2"></span></a>
								<ul class="bg-sbt2">
									<li><a href="<?= site_url('admin/resultats') ?>"><span class="title color-sbt2">Tous les résultats</span></a></li>
									<li><a href="<?= site_url('admin/medailles') ?>"><span class="title color-sbt2">Médailles</span></a></li>
									<li><a href="<?= site_url('admin/prixCategorie') ?>"><span class="title color-sbt2">Prix Catégorie</span></a></li>
									<li><a href="<?= site_url('admin/prixAcademy') ?>"><span class="title color-sbt2">SBT Academy</span></a></li>
									<li><a href="<?= site_url('admin/champions') ?>"><span class="title color-sbt2">Champions</span></a></li>
								</ul>
							</li>
						<?php endif; ?>
						<?php if (!SBTParticipant::isManager()): ?>
							<li><a href="<?= site_url('entreprise') ?>"><span class="title color-sbt2"><?= t('mon_entreprise') ?></span></a></li>
							<li><a href="<?= site_url('produits') ?>"><span class="title color-sbt2"><?= t('mes_produits') ?></span></a></li>
						<?php endif; ?>
						<li><a href="<?= site_url('assets/SBT2024-Reglement_' . lang() . '.pdf') ?>" target="_blank" class="color-sbt2"><?= t('reglement') ?></a></li>
					</ul>
				</div>
			</div>
		</div>
	<?php endif; ?>
</div>
<div class="page-container ">
	<div class="page-content-wrapper ">
		<div class="content">
			<div class="content sm-gutter">
				<div class="container">
					<?php if (SBTNotification::hasMessages()): ?>
						<div class="wrapper wrapper-content">
							<?php
							$successes = SBTNotification::getSuccesses();
							if (count($successes) > 0):
								foreach ($successes as $m):
									?>
									<div class="alert alert-success"><?php echo $m; ?></div>
								<?php
								endforeach;
							endif;
							?>
							<?php
							$warnings = SBTNotification::getWarnings();
							if (count($warnings) > 0):
								foreach ($warnings as $m):
									?>
									<div class="alert alert-warning"><?php echo $m; ?></div>
								<?php
								endforeach;
							endif;
							?>
							<?php
							$errors = SBTNotification::getErrors();
							if (count($errors) > 0):
								foreach ($errors as $m):
									?>
									<div class="alert alert-danger"><?php echo $m; ?></div>
								<?php
								endforeach;
							endif;
							?>
						</div>
					<?php endif; ?>
				</div>

