<!-- START COPYRIGHT -->
<!-- START CONTAINER FLUID -->
<!-- START CONTAINER FLUID -->
<div class=" container   container-fixed-lg footer">
	<div class="copyright sm-text-center">
		<p class="small no-margin pull-left sm-pull-reset">
			<span class="hint-text">Copyright &copy; <?= date('Y')?> </span>
			<span class="font-montserrat">Swiss Bakery Trophy</span>.
			<span class="hint-text">Tous droits réservés. </span>
			<?php /*<span class="sm-block"><a href="#" class="m-l-10 m-r-10">Terms of use</a> <span class="muted">|</span> <a href="#" class="m-l-10">Privacy Policy</a></span>*/ ?>
		</p>
		<?php /*<p class="small no-margin pull-right sm-pull-reset">
            Hand-crafted <span class="hint-text">&amp; made with Love</span>
        </p>
 */ ?>
		<div class="clearfix"></div>
	</div>
</div>
<!-- END COPYRIGHT -->
</div>
<!-- END PAGE CONTENT WRAPPER -->
</div>
<!-- END PAGE CONTAINER -->

<?php
SBTInclude::jsFile(array(
//		'assets/plugins/pace/pace.min.js',
		'assets/plugins/modernizr.custom.js',
		'assets/plugins/jquery-ui/jquery-ui.min.js',
//		'assets/plugins/tether/js/tether.min.js',
		'assets/plugins/bootstrap/js/bootstrap.min.js',
//		'assets/plugins/jquery/jquery-easy.js',
//		'assets/plugins/jquery-unveil/jquery.unveil.min.js',
//		'assets/plugins/jquery-bez/jquery.bez.min.js',
//		'assets/plugins/jquery-ios-list/jquery.ioslist.min.js',
//		'assets/plugins/select2/js/select2.full.min.js',
//		'assets/plugins/imagesloaded/imagesloaded.pkgd.min.js',
//		'assets/plugins/jquery-actual/jquery.actual.min.js',
//		'assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js',
//		'pages/js/pages.js',
//		'assets/js/scripts.js',
//		'js/custom.js?2',
//		'assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js',
//		'assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js',
//		'assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js',
//		'assets/plugins/datatables-responsive/js/datatables.responsive.js',
//		'assets/plugins/datatables-responsive/js/lodash.min.js',
//		'js/plugins/datatables/dataTables.buttons.min.js',
//		'js/plugins/datatables/dataTables.tableTools.min.js',
		'js/plugins/buttons/buttons.html5.min.js'
));

SBTInclude::printJs();
SBTInclude::printCss();
?>
<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>-->
<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>-->
<!--<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.18/b-1.5.4/b-colvis-1.5.4/b-html5-1.5.4/b-print-1.5.4/datatables.min.js"></script>-->
</body>
</html>
