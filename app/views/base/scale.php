<?php
/* @var $label String */
/* @var $name String */
?>
<div class="form-group row">
    <label class="col-12 control-label"><h4 class="text-center"><?= $label ?></h4></label>
<!--    <div class="col-12 col-sm-1">--><?//= t('note'); ?><!--</div>-->
    <div class="col-12 col-sm-12 row">
        <?php for ($i = 1; $i <= 10; $i++): ?>
            <div class="col-1 text-center" style="width:10%">
				<?= form_radio($name . '[note]', $i, false, 'id=' . $name . '-note-' . $i . ' required onchange="checkscore(this)"'); ?>
				<br>
				<label for="<?= $name . '-note-' . $i ?>" style="font-size:9px;height:40px"><?= t('scale_'.$i) ?></label>
            </div>
        <?php endfor; ?>
    </div>

    <div class="col-12 col-sm-2"><?= t('commentaire'); ?></div>

    <div class="col-12 col-sm-10">
        <?= form_textarea($name . '[commentaire]', set_value($name . '[commentaire]'),
            array('class' => 'form-control', 'style' => 'height:50px', 'id' => $name . '-commentaire', 'required' => 'required', ' maxlength' => '250'))
        ?>
    </div>
</div>

