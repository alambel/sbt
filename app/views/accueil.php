<?php
if (SBTParticipant::isTaxateur()) {
	redirect('taxation');
}
redirect('s');
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-121731481-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}

		gtag('js', new Date());

		gtag('config', 'UA-121731481-1');
	</script>

	<meta charset="utf-8">
	<title>Swiss Bakery Trophy</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="stylesheets/menu.css">
	<!-- CSS
	================================================== -->
	<link rel="stylesheet" href="stylesheets/menu.css">
	<link rel="stylesheet" href="stylesheets/flat-ui-slider.css">
	<link rel="stylesheet" href="stylesheets/base.css">
	<link rel="stylesheet" href="stylesheets/skeleton.css">
	<link rel="stylesheet" href="stylesheets/landings.css">
	<link rel="stylesheet" href="stylesheets/main.css?2">
	<link rel="stylesheet" href="stylesheets/landings_layouts.css">
	<link rel="stylesheet" href="stylesheets/box.css">
	<link rel="stylesheet" href="stylesheets/pixicon.css">
	<style>
		.parrains img {
			width: 220px;
		}

		.partners img {
			width: 220px;
		}
	</style>
	<!--[if lt IE 9]>
	<script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<!-- Favicons
	================================================== -->
	<link rel="shortcut icon" href="favicon.ico">
</head>
<body>

<div class="header_nav_1 dark inter_3_bg pix_builder_bg" id="section_intro_3">
	<div class="header_style">
		<div class="container">
			<div class="sixteen columns firas2">
				<nav role="navigation" class="navbar navbar-white navbar-embossed navbar-lg pix_nav_1">
					<div class="containerss">
						<div class="navbar-header" style="background-color: white;">
							<button data-target="#navbar-collapse-02" data-toggle="collapse" class="navbar-toggle" type="button">
								<span class="sr-only">Toggle navigation</span>
							</button>
							<img src="img/logosbt2024.jpg" class="pix_nav_logo" alt="" height="100">
						</div>
						<div id="navbar-collapse-02" class="navbar-collapse">
							<ul class="nav navbar-nav navbar-right bg-sbt"
							">
							<li class="propClone"><a href="<?= site_url('academy') ?>" style="color: white">Academy</a></li>
							<li class="active propClone"><a href="<?= site_url('assets/SBT2024-Reglement_' . lang() . '.pdf') ?>" target="_blank" style="color: white"><?= t('reglement') ?></a></li>
							<?php if (INSCRIPTIONS_OUVERTES): ?>
								<li class="propClone"><a href="<?= site_url('inscription') ?>" style="color: white"><?= t('inscription_entreprise') ?></a></li>
							<?php endif; ?>
							<?php if (INSCRIPTIONS_TAXATEUR_OUVERTES): ?>
								<li class="propClone"><a href="<?= site_url('inscription_taxateur') ?>" style="color: white"><?= t('inscription_taxateur') ?></a></li>
							<?php endif; ?>
							<?php $user = SBTParticipant::getCurrentUserId();
							if ($user):?>
								<li class="propClone"><a href="<?= site_url(SBTParticipant::isTaxateur() ? 'taxation' : 'entreprise') ?>" style="color: white"><?= t('mon_compte'); ?></a></li>
							<?php else: ?>
								<li class="propClone"><a href="<?= site_url('login') ?>" style="color: white"><?= t('login'); ?></a></li>
							<?php endif; ?>
							<?php $this->load->view('base/language_selector'); ?>
							</ul>
						</div><!-- /.navbar-collapse -->
					</div><!-- /.container -->
				</nav>
			</div>
		</div><!-- container -->
	</div>
	<div class="container">
		<div class="sixteen columns margin_bottom_50 padding_top_60">
			<div class="twelve offset-by-two columns">
				<div class="center_text big_padding">
					<p class="big_title bold_text editContent">Swiss Bakery Trophy 2024</p>
					<p class="big_title bold_text editContent"><?= t('welcome_dates') ?></p>
					<p class="big_text editContent"><?= t('welcome_subtitle') ?></p>

					<a href="<?= base_url('assets/SBT2021_Medailles.pdf') ?>" class="pix_button pix_button_line white_border_button bold_text big_text btn_big" target="_blank">
						<i class="pi pixicon-paper"></i>
						<span><?= t('resultats'); ?></span>
					</a><br>
					<?php if (lang() == 'de'): ?>
						&nbsp;<a href="<?= base_url('assets/SBT2024_Pressemitteilung_Daten.pdf') ?>" class="pix_button pix_button_line white_border_button bold_text big_text btn_big" target="_blank">
							<i class="pi pixicon-paper"></i>
							<span>Pressemitteilung</span>
						</a>
						<br>

						<a href="<?= base_url('assets/SBTAcademy-Pressemitteilung.pdf') ?>" class="pix_button pix_button_line white_border_button bold_text big_text btn_big" target="_blank">
							<i class="pi pixicon-paper"></i>
							<span>Swiss Bakery Trophy Academy</span>
						</a>
					<?php else: ?>
						<a href="<?= base_url('assets/SBT2024_CommuniquePresseFR.pdf') ?>" class="pix_button pix_button_line white_border_button bold_text big_text btn_big" target="_blank">
							<i class="pi pixicon-paper"></i>
							<span>Communiqué de presse</span>
						</a>
						<br>
						<a href="<?= base_url('assets/SBTAcademy-CommuniquePresse.pdf') ?>" class="pix_button pix_button_line white_border_button bold_text big_text btn_big" target="_blank">
							<i class="pi pixicon-paper"></i>
							<span>Swiss Bakery Trophy Academy</span>
						</a>
					<?php endif; ?>
					<br>


					<?php if (INSCRIPTIONS_OUVERTES): ?>
						<a href="<?= site_url('inscription') ?>" class="pix_button pix_button_line white_border_button bold_text big_text btn_big">
							<i class="pi pixicon-paper"></i>
							<span><?= t('inscription_entreprise'); ?></span>
						</a>
						<a href="<?= INSCRIPTIONS_TAXATEUR_OUVERTES ? site_url('inscription_taxateur') : '#' ?>" class="pix_button pix_button_line white_border_button bold_text big_text btn_big" style="height:30px">
							<i class="pi pixicon-paper"></i>
							<span><?= t('inscription_taxateur'); ?></span>
							<?php if (!INSCRIPTIONS_TAXATEUR_OUVERTES): ?>
								<div class="product-label complet"><?= t('complet') ?></div>
							<?php endif; ?>
						</a>
					<?php endif; ?>
				</div>
			</div>
		</div>
		<div class="center_text">
			<a href="#section_services" class="intro_arrow">
				<i class="pi pixicon-arrow-down"></i>
			</a>
		</div>
	</div>
</div>

<div class="" id="section_services">
	<div class="big_padding padding_bottom_0 pix_builder_bg">
		<div class="container">
			<div class="sixteen columns">
				<div class="sixteen columns alpha">
					<p class="big_text center_text editContent">
					<div class="main-text event">
						<h1>Les meilleurs artisans boulangers-pâtissiers-confiseurs suisses récompensés</h1>
						<p>La 10ème édition du Swiss Bakery Trophy aura lieu du 30 octobre au 3 novembre 2024 à Espace Gruyère, à Bulle (FR). Ce concours national récompense la créativité, l’originalité et le savoir-faire de l’artisan. Pour un boulanger-pâtissier-confiseur, une médaille au Swiss Bakery Trophy est la plus haute distinction qu’il est possible d’obtenir dans la branche.</p>
						<p>Près de 1300 produits seront évalués durant quatre jours par 180 professionnels et 120 consommateurs. Ce concours national des boulangers-pâtissiers-confiseurs se déroulera en public, dans le cadre du Salon Suisse des Goûts et Terroirs. Il vise à mettre en lumière la richesse et la qualité du savoir-faire des artisans de la branche, ainsi qu’à montrer le métier et ses produits auprès des consommateurs. Face à une concurrence toujours plus forte, cette manifestation est une plateforme marketing incontournable qui permet aux artisans de se démarquer clairement des grandes surfaces et des discounters.</p>
						<h2>Palmarès 2022-2023</h2>
						<p><strong>Swiss Bakery Trophy Champion</strong> (Ce prix récompense la meilleure moyenne sur la base de cinq produits)<br>
							<em>Chocolats Kaufmann à Buchs (AG)</em></p>
						<p><strong>Premier prix pour le produit de boulangerie</strong> (2 ex-aequo)<br>
							<em>Panettone al Mandarino di Lipari</em>, La Fabricca del Panettone à St. Gallen (SG)<br>
							<em>Wetterhornbrot</em>, Bäckerei-Konditorei-Café Ringgenberg à Grindelwald (BE)</p>
						<p><strong>Premier prix pour le produit de pâtisserie</strong> (3 ex-aequo)<br>
							<em>Granny swiss</em>, Bäckerei-Confiserie Mohn à Sulgen (TG)<br>
							<em>Murano</em>, Maison Eric Vuissoz à Ste-Croix (VD)<br>
							<em>Vermicelles</em>, Maison Eric Vuissoz à Ste-Croix (VD)</p>
						<p><strong>Premier prix pour le produit de confiserie</strong> (4 ex-aequo)<br>
							<em>Praliné « La Golée »</em>, L'ART'isan Pâtissier à Prez-vers-Noréaz (FR)<br>
							<em>Praliné n°9</em>, L'ART'isan Pâtissier à Prez-vers-Noréaz (FR)<br>
							<em>Aprikosen Brotaufstrich</em>, Bäckerei Konditorei Café Weber à Davos Dorf (GR)<br>
							<em>Tartinade de l’Ecureuil</em>, Boulangerie Dubey-Grandjean à Romont (FR)</p>
						<p><strong>Premier prix pour le produit de snack-traiteur</strong> (2 ex-aequo)<br>
							<em>Engelberger Käsekuchen</em>, Bäckerei Dossenbach à Engelberg (OW)<br>
							<em>Flûtes au Gruyère AOP</em>, Boulangerie-Tea-Room Les Arcades à Charmey (FR)</p>

						<p><strong>Prix Bernhard Aebersold</strong> (Ce prix récompense le produit le plus créatif)<br>
							<em>Pompon de Princesse</em>, La Pâtisserie David Schmid à Zofingen (AG)</p>
						<h2>Champions cantonaux</h2>
						<strong>AG</strong> : Chocolats Kaufmann, Buchs<br>
						<strong>AI</strong> : BÖHLI AG Bäckerei-Confiserie, Appenzell<br>
						<strong>AR </strong>: Bäckerei Kast AG, Reute<br>
						<strong>BE </strong>: Bäckerei-Konditorei-Café Ringgenberg GmbH, Grindelwald <br>
						<strong>BL </strong>: Konditorei Buchmann AG, Münchenstein<br>
						<strong>BS </strong>: Confiserie Bachmann AG, Basel<br>
						<strong>FL </strong>: Josef Amann AG, Vaduz<br>
						<strong>FR </strong>: Boulangerie Ecoffey Didier, Romont<br>
						<strong>GE </strong>: Boulangerie Oberson SA, Vernier<br>
						<strong>GR </strong>: Bäckerei Konditorei Café Weber, Davos Dorf<br>
						<strong>JU </strong>: Parrat Boulangerie-Confiserie, Saignelégier<br>
						<strong>LU </strong>: Ehliger Bäckerei Konditorei Confiserie, Hochdorf<br>
						<strong>NE </strong>: Boulangerie Conrad SA, Le Landeron<br>
						<strong>NW </strong>: Christen Beck AG, Buochs<br>
						<strong>OW </strong>: Beck Berwert, Stalden<br>
						<strong>SG </strong>: Confiserie Hirschy AG, Wil SG<br>
						<strong>SH </strong>: Konditorei am Schaubmarkt, Stein am Rhein<br>
						<strong>SO </strong>: Confiserie Hofer, Solothurn<br>
						<strong>SZ </strong>: Konditorei-Confiserie Schwarzenberger, Küssnacht am Rigi<br>
						<strong>TG </strong>: Bäckerei-Confiserie Mohn, Sulgen<br>
						<strong>TI </strong>: Confiserie Al Porto SA, Tenero<br>
						<strong>VD </strong>: Maison Eric Vuissoz, Ste-Croix<br>
						<strong>VS </strong>: Zenhäusern Frères SA, Sion<br>
						<strong>ZG </strong>: Bäckerei Hotz Rust, Baar<br>
						<strong>ZH </strong>: Wylandbeck, Truttikon</p>


						<p><strong>Finale romande et tessinoise des meilleur(e)s jeunes boulangers(ères)- pâtissiers(ères)</strong><br>
							Amandine Freudiger<br>
							Entreprise formatrice : Parrat Artisan Boulanger-Confiseur, Saignelégier (JU)<br>
							Employeur : Parrat Artisan Boulanger-Confiseur, Saignelégier (JU)</p>
						<p><a href="https://www.lepain.ch/download_file/view/64/196/196/196" target="_blank">Communiqué de presse</a> (PDF)</p>
						<h3>Galerie photos</h3>
						<a href="https://www.dropbox.com/sh/opzm515aphg7mnx/AACrxb86iLfVe9wGjJFrBVoqa?dl=0" target="_blank">https://www.dropbox.com/sh/opzm515aphg7mnx/AACrxb86iLfVe9wGjJFrBVoqa?dl=0 (© Swiss Bakery Trophy)</a>
						<h3>Sur internet</h3>
						<p>
							<a href="https://www.lepain.ch" target="_blank">www.lepain.ch</a><br>
							<a href="https://www.facebook.com/swissbakerytrophy" target="_blank">Facebook: @swissbakerytrophy</a>
						</p>

						<p><a href="<?= base_url('assets/SBT2024_CommuniquePresseFR.pdf') ?>" target="_blank">Communiqué de presse</a> (PDF)<br>
							<a href="<?= base_url('assets/SBT2024_Pressemitteilung_Daten.pdf') ?>" target="_blank">Pressemitteilung</a> (PDF)</p>

					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="" id="section_services">
		<div class="big_padding padding_bottom_0 pix_builder_bg">
			<div class="container">
				<div class="sixteen columns">
					<div class="sixteen columns alpha">
						<p class="big_title bold_text center_text editContent" style="font-size:42px;"><?= t('welcome_2_title'); ?></p>
					</div>
					<!--
                <div class="sixteen columns alpha">
                    <p class="big_text center_text editContent"><?= t('welcome_2_subtitle'); ?></p>
                </div>
                <div class="sixteen columns alpha">
                    <p class="center_text"><?= t('intro_repartition_cantons'); ?></p>
                    <table class="table" style="width:100%;text-align:center;
    border-spacing: 2px !important;
 border: 1px solid grey !important;
    ">
                        <thead>
                        <th style="border-spacing: 2px !important;
 border: 1px solid grey !important;font-weight: bold"><?= t('mercredi') ?></th>
                        <th style="border-spacing: 2px !important;
 border: 1px solid grey !important;font-weight: bold"><?= t('jeudi') ?></th>
                        <th style="border-spacing: 2px !important;
 border: 1px solid grey !important;font-weight: bold"><?= t('vendredi') ?></th>
                        <th style="border-spacing: 2px !important;
 border: 1px solid grey !important;font-weight: bold"><?= t('samedi') ?></th>
                        </thead>
                        <tbody>
                        <tr>
                            <td style="border-spacing: 2px !important;
 border: 1px solid grey !important;">AG<br>AI-AR<br>GL<br>SG<br>VS<br>TI
                            </td>
                            <td style="border-spacing: 2px !important;
 border: 1px solid grey !important;">BS-BL<br>GE<br>GR<br>VD<br>ZG
                            </td>
                            <td style="border-spacing: 2px !important;
 border: 1px solid grey !important;">BE + SO<br>JU<br>OW – NW<br>TG<br>SZ
                            </td>
                            <td style="border-spacing: 2px !important;
 border: 1px solid grey !important;">FR<br>LU<br>NE<br>ZH
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <br><br>
                </div>
                -->
				</div>
			</div>
		</div>
	</div>


	<div class="" id="section_features_3">
		<div class="big_padding padding_top_0 pix_builder_bg">
			<div class="container">
				<div class="sixteen columns">
					<div class="one-third column onethird_style alpha">
						<div class="f1_box">
							<div class="margin_bottom">
								<span class="small_title bold_text"><span class="editContent"><span class="pix_text">Renseignements :</span></span></span><br>
								<span class="editContent light_gray normal_text"><span class="pix_text">ARABPC<br>Catherine Oberson<br><a href="tel:0269198751">Tél. 026 919 87 51</a></span></span>
							</div>
							<a href="mailto:info@lepain.ch" class="pix_button bold_text small_button pix_button_line orange">
								<i class="pi pixicon-mail"></i>
								<span>info@lepain.ch</span>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="hl2 pix_builder_bg" id="section_highlight_2">
		<div class="big_padding highlight-section">
			<div class="highlight-right pix_builder_bg"></div>
			<div class="container">
				<div class="sixteen columns">
					<div class="eight columns alpha">
						<br>
					</div>
					<div class="eight columns omega ">
						<div class="highlight_inner">
							<p class="big_title editContent" style="font-size:35px"><?= t('welcome_5_title') ?></p>
							<p class="bold_text margin_bottom normal_text orange editContent"><?= t('welcome_5_subtitle') ?></p>
							<p class="normal_text light_gray editContent"><?= t('welcome_5_text') ?></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="pix_builder_bg hl1" id="section_highlight_1">
		<div class="big_padding highlight-section">
			<div class="highlight-left pix_builder_bg"></div>
			<div class="container ">
				<div class="sixteen columns ">
					<div class="eight columns alpha">
						<div class="highlight_inner">
							<p class="big_title center_text editContent"><?= t('welcome_3_title') ?></p>
							<img src="img/lepain.png" width="400"/>
						</div>
					</div>
					<div class="eight columns omega ">

					</div>
				</div>
			</div>
		</div>
	</div>


	<div class="" id="section_clients_2">
		<div class="big_padding light_gray_bg">
			<div class="container">
				<div class=" pix_inline_block ">
					<div class="sixteen columns ">
						<!--                    <div class="four columns alpha">-->
						<p class="big_text center_text editContent"><?= t('welcome_4_title') ?></p>
						<!--                    </div>-->
						<!--                    <div class="four columns center_text">-->
						<!--						<img src="img/sponsors.png" class="margin_vertical" alt="" width="900" style="max-width:100%">-->
						<div class="ccm-custom-style-container ccm-custom-style-extracontent-118">
							<h2 class="galleria-title">Partenaires</h2>
							<div class="partners">
								<a href="https://www.fpe-ciga.ch" target="_blank"><img src="<?= base_url('img/partenaires/fpe.jpg') ?>" alt="Fédération Patronale et Économique"></a>
								<a href="https://www.gmsa.ch" target="_blank"><img src="<?= base_url('img/partenaires/gmsa.jpg') ?>" alt="GMSA"></a>
								<a href="https://www.pistor.ch/fr/home" target="_blank"><img src="<?= base_url('img/partenaires/pistor.jpg') ?>" alt="Pistor"></a>
								<a href="https://pitec.ch" target="_blank"><img src="<?= base_url('img/partenaires/pitec.jpg?v3') ?>" alt="Pitec"></a>
								<a href="https://www.swissbaker.ch" target="_blank"><img src="<?= base_url('img/partenaires/sbc.jpg') ?>" alt="Boulangers-Pâtissiers-Confiseurs suisses"></a>
								<a href="https://www.safeatwork.ch/fr" target="_blank"><img src="<?= base_url('img/partenaires/safeatwork.jpg') ?>" alt="Safe At Work"></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<? /*

<div class="dark pix_builder_bg" id="section_call_2_dark">
    <div class="footer3">
        <div class="container ">
            <div class="sixteen columns alpha">
                <div class="content_div center_text">
                    <div class="margin_bottom_30">
                        <p class="big_title bold_text editContent"><?= t('inscrivez_vous_maintenant') ?></p>
                    </div>
                    <a href="<?= site_url('inscription') ?>" class="pix_button pix_button_line bold_text orange btn_big">
                        <span><?= t('inscription_entreprise') ?></span>
                    </a>&nbsp;
                    <a href="#<?= null//site_url('inscription_taxateur')       ?>" class="pix_button pix_button_line bold_text orange btn_big">
                        <span><?= t('inscription_taxateur') ?></span>
                        <div class="product-label complet"><?= t('complet') ?></div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
*/ ?>

	<div class="pix_builder_bg dark soft_dark_gray_bg" id="section_footer_5_dark">
		<div class="footer3">
			<div class="container">
				<div class="sixteen columns ">
					<div class="four column alpha mobile_center margin_vertical ">
						<div class="pix_div_fit">
							<img src="img/logosbt2024.jpg" class="pix_footer_logo pix_img_fit" alt="" width="200">
						</div>
					</div>
					<div class="four column alpha mobile_center margin_vertical ">
						<p class="small_text light_gray left_text mobile_center editContent"><?= t('welcome_2_title') ?></p>
						<p><a href="<?= site_url('assets/SBT2024-Reglement_fr.pdf') ?>" target="_blank" style="color: white">Règlement et conditions de participation</a><br>
							<a href="<?= site_url('assets/SBT2024-Reglement_de.pdf') ?>" target="_blank" style="color: white">Reglement und Teilnahmebedingungen</a></p>
						<ul class="bottom-icons center_text big_title">
							<li><a class="pi pixicon-instagram2 normal_gray" target="_parent" href="https://www.instagram.com/swissbakerytrophy/"></a></li>
							<li><a class="pi pixicon-linkedin2 normal_gray" target="_parent" href="https://www.linkedin.com/company/swiss-bakery-trophy/"></a></li>
							<li><a class="pi pixicon-facebook2 normal_gray" target="_parent" href="https://www.facebook.com/swissbakerytrophy/"></a></li>
							<li><a class="pi pixicon-twitter2 normal_gray" target="_blank" href="https://twitter.com/chbakerytrophy"></a></li>
							<li><a class="pi pixicon-flickr2 normal_gray" target="_blank" href="https://www.flickr.com/photos/127946990@N07/"></a></li>
						</ul>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>

<script src="js/jquery-2.1.1.js" type="text/javascript"></script>
<script src="js/jquery.easing.1.3.js" type="text/javascript"></script>
<script src="js/jquery.ui.touch-punch.min.js"></script>
<script src="js/ticker.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bootstrap-switch.js"></script>
<script src="js/appear.min.js" type="text/javascript"></script>
<script src="js/smoothscroll.min.js" type="text/javascript"></script>
<script src="js/custom.js?2" type="text/javascript"></script>
</body>
</html>
