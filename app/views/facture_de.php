<?php
/**
 * @var $p SBTParticipant
 */

$fmt = new NumberFormatter( 'fr_CH', NumberFormatter::CURRENCY );
?>
<div><img src="<?= base_url('img/entete.jpg'); ?>" alt="Logo" width="100%"/></div>
<table style="border:none">
	<tr>
		<td style="width:70%"></td>
		<td>
			<?= $p->entreprise ?><br>
			<?= $p->nom ?> <?= $p->prenom ?><br>
			<?= $p->rue ?><br>
			<?= $p->npa ?> <?= $p->lieu ?>
		</td>
	</tr>
</table>
<br><br>
<div>
	<small>CHE-107.875.247 TVA<br>
		Ref. : co
	</small>
</div>
<div style="text-align: right">
	Bulle, <?= date('d.m.Y') ?>
</div>


<h3>Rechnung F<?= sprintf('%04d', $p->id) ?></h3>
<p>Sehr geehrte Damen und Herren<br>Geschätze Kollegen,</p>
<p>Wir freuen uns, Ihnen Ihre Teilnahme and der Swiss Bakery Trophy 2024 zu bestätigen und bitten Sie, die Gebühr für Ihre angemeldeten Produkte zu begleichen.</p>
<br>
<table style="width:100%; border-collapse: collapse;  border: 1px solid black;" cellpadding="8" border="1">
	<thead>
	<tr>
		<th style="text-align: left">anzahl</th>
		<th style="text-align: left">Artikeln</th>
		<th style="text-align: left"></th>
		<th style="text-align: left">Total</th>
	</tr>
	</thead>
	<tbody>
	<tr>
		<td style="text-align: center"><?= $p->getNbProduits() ?></td>
		<td>Zu bewertende Produkte an der Swiss Bakery Trophy 2024</td>
		<td>Fr.</td>
		<td style="text-align: right"><?= $fmt->formatCurrency($p->getTotal(), "CHF"); ?></td>
	</tr>
	<tr>
		<td></td>
		<td style="text-align: right">+ Mwst. 8.1 %</td>
		<td>Fr.</td>
		<td style="text-align: right"><?= $fmt->formatCurrency($p->getTotal() * 0.081, "CHF") ?></td>
	</tr>
	<tr>
		<td></td>
		<td style="text-align: right"><b>Total</b></td>
		<td>Fr.</td>
		<td style="text-align: right"><b><?=$fmt->formatCurrency($p->getTotal() * 1.081, "CHF") ?></b></td>
	</tr>
	</tbody>
</table>
<br>
<p><b>Zahlungsbedingungen</b> : gemäss Teilnahmebedingungen sind die Kosten vor dem 15. November 2024 (IBAN CH70 0076 8250 1157 5060 3 - Banque Cantonale de Fribourg)</p>
<p>Wir danken Ihnen für Ihr Interesse an der Swiss Bakery Trophy und grüssen Sie Freundlich</p>
<br>
<p style="text-align: right"><strong>Association romande des artisans<br/>boulangers-pâtissiers-confiseurs<br><i>Swiss Bakery Trophy</i></strong></p>

<div><img src="<?= base_url('img/qr.jpg'); ?>" alt="QR-Code" width="100%"/></div>
