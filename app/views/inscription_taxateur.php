<?php $this->load->view('base/header');

?>
	<!-- START CONTAINER FLUID -->
	<div class=" no-padding    container-fixed-lg bg-white">
		<div class="container">
			<!-- START BREADCRUMB -->
			<div class="row"><h1><?= t('inscription_taxateur'); ?></h1></div>
			<div class="row">
				<div class="col-xl-12 col-lg-12 ">
					<!-- START card -->
					<div class="card card-transparent">
						<div class="card-block">
							<?php echo form_open(); ?>
							<div class="row ">
								<div class="col-md-12">
									<label><?= t('langue') ?></label>
									<div class="radio radio-success">
										<input type="radio" value="fr" name="taxateur[langue]" id="fr" required <?= $taxateur->langue == 'F' ? 'checked' : '' ?>>
										<label for="fr">Français</label>
										<input type="radio" value="de" name="taxateur[langue]" id="de" required <?= $taxateur->langue == 'D' ? 'checked' : '' ?>>
										<label for="de">Deutsch</label>
									</div>

								</div>
							</div>
							<div class="row ">
								<div class="col-md-12">
									<label><?= t('type_taxateur') ?></label>
									<div class="radio radio-success">
										<input type="radio" value="conso" name="taxateur[type]" id="conso" required <?= $taxateur->type == 'conso' ? 'checked' : '' ?>>
										<label for="conso"><?= t('consommateur') ?></label>
										<input type="radio" value="pro" name="taxateur[type]" id="pro" required <?= $taxateur->type == 'pro' ? 'checked' : '' ?>>
										<label for="pro"><?= t('professionnel') ?></label>
									</div>

								</div>
							</div>
							<div class="row">
								<p id="complet" style="display:none;color:red"><?= t('inscription_taxateur_complet'); ?></p>
							</div>
							<div id="part2">
								<div class="row ">
									<div class="col-md-12">
										<label><?= t('civilite') ?></label>
										<div class="radio radio-success">
											<input type="radio" value="F" name="taxateur[civilite]" id="F" required <?= $taxateur->civilite == 'F' ? 'checked' : '' ?>>
											<label for="F"><?= t('madame') ?></label>
											<input type="radio" value="M" name="taxateur[civilite]" id="M" required <?= $taxateur->civilite == 'M' ? 'checked' : '' ?>>
											<label for="M"><?= t('monsieur') ?></label>
										</div>

									</div>
								</div>
								<div class="row ">
									<div class="col-md-6">
										<div class="form-group form-group-default required">
											<label><?= t('nom') ?></label>
											<input type="text" class="form-control" name="taxateur[nom]" required
												   value="<?= $taxateur ? $taxateur->nom : set_value('taxateur[nom]'); ?>">
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group form-group-default required">
											<label><?= t('prenom') ?></label>
											<input type="text" class="form-control" name="taxateur[prenom]" required
												   value="<?= $taxateur ? $taxateur->prenom : set_value('taxateur[prenom]'); ?>">
										</div>
									</div>
								</div>
								<div class="row ">
									<div class="col-md-12">
										<div class="form-group form-group-default">
											<label><?= t('entreprise') ?></label>
											<input type="text" class="form-control" name="taxateur[entreprise]"
												   value="<?= $taxateur ? $taxateur->entreprise : set_value('taxateur[entreprise]'); ?>">
										</div>
									</div>
								</div>
								<div class="row ">
									<div class="col-md-12">
										<div class="form-group form-group-default required">
											<label><?= t('rue') ?></label>
											<input type="text" class="form-control" name="taxateur[rue]" required
												   value="<?= $taxateur ? $taxateur->rue : set_value('taxateur[rue]'); ?>">
										</div>
									</div>
								</div>
								<div class="row ">
									<div class="col-sm-4">
										<div class="form-group form-group-default required">
											<label><?= t('npa') ?></label>
											<input type="text" class="form-control" name="taxateur[npa]" required
												   value="<?= $taxateur ? $taxateur->npa : set_value('taxateur[npa]'); ?>">
										</div>
									</div>
									<div class="col-sm-8">
										<div class="form-group form-group-default required">
											<label><?= t('ville') ?></label>
											<input type="text" class="form-control" name="taxateur[lieu]" required
												   value="<?= $taxateur ? $taxateur->lieu : set_value('taxateur[lieu]'); ?>">
										</div>
									</div>
								</div>
								<div class="row ">
									<div class="col-sm-12">
										<div class="form-group form-group-default required">
											<label>Canton</label>
											<?= form_dropdown('taxateur[canton]', SBTParticipant::$cantons, $taxateur ? $taxateur->canton : set_value('taxateur[canton]'), 'id="canton" class="full-width" data-init-plugin="select2" required'); ?>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group form-group-default required">
											<label><?= t('email') ?></label>
											<input type="email" class="form-control" name="taxateur[email]" required
												   value="<?= $taxateur ? $taxateur->email : set_value('taxateur[email]'); ?>">
										</div>
									</div>
								</div>
								<div class="row ">
									<div class="col-md-12">
										<div class="form-group form-group-default required">
											<label><?= t('telephone') ?></label>
											<input type="phone" class="form-control" name="taxateur[telephone]" id="telephone" required
												   value="<?= $taxateur ? $taxateur->telephone : set_value('taxateur[telephone]'); ?>">
										</div>
									</div>
								</div>

								<div class="row ">
									<div class="col-md-12">
										<label><?= t('taxateur_dispo_label') ?></label>
										<div class="radio radio-success">
											<input type="radio" value="1" name="taxateur[dispo]" id="dispo-1" required <?= $taxateur->dispo == '1' ? 'checked' : '' ?>>
											<label for="dispo-1"><?= t('taxateur_dispo_option1') ?></label>
											<input type="radio" value="2" name="taxateur[dispo]" id="dispo-2" required <?= $taxateur->dispo == '2' ? 'checked' : '' ?>>
											<label for="dispo-2"><?= t('taxateur_dispo_option2') ?></label>
										</div>
									</div>
								</div>
								<div class="row ">
									<div class="col-md-12">
										<div class="form-group form-group-default required">
											<label><?= t('disponibilites') ?></label>
											<span class="help"><?= t('infos_disponibilite') ?></span><br>
											<span class="help">Les personnes qui s’inscrivent dès aujourd’hui à certaines tranches horaires seront en liste d’attente, à part pour les tranches horaires suivantes :<br>
- Professionnels :<br>
Mercredi 30, 09h30 - 15h00 - 4 personnes<br>
Mercredi 30, 15h00 - 20h30 - 5 personnes<br>
Jeudi 31, 16h00 - 20h30 - 1 personne<br>
Vendredi 1, 15h00 - 20h30 - 5 personnes<br>
Samedi 2, 15h00 - 19h30 - 1 personne<br>
<br>
- Consommateurs :<br>
Mercredi 30, 15h00 - 20h30 - 4 personnes<br>
Jeudi 31, 16h00 - 20h30 - 1 personne<br>
Vendredi 1, 15h00 - 20h30 - 1 personne<br>
Samedi 2, 15h00 - 19h30 - 1 personne<br></span>

											<table class="table table-condensed dataTable no-footer" id="choixdates">
												<tr>
													<th>Date</th>
													<th class="text-center">11:00 - 15:00<br>(<?= sprintf(t('rdv_texte'), '10:30') ?>)</th>
													<th class="text-center">15:00 - 20:00<br>(<?= sprintf(t('rdv_texte'), '14:30') ?>)</th>
												</tr>

												<tr>
													<td>30.10.2024</td>
													<td class="text-center"><input type="checkbox" value="1" name="jour1_am" id="jour1_am"/></td>
													<td class="text-center"><input type="checkbox" value="1" name="jour1_pm" id="jour1_pm"></td>
												</tr>
												<tr>
													<td>31.10.2024</td>
													<td class="text-center"><input type="checkbox" value="1" name="jour2_am" id="jour2_am"/></td>
													<td class="text-center"><input type="checkbox" value="1" name="jour2_pm" id="jour2_pm"></td>
												</tr>
												<tr>
													<td>01.11.2024</td>
													<td class="text-center"><input type="checkbox" value="1" name="jour3_am" id="jour3_am"/></td>
													<td class="text-center"><input type="checkbox" value="1" name="jour3_pm" id="jour3_pm"></td>
												</tr>
												<tr>
													<td>02.11.2024</td>
													<td class="text-center"><input type="checkbox" value="1" name="jour4_am" id="jour4_am"/></td>
													<td class="text-center"><input type="checkbox" value="1" name="jour4_pm" id="jour4_pm"></td>
												</tr>
											</table>

										</div>
									</div>
								</div>

								<div class="row ">
									<div class="col-md-12">
										<div class="form-group form-group-default">
											<label><?= t('remarques') ?>
												<input type="text" class="form-control" name="taxateur[note]" maxlength="250"
													   value="<?= $taxateur ? $taxateur->note : set_value('taxateur[note]'); ?>">
											</label>
										</div>
									</div>
								</div>
							</div>
							<?php /*
                            <div class="row ">
                                <div class="col-md-12">
                                    <label><?= t('frais_titre') ?></label>
                                    <div class="checkbox">
                                        <input type="checkbox" value="1" name="taxateur[frais_jour]" id="frais_jour">
                                        <label for="frais_jour"><?= t('frais_jour') ?></label><br>
                                        <input type="checkbox" value="1" name="taxateur[frais_deplacement]" id="frais_deplacement">
                                        <label for="frais_deplacement"><?= t('frais_deplacement') ?></label><br>
                                        <input type="checkbox" value="1" name="taxateur[frais_non]" id="frais_non">
                                        <label for="frais_non"><?= t('frais_non') ?></label>
                                    </div>

                                </div>
                            </div>
 */ ?>
						</div>

						<div class="clearfix"></div>
						<?= validation_errors(); ?>
						<button class="btn btn-primary" type="submit" id="validate"><?= t('valider_inscription') ?></button>
						<?= form_close() ?>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php ob_start(); ?>
<?php if(false): //set true si inscription taxateur conso fermées?>
	<script type="application/javascript">
		$(document).ready(function () {
			$('#conso').change(function () {
				if (this.checked) {
					$('#complet').show();
					$('#part2').hide();
					$('#jour1_am').prop('disabled', true);
					$('#jour1_pm').prop('disabled', true);
					$('#jour2_am').prop('disabled', true);
					$('#jour2_pm').prop('disabled', true);
					$('#jour3_am').prop('disabled', true);
					$('#jour3_pm').prop('disabled', true);
					$('#jour4_am').prop('disabled', true);
					$('#jour4_pm').prop('disabled', true);
					$('#jour4_pm').prop('disabled', true);
					$('#validate').prop('disabled', true);

				}
			});
			$('#pro').change(function () {
				if (this.checked) {
					$('#complet').hide();
					$('#part2').show();
					$('#jour1_am').prop('disabled', true);
					$('#jour1_pm').prop('disabled', false);
					$('#jour2_am').prop('disabled', false);
					$('#jour2_pm').prop('disabled', false);
					$('#jour3_am').prop('disabled', false);
					$('#jour3_pm').prop('disabled', false);
					$('#jour4_am').prop('disabled', false);
					$('#jour4_pm').prop('disabled', false);
					$('#validate').prop('disabled', false);

				}
			});
		});
	</script>
<?php endif;?>
<?php
SBTInclude::js(ob_get_clean());
$this->load->view('base/footer');
