<?php $this->load->view('base/header');

?>
	<!-- START CONTAINER FLUID -->
	<div class=" no-padding    container-fixed-lg bg-white">
		<div class="container">
			<!-- START BREADCRUMB -->
			<div class="row"><h1><?= t('inscription_taxateur'); ?></h1></div>
			<div class="row">
				<div class="col-xl-12 col-lg-12 ">
					<!-- START card -->
					<div class="card card-transparent">
						<div class="card-block">
							<?php echo form_open(); ?>
							<div class="row ">
								<div class="col-md-12">
									<label><?= t('langue') ?></label>
									<div class="radio radio-success">
										<input type="radio" value="fr" name="taxateur[langue]" id="fr" required <?= $taxateur->langue == 'fr' ? 'checked' : '' ?>>
										<label for="fr">Français</label>
										<input type="radio" value="de" name="taxateur[langue]" id="de" required <?= $taxateur->langue == 'de' ? 'checked' : '' ?>>
										<label for="de">Deutsch</label>
									</div>

								</div>
							</div>
							<div class="row ">
								<div class="col-md-12">
									<label><?= t('type_taxateur') ?></label>
									<div class="radio radio-success">
										<input type="radio" value="conso" name="taxateur[type]" id="conso" required <?= $taxateur->type == 'conso' ? 'checked' : '' ?>>
										<label for="conso"><?= t('consommateur') ?></label>
										<input type="radio" value="pro" name="taxateur[type]" id="pro" required <?= $taxateur->type == 'pro' ? 'checked' : '' ?>>
										<label for="pro"><?= t('professionnel') ?></label>
									</div>

								</div>
							</div>
							<div class="row ">
								<div class="col-md-12">
									<label><?= t('civilite') ?></label>
									<div class="radio radio-success">
										<input type="radio" value="F" name="taxateur[civilite]" id="F" required <?= $taxateur->civilite == 'F' ? 'checked' : '' ?>>
										<label for="F"><?= t('madame') ?></label>
										<input type="radio" value="M" name="taxateur[civilite]" id="M" required <?= $taxateur->civilite == 'M' ? 'checked' : '' ?>>
										<label for="M"><?= t('monsieur') ?></label>
									</div>

								</div>
							</div>
							<div class="row ">
								<div class="col-md-6">
									<div class="form-group form-group-default required">
										<label><?= t('nom') ?></label>
										<input type="text" class="form-control" name="taxateur[nom]" required
											   value="<?= $taxateur ? $taxateur->nom : set_value('taxateur[nom]'); ?>">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group form-group-default required">
										<label><?= t('prenom') ?></label>
										<input type="text" class="form-control" name="taxateur[prenom]" required
											   value="<?= $taxateur ? $taxateur->prenom : set_value('taxateur[prenom]'); ?>">
									</div>
								</div>
							</div>
							<div class="row ">
								<div class="col-md-12">
									<div class="form-group form-group-default">
										<label><?= t('entreprise') ?></label>
										<input type="text" class="form-control" name="taxateur[entreprise]"
											   value="<?= $taxateur ? $taxateur->entreprise : set_value('taxateur[entreprise]'); ?>">
									</div>
								</div>
							</div>
							<div class="row ">
								<div class="col-md-12">
									<div class="form-group form-group-default required">
										<label><?= t('rue') ?></label>
										<input type="text" class="form-control" name="taxateur[rue]" required
											   value="<?= $taxateur ? $taxateur->rue : set_value('taxateur[rue]'); ?>">
									</div>
								</div>
							</div>
							<div class="row ">
								<div class="col-sm-4">
									<div class="form-group form-group-default required">
										<label><?= t('npa') ?></label>
										<input type="text" class="form-control" name="taxateur[npa]" required
											   value="<?= $taxateur ? $taxateur->npa : set_value('taxateur[npa]'); ?>">
									</div>
								</div>
								<div class="col-sm-8">
									<div class="form-group form-group-default required">
										<label><?= t('ville') ?></label>
										<input type="text" class="form-control" name="taxateur[lieu]" required
											   value="<?= $taxateur ? $taxateur->lieu : set_value('taxateur[lieu]'); ?>">
									</div>
								</div>
							</div>
							<div class="row ">
								<div class="col-sm-12">
									<div class="form-group form-group-default required">
										<label>Canton</label>
										<?= form_dropdown('taxateur[canton]', SBTParticipant::$cantons, $taxateur ? $taxateur->canton : set_value('taxateur[canton]'), 'id="canton" class="full-width" data-init-plugin="select2" required'); ?>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group form-group-default required">
										<label><?= t('email') ?></label>
										<input type="email" class="form-control" name="taxateur[email]" required
											   value="<?= $taxateur ? $taxateur->email : set_value('taxateur[email]'); ?>">
									</div>
								</div>
							</div>
							<div class="row ">
								<div class="col-md-12">
									<div class="form-group form-group-default required">
										<label><?= t('telephone') ?></label>
										<input type="phone" class="form-control" name="taxateur[telephone]" id="telephone" required
											   value="<?= $taxateur ? $taxateur->telephone : set_value('taxateur[telephone]'); ?>">
									</div>
								</div>
							</div>
							<div class="row ">
								<div class="col-md-12">
									<div class="form-group form-group-default required">
										<label><?= t('disponibilites') ?></label>
										<span class="help"><?= t('infos_disponibilite') ?></span>
										<table class="table table-condensed dataTable no-footer">
											<tr>
												<th>Date</th>
												<th class="text-center">11:00 - 15:00<br>rendez-vous à 10h30 pour l’instruction</th>
												<th class="text-center">15:00 - 20:00<br>rendez-vous à 14h30 pour l’instruction</th>
											</tr>
											<tr>
												<td>30.10.2024</td>
												<td class="text-center">
													<input type="hidden" name="taxateur[jour1_am]" value="0"/>
													<input type="checkbox" value="1" name="taxateur[jour1_am]" id="jour1_am" <?= $taxateur->jour1_am ? 'checked' : '' ?>/></td>
												<td class="text-center">
													<input type="hidden" name="taxateur[jour1_pm]" value="0"/>
													<input type="checkbox" value="1" name="taxateur[jour1_pm]" id="jour1_pm" <?= $taxateur->jour1_pm ? 'checked' : '' ?>></td>
											</tr>
											<tr>
												<td>31.10.2024</td>
												<td class="text-center">
													<input type="hidden" name="taxateur[jour2_am]" value="0"/>
													<input type="checkbox" value="1" name="taxateur[jour2_am]" id="jour2_am" <?= $taxateur->jour2_am ? 'checked' : '' ?>/></td>
												<td class="text-center">
													<input type="hidden" name="taxateur[jour2_pm]" value="0"/>
													<input type="checkbox" value="1" name="taxateur[jour2_pm]" id="jour2_pm" <?= $taxateur->jour2_pm ? 'checked' : '' ?>></td>
											</tr>
											<tr>
												<td>01.11.2024</td>
												<td class="text-center">
													<input type="hidden" name="taxateur[jour3_am]" value="0"/>
													<input type="checkbox" value="1" name="taxateur[jour3_am]" id="jour3_am" <?= $taxateur->jour3_am ? 'checked' : '' ?>/></td>
												<td class="text-center">
													<input type="hidden" name="taxateur[jour3_pm]" value="0"/>
													<input type="checkbox" value="1" name="taxateur[jour3_pm]" id="jour3_pm" <?= $taxateur->jour3_pm ? 'checked' : '' ?>></td>
											</tr>
											<tr>
												<td>02.11.2024</td>
												<td class="text-center">
													<input type="hidden" name="taxateur[jour4_am]" value="0"/>
													<input type="checkbox" value="1" name="taxateur[jour4_am]" id="jour4_am" <?= $taxateur->jour4_am ? 'checked' : '' ?>/></td>
												<td class="text-center">
													<input type="hidden" name="taxateur[jour4_pm]" value="0"/>
													<input type="checkbox" value="1" name="taxateur[jour4_pm]" id="jour4_pm" <?= $taxateur->jour4_pm ? 'checked' : '' ?>>
												</td>
											</tr>
										</table>

									</div>
								</div>
							</div>
							<?php /*
                            <div class="row ">
                                <div class="col-md-12">
                                    <label><?= t('frais_titre') ?></label>
                                    <div class="checkbox">

                                        <input type="hidden" name="taxateur[frais_jour]" value="0"/>
                                        <input type="checkbox" value="1" name="taxateur[frais_jour]" id="frais_jour" <?= $taxateur->frais_jour ? 'checked' : '' ?>>
                                        <label for="frais_jour"><?= t('frais_jour') ?></label><br>

                                        <input type="hidden" name="taxateur[frais_deplacement]" value="0"/>
                                        <input type="checkbox" value="1" name="taxateur[frais_deplacement]" id="frais_deplacement" <?= $taxateur->frais_deplacement ? 'checked' : '' ?>>
                                        <label for="frais_deplacement"><?= t('frais_deplacement') ?></label><br>

                                        <input type="hidden" name="taxateur[frais_non]" value="0"/>
                                        <input type="checkbox" value="1" name="taxateur[frais_non]" id="frais_non" <?= $taxateur->frais_non ? 'checked' : '' ?>>
                                        <label for="frais_non"><?= t('frais_non') ?></label>
                                    </div>

                                </div>
                            </div>
 */ ?>
						</div>

						<div class="clearfix"></div>
						<?= validation_errors(); ?>
						<button class="btn btn-primary" type="submit"><?= t('valider_inscription') ?></button>
						<?= form_close() ?>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php $this->load->view('base/footer');
