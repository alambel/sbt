<?php $this->load->view('base/header'); ?>
    <div class="container sm-padding-10 p-t-20 p-l-0 p-r-0">
        <div class="row">
            <div class="col-lg-12 col-sm-12">
                <div class=" card widget-loader-circle todolist-widget pending-projects-widget">
                    <div class="card-header ">
                        <div class="card-title"><h2>Médailles</h2></div>
                    </div>
                    <div class="card-block">
                        <table class="table table-striped table-responsive" id="medailles">
                            <thead>
                            <tr>
                                <th>NO</th>
                                <th>Nom</th>
                                <th>Catégorie</th>
                                <th>Commentaire</th>
                                <th>Auteur</th>
                                <th>Taxé le</th>
                                <th>Validé</th>
                                <th>Notes</th>
                                <th>Moyenne</th>
                                <th>Médaille</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if ($produits):
                                foreach ($produits as $p):
                                    ?>
                                    <tr class="gradeX">
                                        <td><?= $p->no; ?></td>
                                        <td><?= $p->nom; ?></td>
                                        <td><?= SBTProduit::$categories[$p->categorie]; ?></td>
                                        <td><?= $p->commentaire; ?></td>
                                        <td><?= $p->comm_auteur; ?></td>
                                        <td><?= $p->comm_date ? date('d.m.Y H:i', $p->comm_date) : null; ?></td>
                                        <td><?= $p->comm_validated ? 'oui' : '<span style="color:red;">non</span>'; ?></td>
                                        <td><?= $p->notes ?></td>
                                        <td><?= $p->moyenne ? number_format($p->moyenne, 2) : null; ?></td>
                                        <td><?= $p->medaille; ?></td>
                                    </tr>
                                <?php endforeach;
                            endif;
                            ?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
<?php ob_start(); ?>
    <script type="application/javascript">
        $(document).ready(function () {
            $('#medailles').DataTable({
                paging: false,
                "order": [[8, "desc"]],
                dom: 'Bfrtip',
                buttons: [
                    'copyHtml5',
                    'excelHtml5'
                ]
            });
        });

    </script>
<?php
SBTInclude::js(ob_get_clean());

$this->load->view('base/footer');