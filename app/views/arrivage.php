<?php $this->load->view('base/header'); ?>
    <div class="container">
        <div class="card">
            <div class="card-header ">
                <div class="card-title">
                    <h1>Arrivage</h1>
                </div>
            </div>
            <div class="card-block">
                <?= form_open() ?>
                <?php if ($produit): ?>
                    <p><b>NO : </b> <?= $produit->no ?></p>
                    <p><b><?=t('nom_produit')?> : </b><?= $produit->nom ?></p>
                    <p><b>Jubilé : </b><?= $produit->jubile ? 'oui' : 'non' ?></p>
                    <div class="form-group form-group-default required">
                        <label><?= t('categorie') ?></label>
                        <?= form_dropdown('produit[categorie]', SBTProduit::$categories, $produit->categorie, array('placeholder' => t('categorie'), 'class' => 'form-control ')); ?>
                    </div>
                    <div class="form-group form-group-default required">
                        <label><?= t('composition_title') ?></label>
                        <?= form_textarea(array('name' => 'produit[composition]', 'value' => $produit->composition, 'class' => 'form-control', 'style' => 'height:100px;')) ?>
                    </div>
                    <div class="form-group form-group-default required">
                        <label><?= t('arguments_de_vente') ?></label>
                        <?= form_textarea(array('name' => 'produit[arguments]', 'value' => $produit->arguments, 'class' => 'form-control', 'style' => 'height:100px;')) ?>
                    </div>
                    <?php echo form_hidden('no', $produit->no); ?>
                    <button class="btn btn-primary pull-right" type="submit"><?=t('marquer_arrive')?></button>
                <?php else: ?>
                    <p><?=t('entrer_num_produit')?></p>
                    <?= form_input(array('name' => 'no', 'class' => 'form-control', 'placeholder' => t('entrer_num_produit'))); ?>

                    <button class="btn btn-primary" type="submit"><?=t('selectionner')?></button>
                <?php endif; ?>
                <?= form_close(); ?>
            </div>
        </div>

    </div>

<?php $this->load->view('base/footer');
