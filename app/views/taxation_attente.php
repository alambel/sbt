<?php $this->load->view('base/header_taxation');
$participant = SBTParticipant::getCurrent();

?>
	<div class="container">
		<div class="card">
			<div class="card-header ">
				<div class="card-title">
					<h1><?= t('taxation') ?></h1>
					<h4><?= t('table') ?> <?= $participant->table ?> - <?= t('taxateur') ?> <?= $participant->taxateur ?></h4>
				</div>
			</div>
			<div class="card-block">
				<?php
				$produitId = SBTTable::current($participant->table);
				if ($produitId):
					$produit = SBTProduit::get($produitId);
					?>
					<p><?= t('taxation_en_cours') ?></p>
					<p><b>NO</b> : <?= $produit->no; ?></p>
					<p><b><?= t('categorie') ?></b>: <?= SBTProduit::$categories[$produit->categorie]; ?></p>
					<p><b><?= t('composition') ?></b>: <?= $produit->composition ?></p>
					<p><b><?= t('arguments_de_vente') ?></b>: <?= $produit->arguments ?></p>
					<?php if (SBTTaxation::done($produitId, $participant->taxateur, $participant->table)):
					$taxation = SBTTaxation::get($produitId, $participant->taxateur);
					?><br><br>
					<p style="color:orange;"><?= t('vous_avez_fini_taxation') ?></p>
					<table class="col-sm-12 table-bordered table-striped">
						<tr>
							<th><?= t('critere') ?></th>
							<th><?= t('note') ?></th>
							<th><?= t('commentaire') ?></th>
						</tr>
						<?php
						if ($produit->categorie == 'tartiner') {
							$criteres = array('aspect' => 'couleur', 'odeur' => 'gout', 'texture' => 'texture', 'bouche' => 'bouche');
						} else {
							if ($participant->taxateur < 4) {
								$criteres = array('aspect' => 'aspect', 'forme' => 'forme', 'surface' => 'surface', 'texture' => 'texture', 'bouche' => 'bouche', 'odeur' => 'odeur');
							} else {
								$criteres = array('aspect' => 'aspect', 'bouche' => 'bouche', 'odeur' => 'odeur');
							}
						}
						foreach ($criteres as $critere => $label): ?>
							<tr>
								<td><?= t($label) ?></td>
								<td><?= $taxation->{$critere} ?></td>
								<td><?= $taxation->{$critere . '_commentaire'} ?></td>
							</tr>
						<?php endforeach; ?>
					</table>
				<?php else: ?>
					<?php redirect('taxation/produit/' . $produitId); ?>
				<?php endif; ?>
					<br><br>
				<?php endif; ?>

				<p><?= t('attente_chef_table') ?></p>
				<a href="<?= current_url() ?>" class="btn"><?= t('recharger_page') ?></a>
			</div>
		</div>

	</div>

<?php
ob_start() ?>
	<script type="application/javascript">
		$(document).ready(function () {
			setTimeout(function () {
				window.location.reload();
			}, 25000);
		});
		(function checker() {
			$.get('<?=site_url('taxation/status')?>', function (data) {
				// Now that we've completed the request schedule the next one.
				console.log(data);
				if (data == 'reload') {
					window.location.reload();
				} else {
					setTimeout(checker, 6000);
				}
			});
		})();
	</script>
<?php
SBTInclude::js(ob_get_clean());

$this->load->view('base/footer_taxation'); ?>
