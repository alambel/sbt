<?php $this->load->view('base/header'); ?>


    <div class="container sm-padding-10 p-t-20 p-l-0 p-r-0">

        <div class="row">
            <div class="col-lg-12 col-sm-12">
                <div class=" card widget-loader-circle todolist-widget pending-projects-widget">
                    <div class="card-header ">
                        <div class="card-title"><h2>Pre-Inscriptions Taxateurs</h2></div>
                    </div>
                    <div class="card-block">
                        <div class="row m-b-10">
                            <button id="sendEmail" class="btn btn-success btn-cons " type="button"><i class="fa fa-envelope"></i> <span id="sendMailTitle" class="bold">Envoyer 0 email</span>
                            </button>
                            <a href="<?= site_url('admin/taxateurs_prefill/csv') ?>" class="btn btn-white">Export CSV</a>
                        </div>
                        <?= form_open(); ?>
                        <?= form_hidden('action', ''); ?>
                        <table id="participants" class="table table-hover" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th><?= form_checkbox('', null, false, 'id="checkall"'); ?></th>

                                <th>Contact</th>
                                <th>Entreprise</th>
                                <th>Adresse</th>
                                <th>Canton</th>
                                <th>Email</th>
                                <th>Lien d'activation</th>
                                <th>Mail preinscription</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($taxateurs as $p): ?>
                                <tr>
                                    <td><?= form_checkbox('ids[]', $p->id, false, 'class="rowcheckbox"'); ?></td>

                                    <td><?= $p->nom . ' ' . $p->prenom; ?><br><?= $p->telephone ? '<i class="fa fa-phone"></i> ' . $p->telephone : null; ?></td>
                                    <td><?= $p->entreprise; ?></td>
                                    <td><?= $p->rue . '<br>' . $p->npa . ' ' . $p->lieu ?></td>
                                    <td><?= $p->canton; ?></td>
                                    <td><?= mailto($p->email, $p->email); ?></td>
                                    <td><?= anchor($p->getPreinscriptionUrl()) ?></td>
                                    <td><?= $p->mailpreinscription ? 'oui' : 'non' ?></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php ob_start(); ?>
    <script type="application/javascript">
        var table;
        $(document).ready(function () {
            table = $('#participants').DataTable({
                select: "multi",
                stateSave: true,
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
            });
            $('.rowcheckbox').change(function () {
                console.log(table.rows('.selected').data().length);
                $('#sendMailTitle').html('Envoyer ' + $('input[name="ids[]"]:checked').length + ' emails');
            });
            $('#checkall').change(function () {
                console.log(this.checked);
                $('input[name="ids[]"]').prop('checked', this.checked);
                $('#sendMailTitle').html('Envoyer ' + $('input[name="ids[]"]:checked').length + ' emails');
            });
            $('#sendEmail').click(function () {
                $('input[name="action"]').val('sendmail');
                $('form').submit();
            });
        });
    </script>
<?php
SBTInclude::js(ob_get_clean());

$this->load->view('base/footer'); ?>