<?php $this->load->view('base/header'); ?>
	<div class="container sm-padding-10 p-t-20 p-l-0 p-r-0">
		<div class="row">
			<div class="col-lg-12 col-sm-12">
				<div class=" card widget-loader-circle todolist-widget pending-projects-widget">
					<div class="card-header ">
						<div class="card-title"><h2>Champions</h2></div>
					</div>
					<div class="card-block">

						<table class="table table-striped table-bordered table-hover responsive" id="champions">
							<thead>
							<tr>
								<th>Canton</th>
								<th>Entreprise</th>
								<th>Npa</th>
								<th>Localité</th>
								<th>Tél.</th>
								<th>Moyenne</th>
								<th>Produits</th>
							</tr>
							</thead>
							<tbody>
							<?php
							foreach ($champions as $c):
								?>
								<tr class="gradeX">
									<td><?php echo $c['participant']->canton; ?></td>
									<td><?php echo $c['participant']->entreprise; ?></td>
									<td><?php echo $c['participant']->npa; ?></td>
									<td><?php echo $c['participant']->lieu; ?></td>
									<td><?php echo $c['participant']->mobile ?: $c['participant']->telephone; ?></td>
									<td><?php echo number_format($c['moyenne'], 2); ?></td>
									<td><br>
										<?php foreach ($c['produits'] as $p): ?>
											<?= number_format($p->moyenne, 2); ?>: <?= $p->nom; ?> (<?= $p->no; ?>) - <?= $p->categorie ?><br>
										<?php endforeach; ?>
									</td>
								</tr>
							<?php endforeach; ?>
							</tbody>
						</table>

					</div>
				</div>
			</div>
		</div>
	</div>
<?php ob_start(); ?>
	<script type="application/javascript">
		$(document).ready(function () {
			$('#champions').DataTable({
				paging: false,
				"order": [[5, "desc"]],
				dom: 'Bfrtip',
				buttons: [
					'copyHtml5',
					'excelHtml5'
				]
			});
		});

	</script>
<?php
SBTInclude::js(ob_get_clean());

$this->load->view('base/footer');
