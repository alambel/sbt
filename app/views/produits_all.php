<?php $this->load->view('base/header'); ?>
<div class="container sm-padding-10 p-t-20 p-l-0 p-r-0">
	<div class="row">
		<div class="col-lg-12 col-sm-12">
			<div class=" card widget-loader-circle todolist-widget pending-projects-widget">
				<div class="card-header ">
					<div class="card-title"><h2>Produits</h2></div>
				</div>
				<div class="card-block">
					<?php
					$jours = [];
					foreach (SBTTaxation::$joursTaxation as $canton => $jour) {
						$jours[$jour][] = $canton;
					}

					ksort($jours);
					$parCanton = SBTProduit::getParCanton();
					$recus = SBTProduit::getRecuParCanton();
					$taxes = SBTProduit::getTaxesParCanton();
					$taxes = SBTProduit::getTaxesParCanton();
					$photos = SBTProduit::getPhotosParCanton();
					?>
					<div class="row">
						<?php foreach ($jours as $j => $cantons): ?>
							<div class="col-md-6 m-b-20">
								<h3><?= $j == 5 ? 'SBT Academy' : 'Jour '. $j ?></h3>
								<table class="table-bordered table-sm" style="width:100%">
									<tr>
										<th>Canton</th>
										<?php foreach ($cantons as $c): ?>
											<td><?= $c ?></td>
										<?php endforeach; ?>
										<th>Total</th>
									</tr>
									<tr>
										<th>Inscrits</th>
										<?php
										$total = 0;
										foreach ($cantons as $c):
											$nb = isset($parCanton[$c]) ? $parCanton[$c] : null;
											$total += $nb;
											?>
											<td><?= $nb; ?></td>
										<?php endforeach; ?>
										<th><?= $total ?></th>
									</tr>
									<tr>
										<th>Réceptionnés</th>
										<?php
										$totalRecu = 0;
										foreach ($cantons as $c):
											$nb = isset($recus[$c]) ? $recus[$c] : null;
											$totalRecu += $nb;
											?>
											<td><?= isset($recus[$c]) ? $recus[$c] : null; ?></td>
										<?php endforeach; ?>
										<th><?= $totalRecu ?></th>
									</tr>
									<tr>
										<th>Taxés</th>
										<?php
										$totalTaxe = 0;
										foreach ($cantons as $c):
											$nb = isset($taxes[$c]) ? $taxes[$c] : null;
											$totalTaxe += $nb;
											$nbRecus = isset($recus[$c]) ? $recus[$c] : null;
											?>
											<td style="color:<?= $nb < $nbRecus ? 'red' : 'green' ?>"><?= $nb ?></td>
										<?php endforeach; ?>
										<th style="color:<?= $totalTaxe < $totalRecu ? 'red' : 'green' ?>"><?= $totalTaxe ?></th>
									</tr>
									<tr>
										<th>Photos</th>
										<?php
										$total = 0;
										foreach ($cantons as $c):
											$nb = isset($photos[$c]) ? $photos[$c] : null;
											$total += $nb;
											?>
											<td><?= isset($photos[$c]) ? $photos[$c] : null; ?></td>
										<?php endforeach; ?>
										<th><?= $total ?></th>
									</tr>
								</table>
							</div>
						<?php endforeach; ?>
					</div>
					<?php $produitsCategorie = SBTProduit::getNbParCategories(); ?>
					<div class="row">
						<div class="col-md-6 m-b-20">
							<h3>Produits par catégorie</h3>
							<table class="table-bordered table-sm" style="width:200px">
								<?php foreach ($produitsCategorie as $cat => $cnt): ?>
									<tr>
										<th><?= $cat ?></th>
										<td><?= $cnt ?></td>
									</tr>
								<?php endforeach; ?>
							</table>
						</div>
						<div class="col-md-6 m-b-20">
							<?php if ($doublons): ?>
								<?= $doublons ?> taxations doubles
							<?php endif; ?>
						</div>
					</div>
					<table id="participants" class="table table-hover" cellspacing="0" width="100%">
						<thead>
						<tr>
							<th>id</th>
							<th>NO</th>
							<th>Entreprise</th>
							<th>Nom</th>
							<th>Catégorie</th>
							<th>Composition</th>
							<th>Arguments</th>
							<th>Jubilé</th>
							<th>Réceptionné le</th>
							<th>Nom</th>
							<th>Prenom</th>
							<th>Telephone</th>
							<th>Mobile</th>
							<th>Rue</th>
							<th>NPA</th>
							<th>Lieu</th>
							<th>Canton</th>
							<th>Email</th>
							<th>Nb Photos</th>
						</tr>
						</thead>
						<tbody>
						<?php
						if ($produits):
							foreach ($produits as $p):
								$entreprise = SBTParticipant::get($p->participant);
								if ($entreprise):
									?>
									<tr>
										<td><?= $p->id; ?></td>
										<td><?= $p->no ?: anchor('admin/generateNO/' . $p->id, 'générer NO'); ?></td>
										<td><?= anchor('entreprise/view/' . $entreprise->id, $entreprise); ?></td>
										<td><?= $p->nom ?></td>
										<td><?= $entreprise->lang == 'D' ? SBTProduit::$categoriesDE[$p->categorie] : SBTProduit::$categoriesFR[$p->categorie] ?></td>
										<td><?= $p->composition; ?></td>
										<td><?= $p->arguments; ?></td>
										<td><?= $p->jubile ? 'Jubilé':'-'; ?></td>
										<td><?= $p->arrivage; ?></td>
										<td><?= $entreprise->nom ?></td>
										<td><?= $entreprise->prenom; ?></td>
										<td><?= $entreprise->telephone ?></td>
										<td><?= $entreprise->mobile ?></td>
										<td><?= $entreprise->rue ?></td>
										<td><?= $entreprise->npa ?></td>
										<td><?= $entreprise->lieu ?></td>
										<td><?= $entreprise->canton; ?></td>
										<td><?= mailto($entreprise->email, $entreprise->email); ?></td>
										<td><?= $p->nbphotos ?></td>
									</tr>
								<?php endif;
							endforeach;
						endif; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<?php ob_start(); ?>
<script type="application/javascript">
	$(document).ready(function () {
		$('#participants').DataTable({
			stateSave: true,
			lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Tous"]],
			scrollX: true,
			dom: '<"top"Blif>rt<"bottom"p><"clear">',
			buttons: [
				{
					extend: 'excelHtml5',
					text: 'Exporter Excel',
				}
			]
		});
	});
</script>
<?php
SBTInclude::js(ob_get_clean());

$this->load->view('base/footer'); ?>
