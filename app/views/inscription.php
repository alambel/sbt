<?php $this->load->view('base/header'); ?>
	<!-- START CONTAINER FLUID -->
	<div class=" no-padding    container-fixed-lg bg-white">
		<div class="container">
			<!-- START BREADCRUMB -->
			<div class="row">
				<div class="col-xl-7 col-lg-6 ">
					<!-- START card -->
					<div class="card card-transparent">
						<div class="card-block">
							<?php
							echo validation_errors();
							echo form_open();
							echo form_hidden('id', $participant ? $participant->id : null) ?>
							<? $this->load->view('register_form'); ?>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group form-group-default required">
										<label><?= t('mot_de_passe') ?></label>
										<input type="password" class="form-control" name="password" required>
									</div>
								</div>
							</div>
							<p class="pull-left">
								<?= t('accepte_le') ?> <a href="<?= site_url('assets/SBT2024-Reglement_' . lang() . '.pdf') ?>" target="_blank"><?= t('reglement_conditions') ?></a>.
							</p>
							<div class="clearfix"></div>
							<?= validation_errors(); ?>
							<button class="btn btn-primary" type="submit"><?= t('valider_inscription') ?></button>
							<?= form_close() ?>
						</div>
					</div>
					<!-- END card -->
				</div>
				<div class="col-xl-5 col-lg-6">
					<!-- START card -->
					<div class="card card-transparent">
						<div class="card-header ">
							<div class="card-title"><?= t('inscription_entreprise'); ?></div>
						</div>
						<div class="card-block">
							<h3><?= t('formulaire_inscription'); ?></h3>
							<p><b><?= t('completer_jusquau'); ?></b></p>
							<p><?= t('inscription_subline'); ?></p>
							<br>
							<p class="small hint-text m-t-5"><?= t('vous_avez_des_questions'); ?></p>
							<a href="mailto:info@lepain.ch" class="btn btn-primary btn-cons"><?= t('contactez_nous'); ?></a>
						</div>
					</div>
					<!-- END card -->
				</div>
			</div>
		</div>
	</div>

<?php $this->load->view('base/footer');
