<?php $this->load->view('base/header'); ?>


    <div class="container sm-padding-10 p-t-20 p-l-0 p-r-0">

        <div class="row">
            <div class="col-lg-12 col-sm-12">
                <div class=" card widget-loader-circle todolist-widget pending-projects-widget">
                    <div class="card-header ">
                        <div class="card-title"><h2>Commentaires à valider</h2></div>
                    </div>
                    <div class="card-block">
                        <table class="table table-striped" id="commentaires">
                            <thead>
                            <tr>
                                <th>NO</th>
                                <th>Nom</th>
                                <!--                                <th>Catégorie</th>-->
                                <th>Langue</th>
                                <th>Commentaire</th>
<!--                                <th>Auteur</th>-->
                                <th>Commenté le</th>
                                <th>Validé</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if ($produits)
                                foreach ($produits as $p): ?>
                                    <tr class="gradeX">
                                        <td><?= $p->no; ?></td>
                                        <td><?= $p->nom; ?></td>
                                        <!--                                        <td>--><? //= SBTProduit::$categories[$p->categorie]; ?><!--</td>-->
                                        <td><?= $p->lang; ?></td>
                                        <td>
                                            <div class="commentLabel" id="label-<?= $p->id; ?>"><?= $p->commentaire; ?></div>
                                            <div class="input commentInput <?= $p->commentaire ? 'hidenow' : null; ?>" id="input-<?= $p->id; ?>">
                                                <textarea id="value-<?= $p->id; ?>" maxlength="250" style="width:100%;height:60px;"><?= $p->commentaire; ?></textarea>
                                            </div>
                                        </td>
<!--                                        <td></td>-->
                                        <td><?= $p->comm_date ? date('Y-m-d H:i', $p->comm_date) : null; ?><br><?= $p->comm_nom; ?></td>
                                        <td><input class="commentValidated" type="checkbox" name="validated"
                                                   id="validated-<?= $p->id; ?>" <?= $p->comm_validated ? 'checked="checked"' : null; ?> value="1"/></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                        <a href="<?= site_url('admin/commentaires') ?>" class="btn pull-right btn-primary">Recharger</a>
                    </div>
                </div>
            </div>
        </div>

    </div>

<?php ob_start(); ?>
    <script type="application/javascript">
        $(document).ready(function () {
            var myTable = $('#commentaires').DataTable({
                paging: false,
                "order": [[4, "desc"]],
            });

            $('.commentLabel').each(function (i, e) {
                $(e).click(function () {
                    $(this).hide();
                    $('#' + this.id.replace('label', 'input')).show();

                });
            });
            $('.commentInput').focusout(function () {
                var id = this.id.replace('input-', '');
                var value = $('#' + this.id.replace('input', 'value')).val();
                if (value) {
                    $.post('<?php echo site_url('admin/commentaire_post'); ?>', {id: id, value: value}).done(function () {
                        $('#label-' + id).html(value);
                        $('#input-' + id).hide();
                        $('#label-' + id).show();
                    }).fail(function () {
                        alert("Une erreur est survenue, veuillez réessayer");
                    });
                }
            });
            $('.hidenow').each(function (i, e) {
                $(this).hide();
            });

            $('.commentValidated').change(function () {
                var id = this.id.replace('validated-', '');
                var checked = $(this).is(':checked');
                $.post('<?php echo site_url('admin/commentaire_validate'); ?>', {id: id, value: checked ? 1 : 0}).done(function () {
                }).fail(function () {
                    $(this).attr("checked", !checked);
                });
            });

        });
    </script>
<?php
SBTInclude::js(ob_get_clean());

$this->load->view('base/footer');