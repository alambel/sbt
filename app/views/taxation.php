<?php $this->load->view('base/header_taxation');
$participant = SBTParticipant::getCurrent();
?>
    <div class="container" style="max-width:100%">
        <div class="card">
            <div class="card-header ">
                <p class="pull-right">Table <?= $participant->table; ?>, taxateur <?= $participant->taxateur ?></p>
                <div class="card-title">
                    <h1><?= $produit->nom ?> (NO: <?= $produit->no ?>)</h1>
                </div>
            </div>
            <div class="card-block">
                <p><b><?= t('categorie') ?></b>: <?= SBTProduit::$categories[$produit->categorie]; ?></p>
                <p><b><?= t('composition') ?></b>: <?= $produit->composition ?></p>
                <p><b><?= t('arguments_de_vente') ?></b>: <?= $produit->arguments ?></p>
                <?php echo form_open();
                echo form_hidden('no', $produit->no); ?>
                <?php if ($produit->categorie == 'tartiner'): ?>
                    <?php scale('aspect', t('couleur')); ?>
                    <hr>
                    <?php scale('odeur', t('gout')); ?>
                    <hr>
                    <?php scale('texture'); ?>
                    <hr>
                    <?php scale('bouche'); ?>
                <?php else: ?>
                    <h3><?= t('caracteristiques_exterieures') ?></h3>
                    <?php scale('aspect'); ?>
                    <hr>
                    <?php if ($participant->taxateur < 4): ?>
                        <?php scale('forme'); ?>
                        <hr>
                        <?php scale('surface'); ?>
                        <hr>
                    <?php endif; ?>
                    <h3><?= t('caracteristiques_interieures') ?></h3>
                    <?php if ($participant->taxateur < 4): ?>
                        <?php scale('texture'); ?>
                        <hr>
                    <?php endif; ?>
                    <?php scale('bouche'); ?>
                    <hr>
                    <?php scale('odeur'); ?>
                <?php endif; ?>
                <button class="btn btn-primary pull-right" type="submit"><?= t('sauver') ?></button>
                <?= form_close() ?>
            </div>
        </div>

    </div>

<?php


ob_start() ?>
    <script>
		function checkscore(e) {
			var note = $(e);
			var commentId = '#' + note.attr('id').split('-')[0] + '-commentaire';
			$(commentId).prop('required', note.val() <= 8);
		}
    </script>
<?php
SBTInclude::js(ob_get_clean());

$this->load->view('base/footer_taxation'); ?>
