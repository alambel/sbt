<?php $this->load->view('base/header'); ?>
    <div class="container sm-padding-10 p-t-20 p-l-0 p-r-0">

        <div class="row">
            <div class="col-lg-12 col-sm-12">
                <div class=" card widget-loader-circle todolist-widget pending-projects-widget">
                    <div class="card-header ">
                        <div class="card-title"><h2>Résultats</h2></div>
                    </div>
                    <div class="card-block">
                        <?/*
                        <div class="pull-right">
                            <a href="<?php echo site_url('admin/recalcul_resultats'); ?>" class="btn">Tout recalculer</a>
                        </div>
*/?>
                        <div class="row m-b-20">
                            <a href="<?php echo site_url('admin/resultats'); ?>" class="btn <?= $categorie == null ? 'btn-primary' : null ?>">Tous les produits</a>
                            <?php foreach (SBTProduit::$categories as $k => $c):
                                if ($k):?>
                                    <a href="<?= site_url('admin/resultats?c=' . $k); ?>" class="btn <?= $categorie == $k ? 'btn-primary' : null ?>"><?= $c ?></a>
                                <?php endif; endforeach; ?>
                            <a href="<?php echo site_url('admin/resultats?c=0'); ?>" class="btn <?= $categorie == "0" ? 'btn-primary' : null ?>">sans catégorie</a>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-content">
                                        <table class="table table-striped table-hover responsive" id="commentaires">
                                            <thead>
                                            <tr>
                                                <th>NO</th>
                                                <th>Nom</th>
                                                <th>Moyenne</th>
                                                <th>Entreprise</th>
                                                <th>Canton</th>
                                                <th>Catégorie</th>
                                                <th>Commentaire</th>
                                                <th>Taxé le</th>
                                                <th>Table</th>
                                                <th>Médaille</th>
                                                <th>Validé</th>
                                                <th>Notes</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            if ($produits):
                                                foreach ($produits as $p):
                                                    $participant = SBTParticipant::get($p->participant);
                                                    ?>
                                                    <tr>
                                                        <td><?= $p->no; ?></td>
                                                        <td><?= $p->nom; ?></td>
                                                        <td><?= $p->moyenne ? number_format($p->moyenne, 3) : null; ?></td>
                                                        <td><?= $participant->entreprise; ?></td>
                                                        <td><?= $participant->canton; ?></td>
                                                        <td><?= SBTProduit::$categories[$p->categorie]; ?></td>
                                                        <td><?= $p->commentaire; ?></td>
                                                        <td><?= $p->comm_date ? date('d.m.Y H:i', $p->comm_date) : null; ?></td>
                                                        <td><?= $p->table; ?></td>
                                                        <td><?= $p->medaille; ?></td>
                                                        <td><?php echo $p->comm_validated ? 'oui' : null; ?></td>
                                                        <td><?php echo $p->notes ?></td>
                                                    </tr>
                                                <?php endforeach;
                                            endif;
                                            ?>
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


<?php ob_start(); ?>
    <script type="application/javascript">
        $(document).ready(function () {
            $('#commentaires').DataTable({
                paging: false,
                "order": [[2, "desc"]],
                dom: 'Bfrtip',
                buttons: [
                    'copyHtml5',
                    'excelHtml5'
                ]
            });
        });

    </script>
<?php
SBTInclude::js(ob_get_clean());

$this->load->view('base/footer');
