<?php $this->load->view('base/header'); ?>

    <!-- START CONTAINER FLUID -->

    <div class="container container-fixed-lg bg-white">
        <!-- START BREADCRUMB -->
        <div class="row">
            <div class="col-xl-12 col-lg-12 ">

                <!-- START card -->
                <div class="card card-transparent">
                    <div class="card-header ">
                        <div class="card-title"><h1><?= t('infos_entreprise') ?></h1></div>
                    </div>
                    <div class="card-block">
                        <?= validation_errors(); ?>
                        <?= form_open(); ?>

                        <? $this->load->view('register_form'); ?>
                        <?php if (SBTParticipant::isAdmin()): ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group form-group-default ">
                                        <label><?= t('mot_de_passe') ?></label>
                                        <input type="text" class="form-control" name="password">
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                        <p class="pull-left">
                            <?= t('accepte_le') ?> <a href="<?= site_url('assets/SBT2024-Reglement_' . lang() . '.pdf') ?>" target="_blank"><?= t('reglement_conditions') ?></a>.
                        </p>
                        <div class="clearfix"></div>
                        <?= validation_errors(); ?>
                        <a href="<?= site_url('entreprise/view/' . $participant->id) ?>" class="btn btn-white"><?= t('retour') ?></a>
                        <button class="btn btn-primary pull-right" type="submit"><?= t('sauver') ?></button>
                        <?= form_close() ?>
                    </div>
                </div>
                <!-- END card -->
            </div>
        </div>
    </div>


<?php

$this->load->view('base/footer');
