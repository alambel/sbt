<?php $this->load->view('base/header'); ?>
    <div class="container">
        <div class="card">
            <div class="card-header ">
                <p class="pull-right"><?=t('table')?> <?=
                    $participant->table; ?></p>
                <div class="card-title">
                    <h1><?=t('demarrer_taxation')?></h1>
                </div>
            </div>
            <div class="card-block">

                <?php if ($produit): ?>
                    <h2><?= $produit->nom ?> (NO: <?= $produit->no ?>)</h2>
                    <p><b><?=t('categorie')?></b>: <?= SBTProduit::$categories[$produit->categorie]; ?></p>
                    <p><b><?=t('composition')?></b>: <?= $produit->composition ?></p>
                    <p><b><?=t('arguments')?></b>: <?= $produit->arguments ?></p>
                    <?php echo form_open();
                    echo form_hidden('no', $produit->no);
                    echo form_hidden('done', 1); ?>

                    <a href="<?= site_url('taxation/canceltable') ?>" class="btn btn-white"><?=t('annuler_taxation')?></a>
                <?php else: ?>
                    <p><?=t('entrer_num_produit')?></p>
                    <?php echo form_open();?>


                    <?= form_input(array('name' => 'no', 'class' => 'form-control','placeholder'=>t('entrer_num_produit'))); ?>

                    <button class="btn btn-primary" type="submit"><?=t('selectionner')?></button>
                    <?= form_close(); ?>
                <?php endif; ?>
            </div>
        </div>

    </div>

<?php $this->load->view('base/footer');