<?php
/**
 * @var $p SBTParticipant
 */

$fmt = new NumberFormatter( 'fr_CH', NumberFormatter::CURRENCY );
?>

<div><img src="<?= base_url('img/entete.jpg'); ?>" alt="Logo" width="100%"/></div>
<br>
<table style="border:none; width:100%">
	<tr>

		<td style="width:70%"></td>
		<td>
			<?= $p->entreprise ?><br>
			<?= $p->nom ?> <?= $p->prenom ?><br>
			<?= $p->rue ?><br>
			<?= $p->npa ?> <?= $p->lieu ?>
		</td>
	</tr>
</table>
<br>
<div>
	<small>CHE-107.875.247 TVA<br>
		Réf. : co
	</small>
</div>
<div style="text-align: right">
	Bulle, <?= date('d.m.Y') ?>
</div>


<h3>Facture F<?= sprintf('%04d', $p->id) ?></h3>
<p>Mesdames, Messieurs,<br>Chers Collègues,</p>
<p>Nous avons l'avantage de vous confirmer votre participation au Swiss Bakery Trophy 2024 et de vous remettre ci-joint notre facture pour le règlement de la finance de participation
	de vos produits soumis à taxation, soit:</p>
<br>
<table style="width:100%; border-collapse: collapse;  border: 1px solid black;" cellpadding="8" border="1">
	<thead>
	<tr>
		<th style="text-align: left">Nombre</th>
		<th style="text-align: left">Articles</th>
		<th style="text-align: left"></th>
		<th style="text-align: left">Total</th>
	</tr>
	</thead>
	<tbody>
	<tr>
		<td style="text-align: center"><?= $p->getNbProduits() ?></td>
		<td>Produits soumis à taxation du Swiss Bakery Trophy 2024</td>
		<td>Fr.</td>
		<td style="text-align: right"><?= $fmt->formatCurrency($p->getTotal(), "CHF");			?></td>
	</tr>
	<tr>
		<td></td>
		<td style="text-align: right">+ TVA 8.1 %</td>
		<td>Fr.</td>
		<td style="text-align: right"><?= $fmt->formatCurrency($p->getTotal() * 0.081,'CHF') ?></td>
	</tr>
	<tr>
		<td></td>
		<td style="text-align: right"><b>Total</b></td>
		<td>Fr.</td>
		<td style="text-align: right"><b><?= $fmt->formatCurrency($p->getTotal() * 1.081,'CHF') ?></b></td>
	</tr>
	</tbody>
</table>
<br>
<p><b>Conditions de paiement</b> : conformément aux conditions de participation, les frais sont à payer avant le 15 novembre 2024 sur le compte IBAN CH70 0076 8250 1157 5060 3 à la Banque
	Cantonale de Fribourg.</p>
<p>Nous vous souhaitons bonne réception de la présente et vous remercions pour l'intérêt porté au Swss Bakery Trophy.</p>
<p>Nous vous présentons nos meilleures salutations.</p>
<br>
<p style="text-align: right"><strong>Association romande des artisans<br/>boulangers-pâtissiers-confiseurs<br><i>Swiss Bakery Trophy</i></strong></p>

<div><img src="<?= base_url('img/qr.jpg'); ?>" alt="QR-Code" width="100%"/></div>
