<?php $this->load->view('base/header');

?>
    <!-- START CONTAINER FLUID -->
    <div class=" no-padding    container-fixed-lg bg-white">
        <div class="container">
            <!-- START BREADCRUMB -->
            <div class="row">
                <div class="col-xl-12 col-lg-12">
                    <!-- START card -->
                    <div class="card card-transparent">
                        <div class="card-header ">
                            <div class="card-title"><?= t('inscription_taxateur'); ?></div>
                        </div>
                        <div class="card-block">

                            <p><?= t('inscription_taxateur_complet'); ?></p>
                            <br>
                            <p class="small hint-text m-t-5"><?= t('vous_avez_des_questions'); ?></p>
                            <a href="mailto:info@lepain.ch" class="btn btn-primary btn-cons"><?= t('contactez_nous'); ?></a>
                        </div>
                    </div>
                    <!-- END card -->
                </div>
            </div>
        </div>
    </div>

<?php $this->load->view('base/footer');