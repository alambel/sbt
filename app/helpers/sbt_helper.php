<?php

function t($line)
{
//    print_r(get_instance()->lang);
    return get_instance()->lang->line($line) ?: '!!!' . $line . '!!!';
}

function sbtmail($to, $subject, $body, $participantId = null, $attachment = null)
{
    $ci =& get_instance();
    $ci->load->library('email');

    $ci->email->clear(true);
    $ci->email->from('noreply@swissbakerytrophy.ch', 'Swiss Bakery Trophy');
    $ci->email->to($to);
    $ci->email->subject($subject);
    $ci->email->message($body);
    if ($attachment) {

        $ci->email->attach($attachment);
    }

    $rs = $ci->email->send();

    $ci->db->insert('emails', array('to' => $to,
        'subject' => $subject, 'body' => $body,
        'result' => $ci->email->print_debugger(),
        'date' => fDatetime(time()),
        'participantId' => $participantId,
        'sent' => $rs ? 1 : 0));
    $ci->email->clear(true);
    return $rs;
}

function fDatetime($date, $dateonly = false)
{
    return $date ? date($dateonly ? 'Y-m-d' : 'Y-m-d H:i:s', $date) : null;
}

function scale($name, $label = null)
{
    get_instance()->load->view('base/scale', array('name' => $name, 'label' => $label ?: t($name)));
}

function lang()
{
    return get_instance()->session->language ?: 'fr';
}

function npaCantons(){
	$rs = get_instance()->db->select('*')
		->from('npa_canton')
		->get()->result();
	$data = array();
	foreach($rs as $r){
		$data[$r->npa] = $r->canton;
	}
	return $data;
}
