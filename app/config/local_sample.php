<?php defined('BASEPATH') OR exit('No direct script access allowed'); 
$config['base_url'] = 'http://sbt.test/';


$now = strtotime('now');
define('INSCRIPTIONS_OUVERTES', $now > strtotime('2024-06-10') && $now < strtotime('2024-10-18'));
define('MODIFICATIONS_PRODUIT', $now > strtotime('2024-06-10') && $now < strtotime('2024-10-30'));
define('INSCRIPTIONS_TAXATEUR_OUVERTES', strtotime('now') > strtotime('2024-06-10'));
define('CLOTURE_CONCOURS', false);


$config['notifs'] = 'swissbakerytrophybkp@gmail.com';
$config['download_photos'] = true;
