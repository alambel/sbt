<?php

class SBTInclude
{

    private static $instance;
    var $jsIncludes = array();
    var $cssIncludes = array();
    var $js = array();
    var $css = array();

    public static function getInstance()
    {
        if (null === static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    public function __construct()
    {

    }

    public static function jsFile($path)
    {
        if (is_array($path)) {
            foreach ($path as $p) {
                self::addJsFile($p);
            }
        } else {
            self::addJsFile($path);
        }
    }

    public static function js($javascript)
    {

        $instance = self::getInstance();

        array_push($instance->js, $javascript);
    }

    public static function printJs()
    {
        $instance = self::getInstance();
        foreach ($instance->jsIncludes as $js) {
            echo '<script src="' . $js . '"></script>' . PHP_EOL;
        }
        $instance->jsIncludes = array();

        echo implode(PHP_EOL, $instance->js);
        $instance->js = array();
    }

    private static function addJsFile($path)
    {

        $instance = self::getInstance();
        if (!strpos($path, '//')) {
            $path = base_url($path);
        }
        if (!in_array($path, $instance->js)) {
            array_push($instance->jsIncludes, $path);
        }
    }

    public static function css($path)
    {
        if (is_array($path)) {
            foreach ($path as $p) {
                self::addCss($p);
            }
        } else {
            self::addCss($path);
        }
    }

    public static function printCss()
    {
        $instance = self::getInstance();
        foreach ($instance->cssIncludes as $css) {
            echo '<link href="' . $css . '" rel="stylesheet">' . PHP_EOL;
        }
        $instance->cssIncludes = array();

        echo implode(PHP_EOL, $instance->js);
        $instance->js = array();
    }

    private static function addCss($path)
    {

        $instance = self::getInstance();
        if (!strpos($path, '//')) {
            $path = base_url($path);
        }
        if (!in_array($path, $instance->css)) {
            array_push($instance->cssIncludes, $path);
        }
    }

}
