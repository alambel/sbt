<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class SBTNotification
{

    private static $instance;
    var $successes = array();
    var $warnings = array();
    var $errors = array();

    /**
     * @return SBTNotification
     */
    public static function getInstance()
    {
        if (null === static::$instance) {
            static::$instance = new static();

            $notifs = get_instance()->session->notifications;
            if ($notifs) {
                static::$instance->successes = isset($notifs['successes']) ? $notifs['successes'] : null;
                static::$instance->warnings = isset($notifs['warnings']) ? $notifs['warnings'] : null;
                static::$instance->errors = isset($notifs['errors']) ? $notifs['errors'] : null;
            }
        }

        return static::$instance;
    }

    public function __construct()
    {

    }

    public static function success($message)
    {
        $instance = SBTNotification::getInstance();
        if (is_array($message)) {
            foreach ($message as $m) {
                array_push($instance->successes, $m);
            }
        } else {
            array_push($instance->successes, $message);
        }
        $instance->save();
    }

    public static function warning($message)
    {
        $instance = SBTNotification::getInstance();
        if (is_array($message)) {
            foreach ($message as $m) {
                array_push($instance->warnings, $m);
            }
        } else {
            array_push($instance->warnings, $message);
        }
        $instance->save();
    }

    public static function error($message)
    {
        $instance = self::getInstance();
        if (is_array($message)) {
            foreach ($message as $m) {
                array_push($instance->errors, $m);
            }
        } else {
            array_push($instance->errors, $message);
        }
        $instance->save();
    }

    public static function hasMessages()
    {
        $instance = SBTNotification::getInstance();
        return count($instance->successes) + count($instance->warnings) + count($instance->errors) > 0;
    }

    public static function getSuccesses()
    {
        $instance = SBTNotification::getInstance();
        $successes = $instance->successes;
        $instance->successes = array();
        $instance->save();
        return $successes;
    }

    public static function getWarnings()
    {
        $instance = SBTNotification::getInstance();
        $warnings = $instance->warnings;
        $instance->warnings = array();
        $instance->save();
        return $warnings;
    }

    public static function getErrors()
    {
        $instance = SBTNotification::getInstance();
        $errors = $instance->errors;
        $instance->errors = array();
        $instance->save();
        return $errors;
    }

    private function save()
    {
        get_instance()->session->set_userdata('notifications', (array)$this);
    }
}
