<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class SBTTaxation extends CI_Model
{

	private static $db;
	var $id, $produit, $taxateur, $aspect, $aspect_commentaire, $forme, $forme_commentaire, $surface, $surface_commentaire, $texture, $texture_commentaire, $bouche, $bouche_commentaire, $odeur, $odeur_commentaire;

	const aspectPro = 2;
	const formePro = 1;
	const surfacePro = 1;
	const texturePro = 1;
	const bouchePro = 2;
	const odeurPro = 3;

	const aspectConso = 3;
	const boucheConso = 3;
	const odeurConso = 4;

	const conf1 = 3; // aspect
	const conf2 = 3; // odeur
	const conf3 = 2; // texture
	const conf4 = 2; // bouche

	// 10% des produits totaux
	const nbMedaillesOr = 155;
	// 15% des produits totaux
	const nbMedaillesArgent = 235;
	// 20% des produits totaux
	const nbMedaillesBronze = 310;

	static $joursTaxation = array(

		"GR" => 1,
		"JU" => 1,
		"LI" => 1,
		"NE" => 1,
		"NW" => 1,
		"OW" => 1,
		"AI" => 1,
		"AR" => 1,
		"GL" => 1,
		"SG" => 1,
		"TG" => 1,
		"ZG" => 1,
		"UR" => 1,

		"AG" => 2,
		"GE" => 2,
		"TI" => 2,

		"BL" => 3,
		"BS" => 3,
		"SH" => 3,
		"SZ" => 3,
		"VD" => 3,
		"ZH" => 3,


		"BE" => 4,
		"SO" => 4,
		"LU" => 4,
		"VS" => 4,
		"FR" => 4,

		"AC" => 5
	);

	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
		self::$db = get_instance()->db;
	}

	/**
	 * @param $produit
	 * @return SBTTaxation[]
	 */
	static function get($produit, $taxateur = null)
	{
		self::$db->where(array('produit' => $produit))
			->from('taxation');
		if ($taxateur) {
			self::$db->where('taxateur', $taxateur);
			return self::rsToObject(self::$db->get()->row());
		}
		return self::rsToObjects(self::$db->get()->result());

	}

	static function done($produit, $taxateur, $table)
	{
		$rs = self::$db->where(array('produit' => $produit, 'taxateur' => $taxateur, 'table' => $table))->count_all_results('taxation');
		return $rs > 0;
	}

	static function allDone($produit, $table)
	{
		$alldone = true;
		for ($i = 1; $i <= 5; $i++) {
			$done = SBTTaxation::done($produit, $i, $table);
			$alldone = $done ? $alldone : false;
		}
		if ($alldone) {
			$rs = self::$db->where(array('commentaire IS NOT NULL' => null, 'comm_auteur >' => 0, 'id' => $produit))->count_all_results('produit');
			$alldone = $rs > 0 ? $alldone : false;
		}
		return $alldone;
	}

	function save()
	{
		$data = array(
			'id' => $this->id,
			'produit' => $this->produit,
			'taxateur' => $this->taxateur,
			'table' => $this->table,
			'aspect' => $this->aspect,
			'aspect_commentaire' => $this->aspect_commentaire,
			'forme' => $this->forme,
			'forme_commentaire' => $this->forme_commentaire,
			'surface' => $this->surface,
			'surface_commentaire' => $this->surface_commentaire,
			'texture' => $this->texture,
			'texture_commentaire' => $this->texture_commentaire,
			'bouche' => $this->bouche,
			'bouche_commentaire' => $this->bouche_commentaire,
			'odeur' => $this->odeur,
			'odeur_commentaire' => $this->odeur_commentaire,
			'date' => fDatetime(time())
		);
		self::$db->insert('taxation', $data);
	}


	static function rsToObjects($rs)
	{
		if ($rs) {
			$items = array();
			foreach ($rs as $data) {
				array_push($items, self::rsToObject($data));
			}
			return $items;
		}
		return null;
	}

	static function rsToObject($data)
	{
		if ($data) {
			$item = new SBTTaxation();
			$item->load($data);
			return $item;
		}
		return null;
	}

	function load($args)
	{
		if ($args) {
			foreach ($args as $key => $val) {
				$key = strtolower($key);
				$this->{$key} = $val;
			}
		}
		return $this;
	}

	static function stringBackup($produitId)
	{
		$taxation = SBTTaxation::get($produitId);
		$data = array();
		if ($taxation) {
			foreach ($taxation as $t) {
				$tax = array();
				foreach ($t as $key => $value) {
					$tax[] = $key . ': ' . $value;
				}
				$data[] = '<br><br>TAXATEUR ' . $t->taxateur . '<br>' . implode('<br>', $tax);
			}
		}
		return implode(PHP_EOL, $data);

	}


	function calcTotalPro()
	{
		return ($this->aspect * self::aspectPro) +
			($this->forme * self::formePro) +
			($this->surface * self::surfacePro) +
			($this->texture * self::texturePro) +
			($this->bouche * self::bouchePro) +
			($this->odeur * self::odeurPro);
	}

	function calcTotalConso()
	{
		return ($this->aspect * self::aspectConso) +
			($this->bouche * self::boucheConso) +
			($this->odeur * self::odeurConso);

	}

	function calcTotalConf()
	{
		return ($this->aspect * self::conf1) +
			($this->odeur * self::conf2) +
			($this->texture * self::conf3) +
			($this->bouche * self::conf4);
	}

	static function edit($pId, $taxateur, $critere, $note)
	{
		return self::$db->update('taxation', [$critere => $note], ['produit' => $pId, 'taxateur' => $taxateur]);
	}

}
