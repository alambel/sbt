<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class SBTPhoto extends CI_Model
{

	static $uploadfolder = './cache/uploads/';
	static $zipfolder = './cache/zip/';
	static $productImagesFolder = './img/produits/';
	static $allPhotosZip = './cache/SBT2024-Photos.zip';
	private static $db;

	function __construct()
	{
		parent::__construct();
		self::$db = &get_instance()->db;
		get_instance()->load->helper('directory');
		if (!is_dir(SBTPhoto::$uploadfolder)) {
			mkdir(SBTPhoto::$uploadfolder);
		}
		if (!is_dir(SBTPhoto::$zipfolder)) {
			mkdir(SBTPhoto::$zipfolder);
		}

	}

	static function assign($pid, $images)
	{
		$produit = SBTProduit::get($pid);
		mkdir(self::$productImagesFolder . $produit->no);
		$id = 1;
		foreach ($images as $i) {
			$filename = self::$productImagesFolder . $produit->no . '/' . $produit->no . "_" . $id . '.jpg';
			while (file_exists($filename)) {
				$filename = self::$productImagesFolder . $produit->no . '/' . $produit->no . "_" . $id++ . '.jpg';
			}
			rename(self::$uploadfolder . $i, $filename);
		}
		unlink(self::$allPhotosZip);

		self::savePhotosCount();
	}

	static function getForProduit($pid)
	{
		$produit = SBTProduit::get($pid);
		if ($produit->no) {
			$images = directory_map(self::$productImagesFolder . $produit->no . '/');
			return $images !== false ? $images : [];
		}
		return [];
	}

	static function savePhotosCount()
	{
		$source = self::$productImagesFolder;
		$count = array();
		$params = array();
		if (is_dir($source) === true) {
			$files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);
			foreach ($files as $path => $object) {
				if (in_array(substr($path, strrpos($path, '/') + 1), array('.', '..')))
					continue;
				if (is_dir($path)) {
					$name = str_replace($source, '', $path);
					if (!isset($count[$name])) {
						$count[$name] = count(directory_map($path . '/'));
						$params[] = ['no' => $name, 'nbphotos' => $count[$name]];
					}

				}
			}

			self::$db->update_batch('produit', $params, 'no');
		}

		return $count;
	}



	static function generateAllPhotosZip()
	{
		$source = self::$productImagesFolder;
		if (!extension_loaded('zip') || !file_exists($source) || file_exists(self::$allPhotosZip)) {
			return false;
		}

		$zip = new ZipArchive();
		if (!$zip->open(self::$allPhotosZip, ZIPARCHIVE::CREATE)) {
			return false;
		}

		$source = str_replace('\\', '/', realpath($source));

		if (is_dir($source) === true) {
			$files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);

			foreach ($files as $file) {
				$file = str_replace('\\', '/', $file);

				// Ignore "." and ".." folders
				if (in_array(substr($file, strrpos($file, '/') + 1), array('.', '..')))
					continue;

				$file = realpath($file);

				if (is_dir($file) === true) {
					$zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
				} else if (is_file($file) === true) {
					$zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
				}
			}
		} else if (is_file($source) === true) {
			$zip->addFromString(basename($source), file_get_contents($source));
		}

		return $zip->close();

	}

}

