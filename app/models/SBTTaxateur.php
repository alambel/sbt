<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class SBTTaxateur extends CI_Model
{

    private static $db;
    var $id, $civilite, $nom, $prenom, $entreprise, $rue, $npa, $lieu, $canton, $langue, $email, $telephone, $jour1_am, $jour1_pm, $jour2_am, $jour2_pm, $jour3_am, $jour3_pm, $jour4_am, $jour4_pm, $type, $actif, $frais_jour, $frais_deplacement, $frais_non, $mailpreinscription;

    function __construct()
    {
        parent::__construct();
        self::$db = &get_instance()->db;
    }

    public function __toString()
    {
        return $this->nom . " " . $this->prenom;
    }

    static function getForEmail($email)
    {
        $rs = self::$db->select('email')
            ->from('participant')
            ->where('EMAIL', $email)
            ->get()->result();
        return self::rsToObject($rs);
    }

    static function rsToObjects($rs)
    {

        if ($rs) {
            $items = array();
            foreach ($rs as $data) {
                array_push($items, self::rsToObject($data));
            }
            return $items;
        }
        return null;
    }

    static function rsToObject($data)
    {
        if ($data) {
            $item = new SBTTaxateur();
            $item->load($data);
            return $item;
        }
        return null;
    }

    function load($args)
    {
        if ($args) {
            foreach ($args as $key => $val) {
                $key = strtolower($key);
                $this->{$key} = $val;
            }
        }
        return $this;
    }


    function save()
    {
        $params = array(
            'civilite' => $this->civilite,
            'nom' => $this->nom,
            'prenom' => $this->prenom,
            'entreprise' => $this->entreprise,
            'rue' => $this->rue,
            'npa' => $this->npa,
            'lieu' => $this->lieu,
            'canton' => $this->canton,
            'langue' => $this->langue,
            'email' => $this->email,
            'telephone' => $this->telephone,
            'jour1_am' => intval($this->jour1_am),
            'jour1_pm' => intval($this->jour1_pm),
            'jour2_am' => intval($this->jour2_am),
            'jour2_pm' => intval($this->jour2_pm),
            'jour3_am' => intval($this->jour3_am),
            'jour3_pm' => intval($this->jour3_pm),
            'jour4_am' => intval($this->jour4_am),
            'jour4_pm' => intval($this->jour4_pm),
            'type' => $this->type,
            'actif' => $this->actif,
            'frais_jour' => $this->frais_jour,
            'frais_deplacement' => $this->frais_deplacement,
            'frais_non' => $this->frais_non,
			'dispo'=>$this->dispo,
			'note'=>$this->note
        );
        if ($this->id) {
            self::$db->where('id', $this->id)->update('taxateur', $params);
        } else {
            self::$db->insert('taxateur', $params);
            $this->id = self::$db->insert_id();
        }
        return true;
    }

    static function get($cle)
    {
        $rs = self::$db->select('*')
            ->where(array('id' => $cle))
            ->get('taxateur')->row();
        if ($rs) {
            $user = self::rsToObject($rs);
            return $user;
        }
        return null;
    }

    static function delete($id)
    {
        self::$db->delete('taxateur', array('id' => $id));
    }

    static function getPrefill($cle)
    {
        $rs = self::$db->select('*')
            ->where(array('id' => $cle))
            ->get('taxateur_prefill')->row();
        if ($rs) {
            $user = self::rsToObject($rs);
            return $user;
        }
        return null;
    }

    static function getAll($actif = true)
    {
        $rs = self::$db->select('*')
            ->where('actif', $actif ? 1 : 0)
            ->get('taxateur')->result();
        return $rs ? self::rsToObjects($rs) : null;

    }


    static function getAllPrefill()
    {
        $rs = self::$db->select('*')
            ->get('taxateur_prefill')->result();
        return $rs ? self::rsToObjects($rs) : null;

    }


    function getPreinscriptionUrl()
    {
        return site_url('inscription_taxateur?t=' . base64_encode($this->id . '|' . substr($this->email, 0, 10)));
    }

    function sendPreinscriptionMail()
    {
        if (intval($this->mailpreinscription) == 0) {
            $subject = 'SBT - ' . ($this->langue == 'D' ? 'Aufruf für Experten' : 'Recherche de taxateurs');
            $lang = $this->langue == 'D' ? 'de' : 'fr';
            $body = $this->load->view('mail/' . $lang . '/preinscription_taxateur', array('taxateur' => $this), true);
            self::$db->where('id', $this->id)->update('taxateur_prefill', array('mailpreinscription' => 1));
            return sbtmail($this->email, $subject, $body);
        }
    }

    static function getCounts()
    {
        $rs = self::$db->select('type,sum(jour1_am) as tot1_am, sum(jour1_pm) as tot1_pm,sum(jour2_am) as tot2_am, sum(jour2_pm) as tot2_pm,sum(jour3_am) as tot3_am, sum(jour3_pm) as tot3_pm,sum(jour4_am) as tot4_am, sum(jour4_pm) as tot4_pm')
            ->where(array('actif' => 1))
            ->group_by('type')
            ->get('taxateur')->result();
        $data = array();
        foreach($rs as $row){
            $data[$row->type] = $row;
        }
        return $data;
    }

    static function getParCanton()
    {
        $rs = self::$db->select('canton,count(*) as count')->from('taxateur')->group_by('canton')->get()->result();
        $cantons = array();
        foreach ($rs as $r) {
            $cantons[$r->canton] = $r->count;
        }
        return $cantons;
    }
}

