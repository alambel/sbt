<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class SBTProduit extends CI_Model
{

	private static $db;
	var $id, $no, $nom, $categorie, $composition, $arguments, $participant, $arrivage, $commentaire, $comm_auteur, $comm_date, $comm_validated, $datemodif, $jubile;
	static $categories, $categoriesFR, $categoriesDE;

	function __construct()
	{
		parent::__construct();
		self::$db = &get_instance()->db;
		self::$categories = array(
			"" => "",
			"boulangerie" => t('boulangerie'),
			"patisserie" => t('patisserie'),
			"confiserie" => t('confiserie'),
			'tartiner' => t('masses_a_tartiner'),
			"snack" => t('snack_traiteur'));
		self::$categoriesFR = array(
			'' => '',
			'boulangerie' => 'Boulangerie',
			'patisserie' => 'Pâtisserie',
			'confiserie' => 'Confiserie',
			'tartiner' => 'Masses à tartiner',
			'snack' => 'Snack-Traiteur');
		self::$categoriesDE = array(
			'' => '',
			'boulangerie' => 'Bäckerei',
			'patisserie' => 'Konditorei',
			'confiserie' => 'Confiserie',
			'tartiner' => 'Aufstriche',
			'snack' => 'Snack-Traiteur');
	}

	static function getForParticipant($participantId, $complets = null, $sansjubile = null)
	{
		self::$db->select('*')
			->from('produit')
			->where('participant', intval($participantId));
		if ($complets) {
			self::$db->where("composition <> ''");
			self::$db->where("arguments <> ''");
		}
		if ($complets === false) {
			self::$db->where("composition = ''");
			self::$db->where("arguments = ''");
		}
		if ($sansjubile) {
			self::$db->where("jubile != 1");
		}

		$rs = self::$db->get()->result();
		return self::rsToObjects($rs);
	}

	static function getJubileForParticipant($participantId)
	{
		self::$db->select('*')
			->from('produit')
			->where('participant', intval($participantId))
			->where("jubile = 1");

		$rs = self::$db->get()->row();
		return self::rsToObject($rs);
	}

	static function rsToObjects($rs)
	{

		if ($rs) {
			$items = array();
			foreach ($rs as $data) {
				$item = new SBTProduit();
				$item->load($data);
				array_push($items, $item);
			}
			return $items;
		}
		return [];
	}

	static function rsToObject($data)
	{
		if ($data) {
			$item = new SBTProduit();
			$item->load($data);
			return $item;
		}
		return null;
	}

	function load($args)
	{
		if ($args) {
			foreach ($args as $key => $val) {
				$key = strtolower($key);
				if ($val && in_array($key, array('comm_date', 'comm_validated', 'comm_date'))) {
					$val = strtotime($val);
				}
				$this->{$key} = $val;
			}
		}
		return $this;
	}


	static function generateAllNO()
	{
		$rs = self::$db->select('pr.id')
			->from('produit pr')
			->join('participant pa ', 'pa.id=pr.participant')
			->order_by('pa.canton,pa.entreprise')
			->get()->result();
		foreach ($rs as $r) {
			$p = SBTProduit::get($r->id);
			if ($p) {
				$p->generateNO();
			}
		}

	}

	function generateNO()
	{
		$participant = SBTParticipant::get($this->participant);

		$jour = SBTTaxation::$joursTaxation[$participant->canton];

		$rs = self::$db->select('max(no) max')
			->from('produit')
			->where(array('no >' => $jour * 1000, 'no<' => ($jour + 1) * 1000))
			->get()->row();

		$lastNo = $rs && $rs->max ? $rs->max : $jour * 1000;
		$no = $lastNo + 1;
		return $this->saveNo($no);

	}


	function save()
	{
		if (!$this->nom && !$this->categorie && !$this->composition && !$this->arguments) {
			return true;
		}
		$params = array(
			'participant' => $this->participant,
			'nom' => $this->nom,
			'categorie' => $this->categorie,
			'composition' => $this->composition,
			'arguments' => $this->arguments,
			'jubile' => (int)$this->jubile
		);
		if ($this->id) {
			self::$db->where('id', $this->id)->update('produit', $params);
			return true;
		}

		$participant = SBTParticipant::get($this->participant);
		if ($participant->getNbProduits() >= 11) {
			SBTNotification::error('Nombre maximum de produits atteint');
			return false;
		}
		self::$db->insert('produit', $params);
		$this->id = self::$db->insert_id();
		return true;
	}

	function saveNo($no)
	{
		if ($this->no) {
			return false;
		}

		self::$db->where('id', $this->id)->update('produit', array('no' => $no));
		return true;

	}

	function commenter($commentaire, $auteur = null)
	{
		$params = array(
			'commentaire' => $commentaire
		);
		if ($auteur) {
			$params['comm_auteur'] = $auteur;
			$params['comm_date'] = date('Y-m-d H:i:s');
			$params['comm_validated'] = null;
		}
		if ($this->id) {
			self::$db->where('id', $this->id)->update('produit', $params);
			return true;
		}
		return false;
	}

	function delete()
	{
		self::$db->delete('produit', array('id' => $this->id));
		return self::$db->affected_rows() ? true : false;
	}

	/**
	 * @param $id
	 * @return null|SBTProduit
	 */
	static function get($id)
	{
		$rs = self::$db->select('*')
			->where(array('id' => $id))
			->get('produit')->row();
		if ($rs) {
			return self::rsToObject($rs);

		}
		return null;
	}

	/**
	 * @param $no
	 * @return null|SBTProduit
	 */
	static function getForNO($no)
	{
		$rs = self::$db->select('*')
			->where(array('no' => $no))
			->get('produit')->row();
		if ($rs) {
			return self::rsToObject($rs);

		}
		return null;
	}

	/**
	 * @param null $categorie
	 * @return SBTProduit[]|null
	 */
	static function getAll($categorie = null)
	{

		self::$db->select('pr.*')
			->from('produit pr')
			->join('participant pa', 'pa.id=pr.participant')
			->where('pa.actif = 1')
			->order_by('no', 'asc');
		if ($categorie !== null) {
			if ($categorie === "0") {
				self::$db->where('pr.categorie', null);
			} else {
				self::$db->where('pr.categorie', $categorie);
			}
		}

		$rs = self::$db->get()->result();

		if ($rs) {
			return self::rsToObjects($rs);

		}
		return array();
	}


	function isComplete()
	{
		if ($this->id > 0 && $this->nom && $this->categorie && strlen($this->composition) >= 10 && strlen($this->arguments) >= 10) {
			return true;
		}
		return false;
	}

	function arrivage()
	{
		if ($this->id) {
			$params = array('arrivage' => date('Y-m-d H:i:s'));
			self::$db->where('id', $this->id)->update('produit', $params);
			return true;
		}
		return false;
	}

	static function nbRecus()
	{
		$rs = self::$db->select('pa.canton, count(*), case when pr.arrivage is not null then 1 else 0 end as recu')
			->from('produit pr')
			->join('participant pa ', 'pa.id=pr.participant')
			->group_by('pa.canton, recu')
			->get()->result();
		print_r($rs);

	}


	function commValidate()
	{
		if ($this->id) {
			$params = array('comm_validated' => date('Y-m-d H:i:s'));
			self::$db->where('id', $this->id)->update('produit', $params);
			return true;
		}
		return false;
	}


	static function getParCanton()
	{
		$rs = self::$db->select('pa.canton,count(*) as count')
			->from('produit pr')
			->join('participant pa', 'pa.id=pr.participant')
			->where('pa.actif', 1)
			->group_by('canton')->get()->result();

		$cantons = array();
		foreach ($rs as $r) {
			$cantons[$r->canton] = $r->count;
		}
		return $cantons;
	}

	static function getRecuParCanton()
	{
		$rs = self::$db->select('pa.canton,count(*) as count')
			->from('produit pr')
			->join('participant pa', 'pa.id=pr.participant')
			->where(array('pa.actif' => 1, 'pr.arrivage IS NOT NULL' => null))
			->group_by('canton')->get()->result();

		$cantons = array();
		foreach ($rs as $r) {
			$cantons[$r->canton] = $r->count;
		}
		return $cantons;
	}

	static function getTaxesParCanton()
	{
		$rs = self::$db->select('pa.canton,count(*) as count')
			->from('produit pr')
			->join('participant pa', 'pa.id=pr.participant')
			->where(array('pa.actif' => 1, 'pr.moyenne IS NOT NULL' => null))
			->group_by('canton')->get()->result();

		$cantons = array();
		foreach ($rs as $r) {
			$cantons[$r->canton] = $r->count;
		}
		return $cantons;
	}

	static function getPhotosParCanton()
	{
		$rs = self::$db->select('pa.canton,count(*) as count')
			->from('produit pr')
			->join('participant pa', 'pa.id=pr.participant')
			->where(array('pa.actif' => 1, 'pr.nbphotos >=' => 1))
			->group_by('canton')->get()->result();

		$cantons = array();
		foreach ($rs as $r) {
			$cantons[$r->canton] = $r->count;
		}
		return $cantons;
	}

	static function getNbParCategories()
	{
		$rs = self::$db->select('categorie,count(*) as cnt')
			->from('produit')
			->group_by('categorie')->get()->result();

		$cantons = array();
		foreach ($rs as $r) {
			$cantons[$r->categorie] = $r->cnt;
		}
		return $cantons;
	}

	static function getNonValides()
	{
		$rs = self::$db->select('pr.*, pa.lang, c.nom as comm_nom, c.prenom as comm_prenom')
			->where('comm_validated is null', null)
			->join('participant pa', 'pa.id=pr.participant', 'left')
			->join('participant c', 'c.id=pr.comm_auteur', 'left')
			->where('comm_date IS NOT NULL', null)
			->get('produit pr')->result();
		return self::rsToObjects($rs);
	}

	static function recalculate()
	{
		if (CLOTURE_CONCOURS) {
			return false;
		}
		self::$db->update('produit', array('moyenne' => null, 'notes' => null, 'medaille' => null));
		self::$db->distinct('produit')->select('produit')
			->from('taxation');

		$rs = self::$db->get()->result();
		foreach ($rs as $r) {
			self::calculate($r->produit);
		}
	}

	static function calculate($pid)
	{

		if (CLOTURE_CONCOURS) {
			return false;
		}
		$p = SBTProduit::get($pid);

		if ($p->categorie == 'tartiner') {
			return self::calculateConfiture($pid);
		} else {
			return self::calculateStandard($pid);
		}
	}

	private static function calculateStandard($pid)
	{

		if (CLOTURE_CONCOURS) {
			return false;
		}
		$taxations = SBTTaxation::get($pid);

		if (count($taxations) != 5) {
			SBTNotification::error(t('taxation_non_terminee').' - '.$pid);

			return false;
		}

		$table = null;
		$data = array('notes' => array());
		foreach ($taxations as $t) {
			$table = $t->table;
			$name = 'n/a';
			$total = 0;
			switch ($t->taxateur) {
				case 1:
					$name = 'pro1';
					$total = $t->calcTotalPro();
					break;
				case 2:
					$name = 'pro2';
					$total = $t->calcTotalPro();
					break;
				case 3:
					$name = 'pro3';
					$total = $t->calcTotalPro();
					break;
				case 4:
					$name = 'conso1';
					$total = $t->calcTotalConso();
					break;
				case 5:
					$name = 'conso2';
					$total = $t->calcTotalConso();
					break;
			}
			$data['notes'][$name] = array('total' => $total,
				'aspect' => $t->aspect,
				'forme' => $t->forme,
				'surface' => $t->surface,
				'texture' => $t->texture,
				'bouche' => $t->bouche,
				'odeur' => $t->odeur);
		}


		// Recherche de la note minimum;
		$data['min'] = array('name' => null, 'value' => null);
		$data['max'] = array('name' => null, 'value' => null);
		foreach ($data['notes'] as $name => $note) {
			if (($note['total'] < $data['min']['value'] || $data['min']['value'] == null) && $data['max']['name'] != $name) {
				$data['min'] = array('name' => $name, 'value' => $note['total']);
			}
		}
		foreach ($data['notes'] as $name => $note) {
			if (($note['total'] > $data['max']['value'] || $data['max']['value'] == null) && $data['min']['name'] != $name) {
				$data['max'] = array('name' => $name, 'value' => $note['total']);
			}
		}

		$data['somme'] = 0;

		foreach ($data['notes'] as $name => $note) {
			if ($name != $data['min']['name'] && $name != $data['max']['name']) {
				$data['somme'] += $note['total'];
			}
		}
		$data['moyenne'] = $data['somme'] / 3;


		$p1 = $data['notes']['pro1']['total'];
		$p2 = $data['notes']['pro2']['total'];
		$p3 = $data['notes']['pro3']['total'];
		$c1 = $data['notes']['conso1']['total'];
		$c2 = $data['notes']['conso2']['total'];

		$minmax = array($data['min']['name'], $data['max']['name']);
		$notes = 'p: ' . (in_array('pro1', $minmax) ? '(' . $p1 . ')' : $p1) . ', ' .
			(in_array('pro2', $minmax) ? '(' . $p2 . ')' : $p2) . ', ' .
			(in_array('pro3', $minmax) ? '(' . $p3 . ')' : $p3) . ', ' .
			'c: ' . (in_array('conso1', $minmax) ? '(' . $c1 . ')' : $c1) . ', ' .
			(in_array('conso2', $minmax) ? '(' . $c2 . ')' : $c2);
		self::$db->update('produit', array('moyenne' => $data['moyenne'], 'notes' => $notes, 'table' => $table), array('id' => $pid));
		return $data;

	}


	private static function calculateConfiture($pid)
	{

		if (CLOTURE_CONCOURS) {
			return false;
		}
		$taxations = SBTTaxation::get($pid);

		if (count($taxations) != 5) {
			SBTNotification::error(t('taxation_non_terminee'));
			return false;
		}
		$table = null;
		$data = array('notes' => array());
		foreach ($taxations as $t) {
			$name = 'n/a';
			$table = $t->table;
			switch ($t->taxateur) {
				case 1:
					$name = 'pro1';
					break;
				case 2:
					$name = 'pro2';
					break;
				case 3:
					$name = 'pro3';
					break;
				case 4:
					$name = 'conso1';
					break;
				case 5:
					$name = 'conso2';

					break;
			}
			$total = $t->calcTotalConf();

			$data['notes'][$name] = array('total' => $total,
				'conf1' => $t->aspect,
				'conf2' => $t->odeur,
				'conf3' => $t->texture,
				'conf4' => $t->bouche);
		}


		// Recherche de la note minimum;
		$data['min'] = array('name' => null, 'value' => null);
		$data['max'] = array('name' => null, 'value' => null);

		foreach ($data['notes'] as $name => $note) {
			if (($note['total'] < $data['min']['value'] || $data['min']['value'] == null) && $data['max']['name'] != $name) {
				$data['min'] = array('name' => $name, 'value' => $note['total']);
			}
		}
		foreach ($data['notes'] as $name => $note) {
			if (($note['total'] > $data['max']['value'] || $data['max']['value'] == null) && $data['min']['name'] != $name) {
				$data['max'] = array('name' => $name, 'value' => $note['total']);
			}
		}


		$data['somme'] = 0;

		foreach ($data['notes'] as $name => $note) {
			if ($name != $data['min']['name'] && $name != $data['max']['name']) {
				$data['somme'] += $note['total'];
			}
		}
		$data['moyenne'] = $data['somme'] / 3;

		$p1 = $data['notes']['pro1']['total'];
		$p2 = $data['notes']['pro2']['total'];
		$p3 = $data['notes']['pro3']['total'];
		$c1 = $data['notes']['conso1']['total'];
		$c2 = $data['notes']['conso2']['total'];

		$minmax = array($data['min']['name'], $data['max']['name']);
		$notes = 'p: ' . (in_array('pro1', $minmax) ? '(' . $p1 . ')' : $p1) . ', ' .
			(in_array('pro2', $minmax) ? '(' . $p2 . ')' : $p2) . ', ' .
			(in_array('pro3', $minmax) ? '(' . $p3 . ')' : $p3) . ', ' .
			'c: ' . (in_array('conso1', $minmax) ? '(' . $c1 . ')' : $c1) . ', ' .
			(in_array('conso2', $minmax) ? '(' . $c2 . ')' : $c2);
		self::$db->update('produit', array('moyenne' => $data['moyenne'], 'notes' => $notes, 'table' => $table), array('id' => $pid));
		return $data;

	}


	static function getNbMedailleForCategory($category)
	{
		$nbTotalProduits = self::$db->where('no <', 5000)->where('jubile',0)->count_all_results('produit');

		if ($category == 'confiserie') {
			$nbProduits = self::$db->where_in('categorie', array('confiserie', 'tartiner'))->where('no <', 5000)->where('jubile',0)->count_all_results('produit');
		} else {
			$nbProduits = self::$db->where(array('categorie' => $category))->where('no <', 5000)->where('jubile',0)->count_all_results('produit');
		}
		$prorataCategory = $nbProduits / $nbTotalProduits;

		$or = floor(SBTTaxation::nbMedaillesOr * $prorataCategory);
		$argent = floor(SBTTaxation::nbMedaillesArgent * $prorataCategory);
		$bronze = floor(SBTTaxation::nbMedaillesBronze * $prorataCategory);
		$nbs = array('or' => $or,
			'argent' => $argent,
			'bronze' => $bronze);

		return $nbs;
	}

	static function calculateMedailles()
	{
		if (CLOTURE_CONCOURS) {
			return false;
		}

		self::$db->update('produit', array('medaille' => null));
		foreach (array('boulangerie', 'patisserie', 'confiserie', 'snack') as $category) {

			$category = $category != 'tartiner' ? $category : 'confiserie';
			$nbMedailles = self::getNbMedailleForCategory($category);

			self::$db->select('id')
				->from('produit')
				->where('moyenne >', 0)
				->where('jubile',0)
				->where('no <', 5000)
				->order_by('moyenne desc')
				->limit($nbMedailles['or'], 0);
			if ($category == 'confiserie') {
				self::$db->where_in('categorie', array('confiserie', 'tartiner'));
			} else {
				self::$db->where(array('categorie' => $category));
			}
			$orData = self::$db->get()->result();
			$or = array();
			foreach ($orData as $o) {
				$or[] = $o->id;
			}
			if (count($or)) {
				self::$db->where_in('id', $or)->update('produit', array('medaille' => '1 Or'));
			}

			self::$db->select('id')
				->from('produit')
				->where('moyenne >', 0)
				->where('jubile',0)
				->where('no <', 5000)
				->order_by('moyenne desc')
				->limit($nbMedailles['argent'], $nbMedailles['or']);
			if ($category == 'confiserie') {
				self::$db->where_in('categorie', array('confiserie', 'tartiner'));
			} else {
				self::$db->where(array('categorie' => $category));
			}
			$argentData = self::$db->get()->result();
			$argent = array();
			foreach ($argentData as $a) {
				$argent[] = $a->id;
			}
			if (count($argent)) {
				self::$db->where_in('id', $argent)->update('produit', array('medaille' => '2 Argent'));
			}


			self::$db->select('id')
				->from('produit')
				->where('moyenne >', 0)
				->where('jubile',0)
				->where('no <', 5000)
				->order_by('moyenne desc')
				->limit($nbMedailles['bronze'], ($nbMedailles['or'] + $nbMedailles['argent']));

			if ($category == 'confiserie') {
				self::$db->where_in('categorie', array('confiserie', 'tartiner'));
			} else {
				self::$db->where(array('categorie' => $category));
			}
			$bronzeData = self::$db->get()->result();
			$bronze = array();
			foreach ($bronzeData as $b) {
				$bronze[] = $b->id;
			}
			if (count($bronze)) {
				self::$db->where_in('id', $bronze)->update('produit', array('medaille' => '3 Bronze'));
			}

		}

	}

	static function getMedailles()
	{

		$rs = self::$db->where('medaille is not null', null)
			->order_by('moyenne desc')
			->get('produit')->result();

		if ($rs) {
			$products = array();
			foreach ($rs as $data) {
				$product = new SBTProduit();
				$product->load($data);
				array_push($products, $product);
			}
			return $products;
		}
		return null;
	}


	static function getPrixCategorie($categories)
	{

		$rs = self::$db->where_in('categorie', $categories)
			->where('medaille is not null', null)
			->where('jubile', 0)->where('no <', 5000)
			->order_by('moyenne desc')
			->limit(5, 0)
			->get('produit')->result();

		if ($rs) {
			$products = array();
			$nb = 0;
			foreach ($rs as $data) {
				$product = new SBTProduit();
				$product->load($data);
				$product->participant = SBTParticipant::get($product->participant);
				array_push($products, $product);
			}
			return $products;
		}
		return null;
	}

	static function getPrixJubile()
	{

		$rs = self::$db->where('jubile', 1)
			->where('moyenne >', 0)->where('no <', 5000)
			->order_by('moyenne desc')
			->limit(5, 0)
			->get('produit')->result();

		if ($rs) {
			$products = array();
			foreach ($rs as $data) {
				$product = new SBTProduit();
				$product->load($data);
				$product->participant = SBTParticipant::get($product->participant);
				array_push($products, $product);
			}
			return $products;
		}
		return null;
	}

	static function getPrixAcademy()
	{

		$rs = self::$db->where('no >', 5000)
//			->where('moyenne >', 0)
			->order_by('moyenne desc', 'no asc')
			->get('produit')->result();

		if ($rs) {
			$products = array();
			foreach ($rs as $data) {
				$product = new SBTProduit();
				$product->load($data);
				$product->participant = SBTParticipant::get($product->participant);
				array_push($products, $product);
			}
			return $products;
		}
		return null;
	}

	static function getChampions()
	{
		$entreprises = self::$db->select('participant, count(*) as nb')->from('produit')->where('moyenne >', 0)->where('no <', 5000)->where('jubile',0)
			->group_by('participant')->having('nb >=5 and count( distinct categorie) > 1')
			->get()->result();
		$champions = array();
		foreach ($entreprises as $e) {
			$produits = self::getMeilleursProduits($e->participant);
			$categories = array();
			foreach ($produits as $p) {
				$categories[$p->categorie] = true;
			}
			if (count($categories) > 1) {
				$moyenne = ($produits[0]->moyenne + $produits[1]->moyenne + $produits[2]->moyenne + $produits[3]->moyenne + $produits[4]->moyenne) / 5;
				$participant = SBTParticipant::get($e->participant);
				$row = array(
					'participant' => $participant,
					'moyenne' => $moyenne,
					'produits' => $produits
				);
				array_push($champions, $row);
			}
		}

		return $champions;
	}

	/**
	 * @param $participantId
	 * @return SBTProduit[]
	 */
	static function getMeilleursProduits($participantId)
	{

		$rs = self::$db->where('participant', $participantId)
			->order_by('moyenne desc')
			->limit(5, 0)
			->get('produit')->result();
		if ($rs) {
			$products = array();
			foreach ($rs as $data) {
				$product = new SBTProduit();
				$product->load($data);
				array_push($products, $product);
			}
			return $products;
		}
		return [];
	}

	static function cancelTaxation($pid)
	{
		self::$db->delete('taxation', array('produit' => $pid));
		self::$db->update('produit',
			array('commentaire' => null, 'comm_auteur' => null, 'comm_date' => null, 'comm_validated' => null, 'moyenne' => null, 'notes' => null, 'medaille' => null),
			array('id' => $pid)
		);
		self::$db->delete('table', array('produit' => $pid));
		redirect('admin/tables');
	}

	static function dropdown()
	{
		$options = array('' => 'Aucun');

		foreach (self::getAll() as $p) {
			$options[$p->id] = $p->no . ' - ' . $p->nom;
		}
		return $options;
	}

	function nbPhotos()
	{
		$this->load->model('SBTPhoto');
		return count(SBTPhoto::getForProduit($this->id));
	}

	function getDoublons()
	{
		$query = "select produit, `table`, taxateur, count(*)
from     taxation
group by produit, `table`, taxateur	
having   count(*) > 1";

		$rs = self::$db->query($query)->result();
		return count($rs);
	}

}

