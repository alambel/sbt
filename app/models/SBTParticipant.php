<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class SBTParticipant extends CI_Model
{

	private static $db;
	var $id, $entreprise, $nom, $prenom, $rue, $npa, $lieu, $canton, $email, $telephone, $mobile, $password, $rights, $billed,
		$paid, $actif, $dateinscription, $taxateur, $table, $mailpreinscription, $lang, $totalfacture, $totalpaiement;
	static $ADMIN = 10;
	static $MANAGER = 5;
	static $cantons = array(
		"" => "",
		"AG" => "Aargau",
		"AI" => "Appenzell Innerrhoden",
		"AR" => "Appenzell Ausserrhoden",
		"BE" => "Bern",
		"BL" => "Basel-Landschaft",
		"BS" => "Basel-Stadt",
		"FR" => "Fribourg",
		"GE" => "Genève",
		"GL" => "Glarus",
		"GR" => "Grisons",
		"JU" => "Jura",
		"LU" => "Luzern",
		"NE" => "Neuchâtel",
		"NW" => "Nidwalden",
		"OW" => "Obwalden",
		"SG" => "St. Gallen",
		"SH" => "Schaffhausen",
		"SO" => "Solothurn",
		"SZ" => "Schwyz",
		"TG" => "Thurgau",
		"TI" => "Ticino",
		"UR" => "Uri",
		"VD" => "Vaud",
		"VS" => "Valais",
		"ZG" => "Zug",
		"ZH" => "Zürich",
		"LI" => "Liechtenstein");

	function __construct()
	{
		parent::__construct();
		self::$db = &get_instance()->db;
	}

	public function __toString()
	{
		return $this->entreprise;
	}

	static function getForEmail($email)
	{
		$rs = self::$db->select('email')
			->from('participant')
			->where('EMAIL', $email)
			->get()->result();
		return self::rsToObject($rs);
	}

	static function rsToObjects($rs, $formatForCsv = false)
	{

		if ($rs) {
			$items = array();
			foreach ($rs as $data) {
				$item = new SBTParticipant();
				$item->load($data);
				if ($formatForCsv) {
					unset($item->password);
					unset($item->rights);
					unset($item->table);
					unset($item->taxateur);
					unset($item->mailpreinscription);
					$item->nbproduits = $item->getNbProduits();
				}
				array_push($items, $item);
			}
			return $items;
		}
		return null;
	}

	static function rsToObject($data)
	{

		if ($data) {
			$item = new SBTParticipant();
			$item->load($data);
			return $item;
		}
		return null;
	}

	function load($args)
	{
		if ($args) {
			foreach ($args as $key => $val) {
				$key = strtolower($key);
				if (in_array($key, array('paid', 'billed'))) {
					$this->{$key} = strtotime($val);
				} else {
					$this->{$key} = $val;
				}

			}
		}
		return $this;
	}


	static function register($data, $password)
	{
		if (!self::getForEmail($data['email'])) {
			$params = array(
				'entreprise' => isset($data['entreprise']) ? $data['entreprise'] : null,
				'nom' => isset($data['nom']) ? $data['nom'] : null,
				'prenom' => isset($data['prenom']) ? $data['prenom'] : null,
				'rue' => isset($data['rue']) ? $data['rue'] : null,
				'npa' => isset($data['npa']) ? $data['npa'] : null,
				'lieu' => isset($data['lieu']) ? $data['lieu'] : null,
				'canton' => isset($data['canton']) ? $data['canton'] : null,
				'email' => isset($data['email']) ? $data['email'] : null,
				'telephone' => isset($data['telephone']) ? $data['telephone'] : null,
				'mobile' => isset($data['mobile']) ? $data['mobile'] : null,
				'password' => $password,
				'lang' => isset($data['langue']) ? $data['langue'][0] : lang(),
				'actif' => 1,
				'dateinscription' => fDatetime(time())
			);
			self::$db->insert('participant', $params);
			return true;
		}
		SBTNotification::error('Cet email est déjà enregistré');
		return false;

	}

	function save()
	{
		$params = array(
			'entreprise' => $this->entreprise,
			'nom' => $this->nom,
			'prenom' => $this->prenom,
			'rue' => $this->rue,
			'npa' => $this->npa,
			'lieu' => $this->lieu,
			'canton' => $this->canton,
			'email' => $this->email,
			'telephone' => $this->telephone,
			'mobile' => $this->mobile
		);
		self::$db->where('id', $this->id)->update('participant', $params);
		return true;
	}

	function setPassword($password)
	{
		$params = array(
			'password' => sha1($password)
		);
		self::$db->where('id', $this->id)->update('participant', $params);
		return true;
	}


	function activate()
	{
		$params = array(
			'actif' => 1,
			'dateinscription' => fDatetime(time())
		);
		self::$db->where('id', $this->id)->update('participant', $params);
		return true;
	}

	/**
	 * @param $cle
	 * @return null|SBTParticipant
	 */
	static function get($cle)
	{
		$rs = self::$db->select('*')
			->where(array('id' => $cle))
			->get('participant')->row();
		if ($rs) {
			$user = self::rsToObject($rs);
			unset($user->password); // do not save passname for security reason
			return $user;
		}
		return null;
	}

	static function getAll($actif = true, $formatForCsv = false)
	{
		$rs = self::$db->select('*')
			->where('actif', $actif ? 1 : 0)
			->get('participant')->result();
		return $rs ? self::rsToObjects($rs, $formatForCsv) : null;

	}

	static function getPreinscriptions()
	{
		$rs = self::$db->select('*')
			->where(array('dateinscription is null' => null))
			->get('participant')->result();
		return $rs ? self::rsToObjects($rs) : null;

	}

	static function getPass($username)
	{
		$rs = self::$db->select('passname')->get_where('ACCES', array('name' => strtolower($username)))->row();
		return isset($rs->passname) ? $rs->passname : null;
	}

	static function login($username, $password)
	{
		self::$db->select('id')->where(array('lower(email)' => strtolower($username), 'password' => sha1($password)));
		$rs = self::$db->get('participant')->row();

		if ($rs) {
			$user = self::get($rs->id);
			get_instance()->session->set_userdata('isLoggedIn', 1);
			get_instance()->session->set_userdata('sbtuser', (array)$user);
			return true;
		}
		return false;
	}

	static function logout()
	{
		get_instance()->session->sess_destroy();
	}

	static function getCurrentUserId()
	{
		$user = get_instance()->session->userdata('sbtuser');
		return $user ? $user['id'] : null;
	}

	static function getCurrent()
	{
		$user = self::getCurrentUserId() ? self::get(self::getCurrentUserId()) : null;
		return $user;
	}

	static function dropdown()
	{
		$options = array();
		foreach (self::getAll() as $d) {
			$options[$d->id] = $d->nom . ' ' . $d->prenom;
		}
		return $options;
	}

	function getProduits($complets = null, $sansjubile = false)
	{
		return SBTProduit::getForParticipant($this->id, $complets, $sansjubile);
	}

	/**
	 * @return SBTProduit
	 */
	function getJubile()
	{
		return SBTProduit::getJubileForParticipant($this->id);
	}

	function getNbProduits()
	{
		$produits = $this->getProduits();
		return count($produits);
	}

	function getNbProduitsSansJubile()
	{
		$produits = SBTProduit::getForParticipant($this->id, null, true);
		return count($produits);
	}

	function getNbProduitsComplets()
	{
		$produits = $this->getProduits(true);
		return count($produits);
	}

	function getNbProduitsIncomplets()
	{
		$produits = $this->getProduits(false);
		return count($produits);
	}

	static function isManager()
	{
		$user = self::getCurrent();
		return $user && $user->rights >= self::$MANAGER;
	}

	static function isAdmin()
	{
		$user = self::getCurrent();
		return $user && $user->rights >= self::$ADMIN;
	}

	static function isTaxateur()
	{
		$user = self::getCurrent();
		return $user && $user->taxateur > 0;
	}

	static function isLoggedIn()
	{
		$user = self::getCurrent();
		return $user ? true : false;
	}

	function sendPreinscriptionMail()
	{
		if (intval($this->mailpreinscription) == 0) {
			$subject = 'Swiss Bakery Trophy 2024';
			$lang = $this->lang == 'D' ? 'de' : 'fr';
			$body = $this->load->view('mail/' . $lang . '/preinscription', array('participant' => $this), true);
			self::$db->where('id', $this->id)->update('participant', array('mailpreinscription' => 1));
			return sbtmail($this->email, $subject, $body, $this->id);
		}
	}

	function sendIncompletsMail()
	{

		$subject = 'SBT - Produits incomplets';
		$lang = $this->lang == 'D' ? 'de' : 'fr';
		$body = $this->load->view('mail/' . $lang . '/incomplets', array('participant' => $this), true);
		return sbtmail($this->email, $subject, $body, $this->id);

	}

	function getFacturePath()
	{

		if (!is_dir(FCPATH . '/cache/factures')) {
			mkdir(FCPATH . '/cache/factures');
		}
		return 'cache/factures/SBT2024-F' . sprintf('%04d', $this->id) . '.pdf';

	}

	function genererFacture()
	{
		require_once FCPATH . '/vendor/autoload.php';
		if (!is_dir(FCPATH . '/cache/pdf')) {
			mkdir(FCPATH . '/cache/pdf');
		}

		try {
			$filepath = FCPATH . $this->getFacturePath();
			if (file_exists($filepath)) {
				unlink($filepath);
			}


			$mpdf = new \Mpdf\Mpdf(array(
				'tempDir' => FCPATH . '/cache/pdf',
				'format' => 'A4',
				'debug' => true,
				'showImageErrors' => true
			));
			$html = $this->load->view('facture_' . ($this->lang == 'D' ? 'de' : 'fr'), array('p' => $this), true);
			$mpdf->WriteHTML($html);
			$mpdf->Output($filepath, \Mpdf\Output\Destination::FILE);
//			$mpdf->Output();//replace by previous line for prod

		} catch (Exception $e) {
			echo 'Error: ' . $e->getMessage();
		}

//		exit();
		return $filepath;
	}

	function sendFactureMail($resend = true)
	{

			ini_set('display_errors', 1);
			ini_set('display_startup_errors', 1);
			error_reporting(E_ERROR);
		if ((!$this->billed || $resend) && $this->getNbProduits() > 0) {
			$facturepath = $this->genererFacture();
//			exit();
			$subject = 'SBT2024 : ' . ($this->lang == 'D' ? 'Rechnung' : 'Facture');
			$body = $this->load->view('mail/' . ($this->lang == 'D' ? 'de' : 'fr') . '/facture', array('participant' => $this), true);
			$rs = sbtmail($this->email, $subject, $body, $this->id, $facturepath);
			if ($rs) {
				$this->billed = time();
				$this->totalfacture = $this->getTotal();
				$params = array('billed' => fDatetime($this->billed), 'totalfacture' => $this->totalfacture);
				self::$db->where('id', $this->id)->update('participant', $params);
			}
			return $rs;
		}
	}

	static function sendAllFactures()
	{
		$participants = self::$db->select('id')->where(array('billed is null' => null, 'actif' => 1))->get('participant')->result();
		$i = 0;
		$count = 0;
		foreach ($participants as $p) {
			$part = SBTParticipant::get($p->id);
			if ($part->sendFactureMail(false)) {
				$count += 1;
				if ($i++ > 10) {
					break;
				}
			}
		}
		SBTNotification::success($count . ' factures envoyées avec succès');
		redirect('admin/entreprises');

	}

	function setPaid($date, $amount)
	{
		$this->paid = $date;
		$this->totalpaiement = $amount;
		$params = array('paid' => fDatetime($this->paid), 'totalpaiement' => floatval($this->totalpaiement));
		self::$db->where('id', $this->id)->update('participant', $params);
		return self::$db->affected_rows() ? true : false;

	}

	function getPreinscriptionUrl()
	{
		return site_url('inscription?t=' . base64_encode($this->id . '|' . substr($this->entreprise, 0, 10)));
	}

	function getHistory()
	{
		$history = array();
		$mails = $this->db->where(array('participantid' => $this->id))->order_by('date desc')->get('emails')->result();
		foreach ($mails as $m) {
			$history[$m->date] = 'Envoi d\'email ' . ($m->sent ? 'réussi' : 'échoué') . ' à ' . $m->to . ': ' . $m->subject;
		}
		return $history;
	}

	function getTotal()
	{
		$nbProduits = $this->getNbProduitsSansJubile();
		switch ($nbProduits) {
			case 1:
				return 200;
			case 2:
				return 350;
			case 3:
				return 500;
			case 4:
				return 550;
			case 5:
				return 600;
			case 6:
				return 650;
			case 7:
				return 700;
			case 8:
				return 750;
			case 9:
				return 800;
			case 10:
				return 850;
		}
		return 0;
	}

	static function delete($id)
	{
		$participant = self::get($id);
		$params = array(
			'actif' => 0,
			'dateinscription' => $participant->dateinscription ?: '2000-00-00'
		);
		self::$db->where('id', $id)->update('participant', $params);
		return true;
	}

	static function getParCanton()
	{
		$rs = self::$db->select('canton,count(*) as count')->from('participant')->where('actif', 1)->group_by('canton')->get()->result();
		$cantons = array();
		foreach ($rs as $r) {
			$cantons[$r->canton] = $r->count;
		}
		return $cantons;
	}
}

