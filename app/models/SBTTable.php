<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class SBTTable extends CI_Model
{

    private static $db;
    var $id, $produit;


    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        self::$db = get_instance()->db;
    }

    static function current($table)
    {
        $rs = self::$db->where(array('id' => $table))->get('table')->row();
        return $rs && isset($rs->produit) ? intval($rs->produit) : null;
    }

    static function set($table, $produit)
    {
        if (self::current($table) > 0 || !$produit) {
            return false;
        }
        $data = array(
            'id' => $table,
            'produit' => $produit
        );
        self::$db->insert('table', $data);
    }


    static function close($table, $sendmail = false)
    {
        $to = get_instance()->config->config['notifs'];
        $rs = self::$db->get('table', array('id' => $table))->row();

        $produit = SBTProduit::get($rs->produit);
        self::$db->delete('table', array('id' => $table));
        if ($sendmail) {
            $body = 'Taxation terminée pour le produit ' . $produit->no . ': ' . $produit->nom . '<br>' .
                'Commentaire Chef de table ' . $table . ':<br>' .
                $produit->commentaire .
                SBTTaxation::stringBackup($rs->produit);
            sbtmail($to, 'Taxation produit no' . $produit->no . ' (' . $rs->produit . ')', $body);
        }

    }
}
